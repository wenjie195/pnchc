<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminAnnoucementAdd.php" />
<link rel="canonical" href="https://agentpnchc.com/adminAnnoucementAdd.php" />
<meta property="og:title" content="<?php echo _PROFILE_ANNOUNCEMENT_ADD ?> | Pure & Cure" />
<title><?php echo _PROFILE_ANNOUNCEMENT_ADD ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>


<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_ANNOUNCEMENT_ADD ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	
	<div class="width100 inner-bg inner-padding">
	
   
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x text-center">

    <form action="utilities/adminAnnoucementAddFunction.php" method="POST">


                
                    <input type="date" placeholder="<?php echo _TOPUP_DATE ?>"  class="rec-input clean ow-margin-left0 ow-width100" name='date' id="date" required>
               

                <div class="clear"></div>

               
                    <input type="text" class="rec-input clean ow-margin-left0 ow-width100" placeholder="<?php echo _ANNOUNCEMENT_TITLE ?>" id="title" name="title" required required>
                

                <div class="clear"></div>
<textarea type="text" class="rec-input clean ow-margin-left0 ow-width100 text-area-height" placeholder="<?php echo _ANNOUNCEMENT_CONTENT ?>"  id="message" name="message" required></textarea>
                
               
                   

                <div class="clear"></div>     

                

                <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _PROFILE_CONFIRM ?></button>

    

        </div>

    </form>

    <div class="clear"></div>
   
</div>
</div>

<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>




<script>
  $(function()
  {
    $("#date").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

</body>
</html>