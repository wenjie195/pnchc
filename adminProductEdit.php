<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminProductEdit.php" />
<link rel="canonical" href="https://agentpnchc.com/adminProductEdit.php" />
<meta property="og:title" content="<?php echo _ADMIN_EDIT_PRODUCT ?> | Pure & Cure" />
<title><?php echo _ADMIN_EDIT_PRODUCT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ADMIN_EDIT_PRODUCT ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
	
   
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x text-center">

    <?php
    if(isset($_POST['product_uid']))
    {
    $conn = connDB();
    $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['product_uid']),"s");
    ?>

    <form action="utilities/adminProductEditFunction.php" method="POST" enctype="multipart/form-data">

	<h3 class="center-div-h3"><?php echo $productDetails[0]->getName();?></h3>
        <!--<input type="text" class="rec-input clean ow-margin-left0 ow-width100" value="<?php echo $productDetails[0]->getName();?>" id="product_name" name="product_name" readonly>-->

        <div class="clear"></div>

        <textarea placeholder="<?php echo _STOCK_PRODUCT_DETAILS ?>" type="text" class="rec-input clean ow-margin-left0 ow-width100 text-area-height"id="description_one" name="description_one"><?php echo $productDetails[0]->getDescription();?></textarea>
        
        <div class="clear"></div>
        
        <!-- <textarea  type="hidden" class="rec-input clean ow-margin-left0 ow-width100 text-area-height"  id="description_two" name="description_two"><?php echo $productDetails[0]->getDescriptionTwo();?></textarea>

        <div class="clear"></div> -->

        <div class="width100 overflow margin-bottom10">
            <p class="input-top-p admin-top-p"><?php echo _ADMIN_IMAGE ?>:<a  href="productImage/<?php echo $productDetails[0]->getImageOne();?>" data-fancybox="images-preview" title="<?php echo _ADMIN_IMAGE ?>"  class="dark-tur-link" target="_blank"><?php echo _ADMIN_VIEW ?></a></p>
            <p><input id="file-upload" style="color:black !important;" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer"/></p>
        </div>

        <input type="hidden" class="clean" value="<?php echo $productDetails[0]->getUid();?>" id="product_uid" name="product_uid">
        <input class="clean" type="hidden" value="<?php echo $productDetails[0]->getImageOne();?>" name="ori_fileone" id="ori_fileone"> 

        <div class="clear"></div>       

        <button class="clean yellow-btn edit-profile-width ow-margin-left0 margin-top30" name="submit"><?php echo _PROFILE_CONFIRM ?></button>

        </div>

    </form>

    <?php
    }
    ?>

    <div class="clear"></div>
   
</div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>