<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$products = getOrderList($conn, "WHERE user_uid = ? ORDER BY date_created DESC ",array("user_uid"),array($uid),"s");

// $states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCommissionOrderHistory.php" />
<link rel="canonical" href="https://agentpnchc.com/userCommissionOrderHistory.php" />
<meta property="og:title" content="<?php echo _ORDER_HISTORY ?> | Pure & Cure" />
<title><?php echo _ORDER_HISTORY ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ORDER_HISTORY ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow overflow-x">
        <div class="width100 overflow-x">

        <?php
        if(isset($_GET['id']))
        {
            $conn = connDB();
            $productADetails = getOrderList($conn," WHERE order_id = ? ", array("order_id") ,array($_GET['id']),"s");
            for($cnt = 0;$cnt < count($productADetails) ;$cnt++)
            {
            ?>

                <table class="width100 tur-table">
                    <thead>
                        <tr>
                            <th><?php echo _TOPUP_NO ?></th>
                            <th><?php echo _STOCK_PRODUCT ?></th>
                            <th><?php echo _ORDER_UNIT_PRICE ?> (RM)</th>
                            <th><?php echo _STOCK_QUANTITY ?></th>
                            <th><?php echo _ORDER_ORIGINAL_PRICE ?> (RM)</th>
                            <th><?php echo _ORDER_DISCOUNT ?></th>
                            <th><?php echo _ORDER_SUBTOTAL ?> (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td>
                                <?php 
                                    $productName = $productADetails[$cnt]->getProductName();
                                    if($productName == 'Product A')
                                    {
                                        $renameProductName = 'Colloid Plus';
                                    }
                                    elseif($productName == 'Product B')
                                    {
                                        $renameProductName = 'Eye Love Oil';
                                    }
                                    else
                                    {
                                        $renameProductName = $productName;
                                    }
                                    echo $renameProductName;
                                ?>
                            </td>
                            <td><?php echo $productADetails[$cnt]->getOriginalPrice();?></td>
                            <td><?php echo $productADetails[$cnt]->getQuantity();?></td>

                            <!-- <td><?php //echo $productADetails[$cnt]->getFinalPrice();?></td> -->
                            <?php $finalPrice = $productADetails[$cnt]->getFinalPrice();?>
                            <td><?php echo number_format("$finalPrice",2);?></td>

                            <!-- <td><?php //echo $productADetails[$cnt]->getDiscount();?></td> -->
                            <?php 
                                $discount = $productADetails[$cnt]->getDiscount();
                                if($discount == 0)
                                {
                                    $renameDiscount = 0;
                                }
                                else
                                {
                                    $renameDiscount = $discount;
                                }
                            ?>
                            <td><?php echo number_format("$renameDiscount",2);?></td>

                            <!-- <td><?php //echo $productADetails[$cnt]->getTotalPrice();?></td> -->
                            <?php $totalPrice = $productADetails[$cnt]->getTotalPrice();?>
                            <td><?php echo number_format("$totalPrice",2);?></td>
                        </tr>
                    </tbody>
                </table>

            <?php
            }
            ?>
        <?php
        }
        ?>

		</div>
		<div class="clear"></div>
    </div>

</div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>