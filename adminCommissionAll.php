<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $allUser = getUser($conn, " WHERE user_type = 1 ");
$commissionDetails = getBonus($conn, " ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminCommissionAll.php" />
<link rel="canonical" href="https://agentpnchc.com/adminCommissionAll.php" />
<meta property="og:title" content="<?php echo _COMMISSION ?> | Pure & Cure" />
<title><?php echo _COMMISSION ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _COMMISSION ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
        <div class="width100 overflow-x">
            <table class="width100 tur-table">
                <thead>
                    <tr>
                        <th><?php echo _TOPUP_NO ?></th>
                        <th><?php echo _COMMISSION_ORDER_ID ?></th>
                        <th><?php echo _COMMISSION_PURCHASER ?></th>
                        <th><?php echo _STOCK_AMOUNT ?></th>
                        <th><?php echo _STOCK_RECEIVER_NAME ?></th>
                        <th><?php echo _COMMISSION_TYPE ?></th>
                        <th><?php echo _TOPUP_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($commissionDetails)
                        {
                            for($cnt = 0;$cnt < count($commissionDetails) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $commissionDetails[$cnt]->getOrderUid();?></td>
                                    <td><?php echo $commissionDetails[$cnt]->getUsername();?></td>
                                    <td><?php echo $commissionDetails[$cnt]->getAmount();?></td>
                                    <td><?php echo $commissionDetails[$cnt]->getReceiver();?></td>
                                    <td><?php echo $commissionDetails[$cnt]->getBonusType();?></td>
                                    <td><?php echo $commissionDetails[$cnt]->getDateCreated();?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>