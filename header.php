<?php
if(isset($_SESSION['uid']))
{
  ?>
  <?php
  if($_SESSION['user_type'] == 0)
  //admin
  {
  ?>

    <div class="page-wrapper">
      <a id="show-sidebar" >
        <img src="img/menu.png" class="menu-icon opacity-hover pointer" alt="<?php echo _PROFILE_MENU ?>" title="<?php echo _PROFILE_MENU ?>">
      </a>
      <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
          <div class="sidebar-brand">
            <div id="close-sidebar">
              <img src="img/close.png" class="menu-icon opacity-hover pointer close-icon" alt="<?php echo _PROFILE_CLOSE_MENU ?>" title="<?php echo _PROFILE_CLOSE_MENU ?>">
            </div>
          </div>

          <div class="sidebar-menu">
            <ul>
            
           <li class="sidebar-dropdown padding-tb10 pointer">
                 <a href="adminDashboard.php" class="margin-top-3"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _PROFILE_DASHBOARD ?></span></a> 
            </li>
              

            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_ANNOUNCEMENT ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="adminAnnoucementAll.php" class="white-to-tur"><?php echo _PROFILE_ANNOUNCEMENT_ALL ?></a>
                  </li>
                  <li>
                    <a href="adminAnnoucementAdd.php" class="white-to-tur"><?php echo _PROFILE_ANNOUNCEMENT_ADD ?></a>
                  </li>
                </ul>
              </div>
            </li>    
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _TOPUP_CREDIT ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="adminCreditPurchasePending.php" class="white-to-tur"><?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?></a>
                  </li>
                 <li>
                    <a href="adminCreditPurchaseHistory.php" class="white-to-tur"><?php echo _PROFILE_PURCHASE_CREDIT_HISTORY ?></a>
                  </li>
                </ul>
              </div>
            </li>                
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _STOCK_PRODUCT ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="adminProductAll.php" class="white-to-tur"><?php echo _PROFILE_PRODUCT_PACKAGE ?></a>
                  </li>
<!--                  <li>
                    <a href="adminProductAdd.php" class="white-to-tur"><?php echo _ADMIN_ADD_PRODUCT ?></a>
                  </li>-->
                </ul>
              </div>
            </li> 

            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _COMMISSION ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="adminCommissionAll.php" class="white-to-tur"><?php echo _COMMISSION_FLOW ?></a>
                  </li>
                 <li>
                    <a href="adminWithdrawalPending.php" class="white-to-tur"><?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?></a>
                  </li>
                 <li>
                    <a href="adminWithdrawalHistory.php" class="white-to-tur"><?php echo WITHDRAWAL_HISTORY ?></a>
                  </li>   
                 <li>
                    <a href="adminWithdrawalTotalCommission.php" class="white-to-tur"><?php echo _COMMISSION_TOTAL_WITHDRAWAL ?></a>
                  </li>                                  
                </ul>
              </div>
            </li>     

            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _ADMIN_DELIVERY ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="adminStockDeliverPending.php" class="white-to-tur"><?php echo _PROFILE_DELIVER_STOCK_REQUEST ?></a>
                  </li>
                 <li>
                    <a href="adminStockDeliverHistory.php" class="white-to-tur"><?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?></a>
                  </li>                              
                </ul>
              </div>
            </li>               
            
             <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="adminSalesAll.php" class="margin-top-3"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _PROFILE_SALES_DASHBOARD ?></span></a> 
            </li> 
            
           <li class="sidebar-dropdown padding-tb10 pointer">
                 <a href="adminMemberAll.php" class="margin-top-3"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _ADMIN_TOTAL_MEMBER ?></span></a> 
            </li>
                                     
              <li class="sidebar-dropdown padding-tb10 pointer">
                <p class="margin-0 swing-hover">
                  <span><img src="img/more.png" class="more-icon swing"></span>
                  <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_LANGUAGE ?></span></a>
                </p>
                <div class="sidebar-submenu">
                  <ul>
                    <li>
                      <a href="<?php $link ?>?lang=en" class="white-to-tur">English</a>
                    </li>
                    <li>
                      <a href="<?php $link ?>?lang=ch" class="white-to-tur">中文</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="sidebar-dropdown padding-tb10 pointer">
                  <a href="logout.php"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _INDEX_LOGOUT ?></span></a>
              </li>          
              
            </ul>
          </div>

        </div>
      </nav>
    </div>

  <?php
  }
  elseif($_SESSION['user_type'] == 1)
  //user
  {
  ?>

  <div class="page-wrapper">
    <a id="show-sidebar" >
      <img src="img/menu.png" class="menu-icon opacity-hover pointer" alt="<?php echo _PROFILE_MENU ?>" title="<?php echo _PROFILE_MENU ?>">
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
      <div class="sidebar-content">
        <div class="sidebar-brand">
          <div id="close-sidebar">
            <img src="img/close.png" class="menu-icon opacity-hover pointer close-icon" alt="<?php echo _PROFILE_CLOSE_MENU ?>" title="<?php echo _PROFILE_CLOSE_MENU ?>">
          </div>
        </div>

        <div class="sidebar-menu">
          <ul>
            <li class="sidebar-dropdown padding-tb10 pointer swing-hover">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_HOME_DASHBOARD ?></span></a>
              </p>
              
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="userDashboard.php" class="white-to-tur"><?php echo _PROFILE_DASHBOARD ?></a>
                  </li>
                  <li>
                    <a href="profile.php" class="white-to-tur"><?php echo _PROFILE ?></a>
                  </li>
                  <li>
                    <a href="userAnnouncement.php" class="white-to-tur"><?php echo _PROFILE_ANNOUNCEMENT ?></a>
                  </li>
                </ul>
              </div>          
            </li>
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_TOTAL_CREDIT ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                  
                    <a href="userCreditTotal.php" class="white-to-tur"><?php echo _PROFILE_TOTAL_CREDIT ?></a>
                  </li>
                  <!-- <li>
                    <a href="userCreditHistory.php" class="white-to-tur"><?php echo _PROFILE_CREDIT_HISTORY ?></a>
                  </li>            -->
                  <li>
                    <a href="userCreditPurchase.php" class="white-to-tur"><?php echo _PROFILE_PURCHASE_CREDIT ?></a>
                  </li>
                  <li>
                    <a href="userCreditPurchaseHistory.php" class="white-to-tur"><?php echo _PROFILE_PURCHASE_CREDIT_HISTORY ?></a>
                  </li>
                  <li>
                    <a href="userCreditConvert.php" class="white-to-tur"><?php echo _COMMISSION_CONVERT ?></a>
                  </li>
                  <li>
                    <a href="userCreditConvertHistory.php" class="white-to-tur"><?php echo _COMMISSION_WALLET_RECORD ?></a>
                  </li>
                </ul>
              </div>
            </li>
            
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_STOCK_INVENTORY ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="stockInventory.php" class="white-to-tur"><?php echo _PROFILE_STOCK_INVENTORY ?></a>
                  </li>
                  <li>
                    <a href="productPackage.php" class="white-to-tur"><?php echo _PROFILE_ADD_STOCK ?></a>
                  </li>
                  <li>
                    <a href="viewShoppingCart.php" class="white-to-tur"><?php echo _STOCK_CART ?></a>
                  </li>   
                  <li>
                    <a href="userStockTransferHistory.php" class="white-to-tur"><?php echo _PROFILE_TRANSFER_STOCK_HISTORY ?></a>
                  </li>
                  <li>
                    <a href="userStockReceivedHistory.php" class="white-to-tur"><?php echo _PROFILE_RECEIVED_STOCK_HISTORY ?></a>
                  </li>
                  <li>
                    <a href="userStockDeliveryHistory.php" class="white-to-tur"><?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?></a>
                  </li>
                </ul>
              </div>
            </li>         
            
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_PRODUCT_PACKAGE ?></span></a>
              
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="userProduct.php" class="white-to-tur"><?php echo _PROFILE_PRODUCT_PACKAGE ?></a>
                  </li>              
                  <!-- <li>
                    <a href="purchaseProduct.php" class="white-to-tur"><?php echo _PROFILE_PURCHASE_PRODUCT ?></a>
                  </li> -->
                  <li>
                    <a href="userProductOrderHistory.php" class="white-to-tur"><?php echo _PROFILE_ALL_ORDER ?></a>
                  </li>
                  <!-- <li>
                    <a href="currentOrder.php" class="white-to-tur"><?php echo _PROFILE_CURRENT_ORDER ?></a>
                  </li> 
                  <li>
                    <a href="completedOrder.php" class="white-to-tur"><?php echo _PROFILE_COMPLETED_ORDER ?></a>
                  </li>                                -->
                </ul>
              </div>
            </li>      
            <li class="sidebar-dropdown padding-tb10 pointer">
            
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _COMMISSION ?></span></a>
              </p>
              <div class="sidebar-submenu">
                <ul>
                                <li>
                    <a href="userCommissionReceivedHistory.php" class="white-to-tur"><?php echo _COMMISSION_RECEIVED ?></a>
                  </li>
                <li>
                    <a href="userCreditWithdrawal.php" class="white-to-tur"><?php echo _COMMISSION_WITHDRAWAL ?></a>
                  </li>
                  <li>
                    <a href="userCreditWithdrawalHistory.php" class="white-to-tur"><?php echo _COMMISSION_WITHDRAWAL_HISTORY ?></a>
                  </li>
				</ul>
            </li>
<!--          <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_DOWNLINE_DASHBOARD ?></span></a>
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="userDownline.php" class="white-to-tur"><?php echo _PROFILE_DOWNLINE_DASHBOARD ?></a>
                    <!-- <a href="#" class="white-to-tur"><?php echo _PROFILE_DOWNLINE_DASHBOARD ?></a> 
                  </li>
                  <!-- <li>
                  <a href="addMember.php" class="white-to-tur"><?php echo _PROFILE_ADD_NEW_MEMBER ?></a>
                  </li> 
                </ul>
              </div>
            </li>-->
            <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="userDownline.php" class="margin-top-3"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _PROFILE_DOWNLINE_DASHBOARD ?></span></a> 
            </li>            
            <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="userSalesDashboard.php" class="margin-top-3"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _PROFILE_SALES_DASHBOARD ?></span></a> 
            </li>
            <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="faq.php" class="margin-top-3"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _HEADER_FAQ ?></span></a> 
            </li>            
            <li class="sidebar-dropdown padding-tb10 pointer">
              <p class="margin-0 swing-hover">
                <span><img src="img/more.png" class="more-icon swing"></span>
                <a class="width-auto margin-top-3"><span class="bold-menu white-to-tur"><?php echo _PROFILE_LANGUAGE ?></span></a>
              </p>
              <div class="sidebar-submenu">
                <ul>
                  <li>
                    <a href="<?php $link ?>?lang=en" class="white-to-tur">English</a>
                  </li>
                  <li>
                    <a href="<?php $link ?>?lang=ch" class="white-to-tur">中文</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="sidebar-dropdown padding-tb10 pointer">
                <a href="logout.php"><span class="bold-menu white-to-tur extra-margin-left"><?php echo _INDEX_LOGOUT ?></span></a>
            </li>          
            
          </ul>
        </div>

      </div>
    </nav>
  </div>

  <?php
  }
  ?>
  <?php
}
else
{
  ?>
  <?php
}
?>