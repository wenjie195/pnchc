<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/success.php" />
<link rel="canonical" href="https://agentpnchc.com/success.php" />
<meta property="og:title" content="<?php echo _SUCCESS_CREATE_ACC_SUCCESS ?> | Pure & Cure" />
<title><?php echo _SUCCESS_CREATE_ACC_SUCCESS ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding dark-tur-bg min-height100 padding-top">
	<p class="text-center"><img src="img/happy.png"  class="center-logo" alt="<?php echo _SUCCESS_CREATE_ACC_SUCCESS ?>" title="<?php echo _SUCCESS_CREATE_ACC_SUCCESS ?>"></p>
    <h1 class="h1 white-text text-center login-h1 margin-bottom0"><?php echo _INDEX_WELCOME ?> <b>Pure & Cure</b></h1>
    <p class="small-success white-text text-center"><?php echo _SUCCESS_CREATE ?></p>
    <div class="white-border margin-auto"></div>
    <p class="text-center"><img src="img/tick.png"  class="center-logo2" alt="<?php echo _SUCCESS_CREATE_ACC_SUCCESS ?>" title="<?php echo _SUCCESS_CREATE_ACC_SUCCESS ?>"></p>
    
   
</div>




<style>
::-webkit-input-placeholder { /* Edge */
  color: white;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: white;
}

::placeholder {
  color: white;
}
</style>


<?php include 'js.php'; ?>
</body>
</html>