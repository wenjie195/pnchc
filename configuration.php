<?php
/**
 * Instruction:
 *
 * 1. Replace the APIKEY with your API Key.
 * 2. OPTIONAL: Replace the COLLECTION with your Collection ID.
 * 3. Replace the X_SIGNATURE with your X Signature Key
 * 4. Replace the http://www.google.com/ with your FULL PATH TO YOUR WEBSITE. It must be end with trailing slash "/".
 * 5. Replace the http://www.google.com/success.html with your FULL PATH TO YOUR SUCCESS PAGE. *The URL can be overridden later
 * 6. OPTIONAL: Set $amount value.
 * 7. OPTIONAL: Set $fallbackurl if the user are failed to be redirected to the Billplz Payment Page.
 *
 */
// $api_key = 'APIKEY';
// $collection_id = 'COLLECTION';
// $x_signature = 'X_SIGNATURE';

$api_key = 'dc8b2c92-96ac-45e7-bd2b-df7628729562';
$collection_id = 'COLLECTION';
$x_signature = 'S-v1Q2xGC5QXikPIw_VguOHg';

//real API
// $api_key = 'c3ad22a5-963b-48fe-b676-28b0ff76f47a';
// $collection_id = 'COLLECTION';
// $x_signature = 'S-9rsHJj5acGSTlFtoxR8QVA';

// $websiteurl = 'http://www.google.com';
// $successpath = 'http://www.google.com/success.html';

$websiteurl = 'http://localhost/pnchc';
$successpath = 'http://localhost/pnchc/successPayment.php';

// //server
// $websiteurl = "http://".$fullPath;
// $successpath = $websiteurl.'/successPayment.php';

// $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
// $fullPath = dirname($path);

// // $websiteurl = 'http://localhost/samofa02';
// $websiteurl = "https://".$fullPath;
// $successpath = $websiteurl.'/successPayment.php';

$amount = ''; //Example (RM13.50): $amount = '1350';
$fallbackurl = ''; //Example: $fallbackurl = 'http://www.google.com/pay.php';
$description = 'PAYMENT DESCRIPTION';
// $reference_1_label = '';
// $reference_2_label = '';
$reference_1_label = 'Product';
$reference_2_label = 'Order ID';