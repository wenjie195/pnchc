<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $creditDetails = getWithdrawal($conn, " WHERE status != 'Pending' ORDER BY date_created DESC ");
$creditDetails = getWithdrawal($conn, " WHERE status = 'APPROVED' ORDER BY date_created DESC ");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminWithdrawalTotalCommission.php" />
<link rel="canonical" href="https://agentpnchc.com/adminWithdrawalTotalCommission.php" />
<meta property="og:title" content="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAW ?> | Pure & Cure" />
<title><?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAW ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAW ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
	
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    	<div class="center-div2">
            <div class="clear"></div>
            <h3 class="center-div-h3"><?php echo _COMMISSION_TOTAL_WITHDRAWAL ?></h3>  

            <?php
            if($creditDetails)
            {
                $totalAmount = 0; // initital
                for ($b=0; $b <count($creditDetails) ; $b++)
                {
                    $totalAmount += $creditDetails[$b]->getAmount();
                }
            }
            ?>

            <?php
            if(!$creditDetails)
            {
            ?>
            
            <?php
            }
            else
            {
            ?>
                <h1 class="white-text value-h1"><?php echo $totalAmount;?></h1> 
            <?php
            }
            ?>

            <!-- <h1 class="white-text value-h1"><?php echo $totalAmount;?></h1>  -->
            <div class="clear"></div>
       </div>
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
        	<thead>
            	<tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _INDEX_USERNAME ?></th>
                    <th><?php echo _WITHDRAW_AMOUNT ?></th>
                    <th><?php echo _TOPUP_STATUS ?></th>
                    <th><?php echo _TOPUP_DATE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($creditDetails)
                    {
                        for($cnt = 0;$cnt < count($creditDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>
                                    <?php 
                                        $userUid = $creditDetails[$cnt]->getUid();
                                        $conn = connDB();
                                        $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
                                        echo $usename = $userDetails[0]->getUsername();
                                    ?>
                                </td>
                                <td><?php echo $creditDetails[$cnt]->getAmount();?></td>
                                <td><?php echo $creditDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $creditDetails[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

   
</div></div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>