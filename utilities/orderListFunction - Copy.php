<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];
$timestamp = time();

// function addOrderList($conn,$orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalDiscount)
// {
//      if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","final_price","quantity","totalPrice","status","main_product_uid","discount"),
//           array($orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalDiscount),"ssssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addOrderList($conn,$orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$finalPrice,$status,$mainProductUid,$totalDiscount,$totalPrice)
{
     if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","original_price","quantity","final_price","status","main_product_uid","discount","totalPrice"),
          array($orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$finalPrice,$status,$mainProductUid,$totalDiscount,$totalPrice),"sssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


// --------------- Commission Product A Lvl 1 Start --------------- //
function commissionProductALvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlAOne,$bonusLvlAOneName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlAOne,$bonusLvlAOneName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product A Lvl 1 End--------------- //

// --------------- Commission Product A Lvl 2 Start --------------- //
function commissionProductALvlTwo($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlATwo,$bonusLvlATwoName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlATwo,$bonusLvlATwoName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product A Lvl 2 End --------------- //


// --------------- Commission Product B Lvl 1 Start --------------- //
function commissionProductBLvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBOne,$bonusLvlBOneName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBOne,$bonusLvlBOneName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product B Lvl 1 End--------------- //

// --------------- Commission Product B Lvl 2 Start --------------- //
function commissionProductBLvlTwo($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBTwo,$bonusLvlBTwoName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBTwo,$bonusLvlBTwoName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product B Lvl 2 End --------------- //


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
     echo $userValueA = $userDetails[0]->getValueA();
     echo "<br>";
     echo $userValueB = $userDetails[0]->getValueB();
     echo "<br>";
     echo $userCurrentRanking = $userDetails[0]->getRank();
     echo "<br>";
     echo $userCurrentRankA = $userDetails[0]->getRankA();
     echo "<br>";
     echo $userCurrentRankB = $userDetails[0]->getRankB();
     echo "<br>";
     echo $userCurrentCredit = $userDetails[0]->getCredit();
     echo "<br>";

     // --------------- Find 1st Level Direct Upline Details Start --------------- //
     $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
     // Direct Upline
     echo "1st Level Upline : ";  
     echo $userUplineUid = $userRH[0]->getReferrerId(); //william
     echo "<br>";  
     $uplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUplineUid),"s");
     $uplineUsername = $uplineDetails[0]->getUsername();
     echo $uplineRankA = $uplineDetails[0]->getRankA();
     echo "<br>";  
     echo $uplineRankB = $uplineDetails[0]->getRankB();
     echo "<br>";  
     echo $uplineWalletA = $uplineDetails[0]->getWalletA();
     echo "<br>";  
     echo $uplineWalletB = $uplineDetails[0]->getWalletB();
     echo "<br>";  
     // --------------- Find Direct Upline Details End --------------- //


     // --------------- Find 2nd Level Upline Details Start --------------- //
     $upline2ndLvl = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($userUplineUid),"s");
     // 2nd Level Upline
     echo "2nd Level Upline : ";  
     echo $upline2ndLvlUid = $upline2ndLvl[0]->getReferrerId();
     echo "<br>";  
     $upline2ndLvlDetails = getUser($conn, "WHERE uid =?",array("uid"),array($upline2ndLvlUid),"s");
     $upline2ndLvlUsername = $upline2ndLvlDetails[0]->getUsername();
     echo $upline2ndLvlRankA = $upline2ndLvlDetails[0]->getRankA();
     echo "<br>";  
     echo $upline2ndLvlRankB = $upline2ndLvlDetails[0]->getRankB();
     echo "<br>";  
     echo $upline2ndLvlWalletA = $upline2ndLvlDetails[0]->getWalletA();
     echo "<br>";  
     echo $upline2ndLvlWalletB = $upline2ndLvlDetails[0]->getWalletB();
     echo "<br>";  
     // --------------- Find 2nd Level Upline Details End --------------- //

     
     $preOrderProduct = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
     // $preOrderProduct = getPreOrderList($conn);
     if ($preOrderProduct) 
     {
       for ($cnt=0; $cnt < count($preOrderProduct) ; $cnt++)
       {
          // echo $productUid = $preOrderProduct[$cnt]->getProductUid();
          // echo "<br>";
          // echo $productName = $preOrderProduct[$cnt]->getProductName();
          // echo "<br>";
          // echo $originalPrice = $preOrderProduct[$cnt]->getOriginalPrice();
          // echo "<br>";
          // echo $quantity = $preOrderProduct[$cnt]->getQuantity();
          // echo "<br>";
          // echo $totalPrice = $preOrderProduct[$cnt]->getTotalPrice();
          // echo "<br>";
     
          $productUid = $preOrderProduct[$cnt]->getProductUid();
          echo $productName = $preOrderProduct[$cnt]->getProductName();
          echo "<br>";
          // $unitPrice = $preOrderProduct[$cnt]->getOriginalPrice();
          echo $quantity = $preOrderProduct[$cnt]->getQuantity();
          echo "<br>";
          $productOrderId = $preOrderProduct[$cnt]->getId();
          $mainProductUid = $preOrderProduct[$cnt]->getMainProductUid();

          if($productName == 'Product A')
          {
               // $productDetails = getVariation($conn, "WHERE product_uid = ? ",array("product_uid"),array($productUid),"s");
               // $productData = $productDetails[0];

               if($quantity >= 300)
               {
                    $unitPrice = '85';
                    $level = '5';
               }
               elseif($quantity >= 100)
               {
                    $unitPrice = '95';
                    $level = '4';
               }
               elseif($quantity >= 50)
               {
                    $unitPrice = '105';
                    $level = '3';
               }
               elseif($quantity >= 10)
               {
                    $unitPrice = '120';
                    $level = '2';
               }
               else
               {
                    $unitPrice = '148';
                    $level = '1';
               }
          }
          elseif($productName == 'Product B')
          {
               if($quantity >= 300)
               {
                    $unitPrice = '199';
                    $level = '4';
                    $profit = '80';
               }
               elseif($quantity >= 100)
               {
                    $unitPrice = '199';
                    $level = '3';
                    $profit = '60';
               }
               elseif($quantity >= 50)
               {
                    $unitPrice = '199';
                    $level = '2';
                    $profit = '40';
               }
               else
               {
                    $unitPrice = '199';
                    $level = '1';
                    $profit = '30';
               }
          }
          else
          {
               echo "Unknown Product";
          }

          //calculate price and deduct credit
          echo $finalPrice = ($quantity * $unitPrice);
          echo "<br>";

          if($userCurrentCredit >= $finalPrice)
          {
               if($userCurrentRankA > $level)
               {
                    $renewRankA = $userCurrentRankA;
               }
               elseif($level > $userCurrentRankA)
               {
                    $renewRankA = $level;
               }

               if($productName == 'Product B')
               {
                    echo $totalDiscount = ($profit * $quantity);
                    echo "<br>";

                    if($userCurrentRankB > $level)
                    {
                         $renewRankB = $userCurrentRankB;
                    }
                    elseif($level > $userCurrentRankB)
                    {
                         $renewRankB = $level;
                    }

                    echo $remainCredit = $userCurrentCredit - $finalPrice + $totalDiscount;
                    echo "<br>";
                    echo $totalPrice = $finalPrice - $totalDiscount;

               }
               else
               {
                    echo $remainCredit = $userCurrentCredit - $finalPrice;
                    echo "<br>";
                    echo $totalDiscount = NULL ;
                    echo "<br>";
                    echo $totalPrice = $finalPrice;
               }
          }
          else
          {
               echo "Insufficent Credit <br> Please Reload !!";
               echo "<br>";
          }

          // $totalPrice = ($quantity * $unitPrice);

          if($productName == 'Product A')
          {
               echo $renewValueA = $userValueA + $quantity;
               echo "<br>";
          }
          elseif($productName == 'Product B')
          {
               echo $renewValueB = $userValueB + $quantity;
               echo "<br>";
          }

          // echo $renewWalletA = $userWalletA + $quantity;
          // echo "<br>";

          $preOrderStatus = "Sold";
          $orderUid = $uid.$timestamp;
          $_SESSION['order_uid'] = $orderUid;
          $status = "Pending";

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($preOrderStatus)
          {
              array_push($tableName,"status");
              array_push($tableValue,$preOrderStatus);
              $stringType .=  "s";
          }    
          array_push($tableValue,$productOrderId);
          $stringType .=  "s";
          $updateBonusPool = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
          if($updateBonusPool)
          {
               if(addOrderList($conn,$orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$finalPrice,$status,$mainProductUid,$totalDiscount,$totalPrice))
               {
                    if($productName == 'Product A')
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($renewValueA)
                         {
                             array_push($tableName,"value_a");
                             array_push($tableValue,$renewValueA);
                             $stringType .=  "s";
                         }    
                         if($remainCredit)
                         {
                             array_push($tableName,"credit");
                             array_push($tableValue,$remainCredit);
                             $stringType .=  "d";
                         }    
                         if($renewRankA)
                         {
                             array_push($tableName,"rank_a");
                             array_push($tableValue,$renewRankA);
                             $stringType .=  "d";
                         }    
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($updateBonusPool)
                         {
                              // header('Location: ../viewCheckoutList.php');

                              echo $userUplineUid;
                              echo "<br>";  
                              echo $uplineUsername;
                              echo "<br>";  
                              echo $uplineWalletA;
                              echo "<br>";  

                              if($quantity >= 300)
                              {
                                   if($uplineRankA > 4)
                                   //means upline rank is 5
                                   {
                                        if($uplineRankA == $userCurrentRankA)
                                        //check same ranking
                                        {
     
                                        }
                                        else
                                        {
                                             header('Location: ../viewCheckoutList.php');
                                        }
                                   }
                                   else
                                   {
                                        header('Location: ../viewCheckoutList.php');
                                   }
                              }
                              elseif($quantity >= 50)
                              {
                                   if($uplineRankA >= 3)
                                   //means upline rank is 3 and 4
                                   {
                                        if($uplineRankA == $userCurrentRankA)
                                        //check same ranking
                                        {
     
                                        }
                                        else
                                        {
                                             header('Location: ../viewCheckoutList.php');
                                        }
                                   }
                                   else
                                   {
                                        header('Location: ../viewCheckoutList.php');
                                   }
                              }
                              else
                              {
                                   header('Location: ../viewCheckoutList.php');
                              }

                         }
                         else
                         {
                              echo "fail renew wallet a";
                         }
                    }
                    elseif($productName == 'Product B')
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($renewValueB)
                         {
                             array_push($tableName,"value_b");
                             array_push($tableValue,$renewValueB);
                             $stringType .=  "s";
                         }    
                         if($remainCredit)
                         {
                             array_push($tableName,"credit");
                             array_push($tableValue,$remainCredit);
                             $stringType .=  "d";
                         }    
                         if($renewRankB)
                         {
                             array_push($tableName,"rank_b");
                             array_push($tableValue,$renewRankB);
                             $stringType .=  "d";
                         }   
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($updateBonusPool)
                         {
                              header('Location: ../viewCheckoutList.php');
                         }
                         else
                         {
                              echo "fail renew wallet b";
                         }
                    }
                    else
                    {
                         echo "Unknown Product !! FAIL TO UPDATE";
                    }
               }
               else
               {
                    echo "fail to add product into order list";
               }
          }
          else
          {
               echo "FAIL TO CLEAR  ORDER LIST";
          }

       }
     }

}
else 
{
     header('Location: ../index.php');
}
?>