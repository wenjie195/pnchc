<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = $_SESSION['uid'];

    $fullname = rewrite($_POST['update_fullname']);
    $email = rewrite($_POST['update_email']);
    $phone = rewrite($_POST['update_phone']);
    $icno = rewrite($_POST['update_icno']);
    $wechatId = rewrite($_POST['update_wechatid']);
    // $username = rewrite($_POST['update_username']);
    $nationality = rewrite($_POST['update_nationality']);

    $address = rewrite($_POST['update_address']);
    $bankName = rewrite($_POST['update_bank']);
    $bankAccountHolder = rewrite($_POST['update_bank_holder']);
    $bankAccountNo = rewrite($_POST['update_bank_account']);

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

    // $icRows = getUser($conn," WHERE icno = ? ",array("icno"),array($icno),"s");
    // $icDetails = $icRows[0];

    if(!$user)
    {
        $checkRenewPassword = rewrite($_POST["register_password"]);
        $checkRetypePassword = rewrite($_POST["register_retype_password"]);
        if($checkRenewPassword != '' && $checkRetypePassword != '')
        {
            $register_password = rewrite($_POST["register_password"]);
            $register_password_validation = strlen($register_password);
            $retypePassword = rewrite($_POST["register_retype_password"]);
    
            $password = hash('sha256',$register_password);
            $salt = substr(sha1(mt_rand()), 0, 100);
            $finalPassword = hash('sha256', $salt.$password);
    
            if($register_password == $retypePassword)
            {
                if($register_password_validation >= 6)
                {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($fullname)
                    {
                        array_push($tableName,"fullname");
                        array_push($tableValue,$fullname);
                        $stringType .=  "s";
                    }
                    if($email)
                    {
                        array_push($tableName,"email");
                        array_push($tableValue,$email);
                        $stringType .=  "s";
                    }
                    if($phone)
                    {
                        array_push($tableName,"phone_no");
                        array_push($tableValue,$phone);
                        $stringType .=  "s";
                    }
                    if($icno)
                    {
                        array_push($tableName,"icno");
                        array_push($tableValue,$icno);
                        $stringType .=  "s";
                    }
                    if($wechatId)
                    {
                        array_push($tableName,"wechat_id");
                        array_push($tableValue,$wechatId);
                        $stringType .=  "s";
                    }
                    // if($username)
                    // {
                    //     array_push($tableName,"username");
                    //     array_push($tableValue,$username);
                    //     $stringType .=  "s";
                    // }
                    if($nationality)
                    {
                        array_push($tableName,"country");
                        array_push($tableValue,$nationality);
                        $stringType .=  "s";
                    }

                    if($address)
                    {
                        array_push($tableName,"address");
                        array_push($tableValue,$address);
                        $stringType .=  "s";
                    }
                    if($bankName)
                    {
                        array_push($tableName,"bank_name");
                        array_push($tableValue,$bankName);
                        $stringType .=  "s";
                    }
                    if($bankAccountHolder)
                    {
                        array_push($tableName,"bank_acc_name");
                        array_push($tableValue,$bankAccountHolder);
                        $stringType .=  "s";
                    }
                    if($bankAccountNo)
                    {
                        array_push($tableName,"bank_acc_number");
                        array_push($tableValue,$bankAccountNo);
                        $stringType .=  "s";
                    }

                    if($finalPassword)
                    {
                        array_push($tableName,"password");
                        array_push($tableValue,$finalPassword);
                        $stringType .=  "s";
                    }
                    if($salt)
                    {
                        array_push($tableName,"salt");
                        array_push($tableValue,$salt);
                        $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                        // echo "success with password";
                        header('Location: ../profile.php');
                    }
                    else
                    {
                        // echo "fail";
                        echo "<script>alert('fail to update profile !');window.location='../editProfile.php'</script>";
                    }
                }
                else
                {
                    echo "<script>alert('password length must more than 5 !!'); window.history.go(-1);</script>";
                }
            }
            else
            {
                echo "<script>alert('password does not match with retype-password !!'); window.history.go(-1);</script>";
            }
        }
        else
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($fullname)
            {
                array_push($tableName,"fullname");
                array_push($tableValue,$fullname);
                $stringType .=  "s";
            }
            if($email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$email);
                $stringType .=  "s";
            }
            if($phone)
            {
                array_push($tableName,"phone_no");
                array_push($tableValue,$phone);
                $stringType .=  "s";
            }
            if($icno)
            {
                array_push($tableName,"icno");
                array_push($tableValue,$icno);
                $stringType .=  "s";
            }
            if($wechatId)
            {
                array_push($tableName,"wechat_id");
                array_push($tableValue,$wechatId);
                $stringType .=  "s";
            }
            // if($username)
            // {
            //     array_push($tableName,"username");
            //     array_push($tableValue,$username);
            //     $stringType .=  "s";
            // }
            if($nationality)
            {
                array_push($tableName,"country");
                array_push($tableValue,$nationality);
                $stringType .=  "s";
            }

            if($address)
            {
                array_push($tableName,"address");
                array_push($tableValue,$address);
                $stringType .=  "s";
            }
            if($bankName)
            {
                array_push($tableName,"bank_name");
                array_push($tableValue,$bankName);
                $stringType .=  "s";
            }
            if($bankAccountHolder)
            {
                array_push($tableName,"bank_acc_name");
                array_push($tableValue,$bankAccountHolder);
                $stringType .=  "s";
            }
            if($bankAccountNo)
            {
                array_push($tableName,"bank_acc_number");
                array_push($tableValue,$bankAccountNo);
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success without password";
                header('Location: ../profile.php');
            }
            else
            {
                // echo "fail";
                echo "<script>alert('fail to update profile !');window.location='../editProfile.php'</script>";
            }
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../editProfile.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}
?>