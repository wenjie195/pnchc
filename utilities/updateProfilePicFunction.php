<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];
$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     $target_dir = "../profileImage/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");    

     // if(!$user)
     if($user)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($imageOne)
          {
               array_push($tableName,"image");
               array_push($tableValue,$imageOne);
               $stringType .=  "s";
          }

          array_push($tableValue,$uid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../profile.php?type=4');
               header('Location: ../profile.php');
          }
          else
          {
               echo "FAIL";
               // $_SESSION['messageType'] = 1;
               // header('Location: ../updateProfilePic.php?type=2');
          }
     }
     else
     {
          echo "ERROR";
          // $_SESSION['messageType'] = 1;
          // header('Location: ../updateProfilePic.php?type=3');
     }
}
else 
{
    header('Location: ../index.php');
}
?>