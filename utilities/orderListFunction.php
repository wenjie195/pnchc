<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];
$timestamp = time();

// function addOrderList($conn,$orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalDiscount)
// {
//      if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","final_price","quantity","totalPrice","status","main_product_uid","discount"),
//           array($orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalDiscount),"ssssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addOrderList($conn,$orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$finalPrice,$status,$mainProductUid,$totalDiscount,$totalPrice)
{
     if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","original_price","quantity","final_price","status","main_product_uid","discount","totalPrice"),
          array($orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$finalPrice,$status,$mainProductUid,$totalDiscount,$totalPrice),"sssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


// --------------- Commission Product A Lvl 1 Start --------------- //
function commissionProductALvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlAOne,$bonusLvlAOneName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlAOne,$bonusLvlAOneName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product A Lvl 1 End--------------- //

// --------------- Commission Product A Lvl 2 Start --------------- //
function commissionProductALvlTwo($conn,$orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlATwo,$bonusLvlATwoName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlATwo,$bonusLvlATwoName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product A Lvl 2 End --------------- //


// --------------- Commission Product B Lvl 1 Start --------------- //
function commissionProductBLvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBOne,$bonusLvlBOneName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBOne,$bonusLvlBOneName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product B Lvl 1 End--------------- //

// --------------- Commission Product B Lvl 2 Start --------------- //
function commissionProductBLvlTwo($conn,$orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlBTwo,$bonusLvlBTwoName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlBTwo,$bonusLvlBTwoName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// --------------- Commission Product B Lvl 2 End --------------- //


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
     echo $userValueA = $userDetails[0]->getValueA();
     echo "<br>";
     echo $userValueB = $userDetails[0]->getValueB();
     echo "<br>";
     
     //additional for product B part 1
     echo $userValueBBuy = $userDetails[0]->getValueBBuy();
     echo "<br>";

     echo $userDateJoin = $userDetails[0]->getDateCreated();
     echo "<br>";
     echo $joinMonth = date("m",strtotime($userDateJoin));
     echo "<br>";
     echo $joinYear = date("Y",strtotime($userDateJoin));
     echo "<br>";

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz));
     $dt->setTimestamp($timestamp);
     echo $currentMonth = $dt->format('m');
     echo "<br>";
     echo $currentYear = $dt->format('Y');
     echo "<br>";
     
     echo $userCurrentRanking = $userDetails[0]->getRank();
     echo "<br>";
     echo $userCurrentRankA = $userDetails[0]->getRankA();
     echo "<br>";
     echo $userCurrentRankB = $userDetails[0]->getRankB();
     echo "<br>";
     echo $userCurrentCredit = $userDetails[0]->getCredit();
     echo "<br>";
     echo $username = $userDetails[0]->getUsername();
     echo "<br>";
     // echo $checkValueB = $userDetails[0]->getValueB();
     // echo "<br>";
     if(!$userValueB)
     {
          $productBCurrentValue = 0 ;
     }
     else
     {
          $productBCurrentValue = $userValueB ;
     }

     //additional for product B part 2
     if(!$userValueBBuy)
     {
          $totalProductBAmount = 0 ;
     }
     else
     {
          $totalProductBAmount = $userValueBBuy ;
     }
     

     // --------------- Find 1st Level Direct Upline Details Start --------------- //
     $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
     // Direct Upline
     echo "1st Level Upline : ";  
     echo $userUplineUid = $userRH[0]->getReferrerId(); //william
     echo "<br>";  
     $uplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUplineUid),"s");
     $uplineUsername = $uplineDetails[0]->getUsername();
     echo $uplineRankA = $uplineDetails[0]->getRankA();
     echo "<br>";  
     echo $uplineRankB = $uplineDetails[0]->getRankB();
     echo "<br>";  
     echo $uplineRankBStatus = $uplineDetails[0]->getMaintenanceB();
     echo "<br>";  
     echo $uplineWalletA = $uplineDetails[0]->getWalletA();
     echo "<br>";  
     echo $uplineWalletB = $uplineDetails[0]->getWalletB();
     echo "<br>";  
     // --------------- Find Direct Upline Details End --------------- //


     // --------------- Find 2nd Level Upline Details Start --------------- //
     $upline2ndLvl = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($userUplineUid),"s");
     // 2nd Level Upline
     echo "2nd Level Upline : ";  
     echo $upline2ndLvlUid = $upline2ndLvl[0]->getReferrerId();
     echo "<br>";  
     $upline2ndLvlDetails = getUser($conn, "WHERE uid =?",array("uid"),array($upline2ndLvlUid),"s");
     $upline2ndLvlUsername = $upline2ndLvlDetails[0]->getUsername();
     echo $upline2ndLvlRankA = $upline2ndLvlDetails[0]->getRankA();
     echo "<br>";  
     echo $upline2ndLvlRankB = $upline2ndLvlDetails[0]->getRankB();
     echo "<br>";  
     echo $upline2ndLvlWalletA = $upline2ndLvlDetails[0]->getWalletA();
     echo "<br>";  
     echo $upline2ndLvlWalletB = $upline2ndLvlDetails[0]->getWalletB();
     echo "<br>";  
     // --------------- Find 2nd Level Upline Details End --------------- //

     
     $preOrderProduct = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
     // $preOrderProduct = getPreOrderList($conn);
     if ($preOrderProduct) 
     {
          for ($cnt=0; $cnt < count($preOrderProduct) ; $cnt++)
          {
               // echo $productUid = $preOrderProduct[$cnt]->getProductUid();
               // echo "<br>";
               // echo $productName = $preOrderProduct[$cnt]->getProductName();
               // echo "<br>";
               // echo $originalPrice = $preOrderProduct[$cnt]->getOriginalPrice();
               // echo "<br>";
               // echo $quantity = $preOrderProduct[$cnt]->getQuantity();
               // echo "<br>";
               // echo $totalPrice = $preOrderProduct[$cnt]->getTotalPrice();
               // echo "<br>";
          
               $productUid = $preOrderProduct[$cnt]->getProductUid();
               echo $productName = $preOrderProduct[$cnt]->getProductName();
               echo "<br>";
               // $unitPrice = $preOrderProduct[$cnt]->getOriginalPrice();
               echo $quantity = $preOrderProduct[$cnt]->getQuantity();
               echo "<br>";
               $productOrderId = $preOrderProduct[$cnt]->getId();
               $mainProductUid = $preOrderProduct[$cnt]->getMainProductUid();

               if($productName == 'Product A')
               {
                    // $productDetails = getVariation($conn, "WHERE product_uid = ? ",array("product_uid"),array($productUid),"s");
                    // $productData = $productDetails[0];

                    if($quantity >= 300)
                    {
                         $unitPrice = '85';
                         $level = '5';
                    }
                    elseif($quantity >= 100)
                    {
                         $unitPrice = '95';
                         $level = '4';
                    }
                    elseif($quantity >= 50)
                    {
                         $unitPrice = '105';
                         $level = '3';
                    }
                    elseif($quantity >= 10)
                    {
                         $unitPrice = '120';
                         $level = '2';
                    }
                    else
                    {
                         $unitPrice = '148';
                         $level = '1';
                    }
               }
               elseif($productName == 'Product B')
               {
                    //additional for product B part 3
                    $totalProductB = $totalProductBAmount + $quantity ;
                    if($totalProductB >= 300)
                    {
                         $unitPrice = '199';
                         echo "Upgrade To Level : ";
                         echo $level = '4';
                         echo "<br>";
                         // $profit = '80';
                         // if($totalProductBAmount >= 300)
                         if($totalProductBAmount >= 299)
                         {               
                              echo "Total Profit : ";
                              echo $profit = $quantity * 80;
                              echo "<br>";
                         }
                         // elseif($totalProductBAmount >= 100)
                         elseif($totalProductBAmount >= 99)
                         {
                              $Q4Alpha = $totalProductB - 299;
                              $profitQ4Alpha = $Q4Alpha * 80;
                              $Q4Beta = $totalProductB - $Q4Alpha - $totalProductBAmount ;
                              $profitQ4Beta = $Q4Beta * 60;
                              $profitQ4Charlie = 0;
                              $profitQ4Delta = 0;
               
                              echo "Total Profit : ";
                              echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
                              echo "<br>";
                         }
                         // elseif($totalProductBAmount >= 50)
                         elseif($totalProductBAmount >= 99)
                         {
                              $Q4Alpha = $totalProductB - 299;
                              $profitQ4Alpha = $Q4Alpha * 80;
                              $Q4Beta = $totalProductB - $Q4Alpha - 99 ;
                              $profitQ4Beta = $Q4Beta * 60;
                              $Q4Charlie = $totalProductB - $Q4Alpha - $Q4Beta - $totalProductBAmount ;
                              $profitQ4Charlie = $Q4Charlie * 40;
                              $profitQ4Delta = 0;
               
                              echo "Total Profit : ";
                              echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
                              echo "<br>";
                         }
                         else
                         {
                              $Q4Alpha = $totalProductB - 299;
                              $profitQ4Alpha = $Q4Alpha * 80;
                              $Q4Beta = $totalProductB - $Q4Alpha - 99 ;
                              $profitQ4Beta = $Q4Beta * 60;
                              $Q4Charlie = $totalProductB - $Q4Alpha - $Q4Beta - 49 ;
                              $profitQ4Charlie = $Q4Charlie * 40;
                              $Q4Delta = $totalProductB - $Q4Alpha - $Q4Beta - $Q4Charlie - $totalProductBAmount ;
                              $profitQ4Delta = $Q4Delta * 30;
               
                              echo "Total Profit : ";
                              echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
                              echo "<br>";
                         }
                    }
                    // elseif($totalProductB >= 100)
                    elseif($totalProductB >= 100 && $totalProductB <= 299)
                    {
                         $unitPrice = '199';
                         echo "Upgrade To Level : ";
                         echo $level = '3';
                         echo "<br>";
                         // $profit = '60';
                         if($totalProductBAmount >= 99)
                         {               
                              echo "Rank 3, Lvl 1";
                              echo "<br>";
                              echo "Total Profit : ";
                              echo $profit = $quantity * 60;
                              echo "<br>";
                         }
                         else
                         {
                              echo $Q3Alpha = $totalProductB - 99;
                              echo "<br>";
                              echo $profitQ3Alpha = $Q3Alpha * 60;
                              echo "<br>";
                              if($totalProductBAmount >= 50)
                              {
                                   echo "Rank 3, AA";
                                   echo "<br>";                                   
                                   $Q3Beta = $totalProductB - $totalProductBAmount - $Q3Alpha;
                                   $profitQ3Beta = $Q3Beta * 40;
                                   $profitQ3Charlie = 0;
                              }
                              else
                              {
                                   echo "Rank 3, BB";
                                   echo "<br>";
                                   $Q3Beta = $totalProductB - $Q3Alpha - 49;
                                   $profitQ3Beta = $Q3Beta * 40;
                                   $Q3Charlie = $totalProductB - $Q3Alpha - $Q3Beta - $totalProductBAmount;
                                   $profitQ3Charlie = $Q3Charlie * 30;
                              }
                              echo "Rank 3, Lvl 2";
                              echo "<br>";
                              echo "Total Profit : ";
                              echo $profit = $profitQ3Alpha + $profitQ3Beta + $profitQ3Charlie;
                              echo "<br>";
                         }
                    }
                    // elseif($totalProductB >= 50)
                    elseif($totalProductB >= 50 && $totalProductB <= 99)
                    {
                         $unitPrice = '199';
                         echo "Upgrade To Level : ";
                         echo $level = '2';
                         echo "<br>";
                         // $profit = '40';
                         if($totalProductBAmount >= 50)
                         {               
                              echo "Rank 2, Lvl 1";
                              echo "<br>";
                              echo "Total Profit : ";
                              echo $profit = $quantity * 40;
                              echo "<br>";
                         }
                         else
                         {
                              $Q2Alpha = $totalProductB - 49;
                              $profitQ2Alpha = $Q2Alpha * 40;
                              $Q2Beta = $totalProductB - $totalProductBAmount - $Q2Alpha;
                              $profitQ2Beta = $Q2Beta * 30;
                              echo "Rank 2, Lvl 2";
                              echo "<br>";
                              echo "Total Profit : ";
                              echo $profit = $profitQ2Alpha + $profitQ2Beta;
                              echo "<br>";
                         }
                    }
                    elseif($totalProductB > 0 && $totalProductB < 50)
                    {
                         $unitPrice = '199';
                         $level = '1';
                         // $profit = '30';               
                         echo "Total Profit : ";
                         echo $profit = $quantity * 30;
                         echo "<br>";
                    }
                    else
                    {    
                         echo "ERROR";
                    }

               }
               elseif($productName == 'Package C')
               {
                    $maintenanceStatusProductC = 'Active';
                    $unitPrice = '540';
               }
               else
               {
                    echo "Unknown Product";
               }

               //calculate price and deduct credit
               echo $finalPrice = ($quantity * $unitPrice);
               echo "<br>";

               // if($finalPrice > $userCurrentCredit)
               // {
               //      $_SESSION['messageType'] = 1;
               //      header('Location: ../stockInventory.php?type=1');
               // }

               if($userCurrentCredit >= $finalPrice)
               {
                    if($userCurrentRankA > $level)
                    {
                         $renewRankA = $userCurrentRankA;
                    }
                    elseif($level > $userCurrentRankA)
                    {
                         $renewRankA = $level;
                    }

                    if($productName == 'Product B')
                    {
                         // echo $totalDiscount = ($profit * $quantity);
                         // echo "<br>";
                         echo $totalDiscount = $profit ;
                         echo "<br>";

                         if($userCurrentRankB > $level)
                         {
                              $renewRankB = $userCurrentRankB;
                         }
                         elseif($level > $userCurrentRankB)
                         {
                              $renewRankB = $level;
                         }

                         echo $remainCredit = $userCurrentCredit - $finalPrice + $totalDiscount;
                         echo "<br>";
                         echo $totalPrice = $finalPrice - $totalDiscount;

                    }
                    else
                    {
                         echo $remainCredit = $userCurrentCredit - $finalPrice;
                         echo "<br>";
                         echo $totalDiscount = NULL ;
                         echo "<br>";
                         echo $totalPrice = $finalPrice;
                    }


                    if($productName == 'Product A')
                    {
                         echo $renewValueA = $userValueA + $quantity;
                         echo "<br>";
                         
                         // $maintenanceStatusProductA = 'Active';

                         // if($userCurrentRankA == '5')
                         // {
                         //      if($quantity >= 120)
                         //      {
                         //           $maintenanceStatusProductA = 'Active';
                         //      }
                         // }
                         // elseif($userCurrentRankA == '4')
                         // {
                         //      if($quantity >= 40)
                         //      {
                         //           $maintenanceStatusProductA = 'Active';
                         //      }
                         // }
                         // elseif($userCurrentRankA == '3')
                         // {
                         //      if($quantity >= 20)
                         //      {
                         //           $maintenanceStatusProductA = 'Active';
                         //      }
                         // }
                         // elseif($userCurrentRankA == '2')
                         // {
                         //      if($quantity >= 4)
                         //      {
                         //           $maintenanceStatusProductA = 'Active';
                         //      }
                         // }
                         // else
                         // {}

                    }
                    elseif($productName == 'Product B')
                    {
                         echo $renewValueB = $userValueB + $quantity;
                         echo "<br>";

                         echo $renewValueBBuy = $userValueBBuy + $quantity;
                         echo "<br>";

                         // $maintenanceStatusProductB = 'Active';
                    }
          
                    // echo $renewWalletA = $userWalletA + $quantity;
                    // echo "<br>";
          
                    $preOrderStatus = "Sold";
                    $orderUid = $uid.$timestamp;
                    $_SESSION['order_uid'] = $orderUid;
                    $status = "Pending";
          
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($preOrderStatus)
                    {
                         array_push($tableName,"status");
                         array_push($tableValue,$preOrderStatus);
                         $stringType .=  "s";
                    }    
                    array_push($tableValue,$productOrderId);
                    $stringType .=  "s";
                    $updateBonusPool = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                    if($updateBonusPool)
                    {
                         if(addOrderList($conn,$orderUid,$uid,$productUid,$productName,$unitPrice,$quantity,$finalPrice,$status,$mainProductUid,$totalDiscount,$totalPrice))
                         {
                              if($productName == 'Product A')
                              {
                                   $maintenanceStatusProductA = 'Active';

                                   $tableName = array();
                                   $tableValue =  array();
                                   $stringType =  "";
                                   //echo "save to database";
                                   if($renewValueA)
                                   {
                                        array_push($tableName,"value_a");
                                        array_push($tableValue,$renewValueA);
                                        $stringType .=  "s";
                                   }    
                                   // if($remainCredit)
                                   if($remainCredit || !$remainCredit)
                                   {
                                        array_push($tableName,"credit");
                                        array_push($tableValue,$remainCredit);
                                        $stringType .=  "d";
                                   }    
                                   if($renewRankA)
                                   {
                                        array_push($tableName,"rank_a");
                                        array_push($tableValue,$renewRankA);
                                        $stringType .=  "d";
                                   }   
                                   
                                   // if($maintenanceStatusProductA)
                                   // {
                                   //      array_push($tableName,"maintenance_a");
                                   //      array_push($tableValue,$maintenanceStatusProductA);
                                   //      $stringType .=  "s";
                                   // }    

                                   // if($userCurrentRankA == '5')
                                   // {}
                                   // elseif($userCurrentRankA == '4')
                                   // {}
                                   // elseif($userCurrentRankA == '3')
                                   // {}
                                   // elseif($userCurrentRankA == '2')
                                   // {}
                                   // else
                                   // {}

                                   if($userCurrentRankA == '5')
                                   {
                                        if($quantity >= 120)
                                        {
                                             if($maintenanceStatusProductA)
                                             {
                                                  array_push($tableName,"maintenance_a");
                                                  array_push($tableValue,$maintenanceStatusProductA);
                                                  $stringType .=  "s";
                                             }    
                                        }
                                   }
                                   elseif($userCurrentRankA == '4')
                                   {
                                        if($quantity >= 40)
                                        {
                                             if($maintenanceStatusProductA)
                                             {
                                                  array_push($tableName,"maintenance_a");
                                                  array_push($tableValue,$maintenanceStatusProductA);
                                                  $stringType .=  "s";
                                             }    
                                        }
                                   }
                                   elseif($userCurrentRankA == '3')
                                   {
                                        if($quantity >= 20)
                                        {
                                             if($maintenanceStatusProductA)
                                             {
                                                  array_push($tableName,"maintenance_a");
                                                  array_push($tableValue,$maintenanceStatusProductA);
                                                  $stringType .=  "s";
                                             }    
                                        }
                                   }
                                   elseif($userCurrentRankA == '2')
                                   {
                                        if($quantity >= 4)
                                        {
                                             if($maintenanceStatusProductA)
                                             {
                                                  array_push($tableName,"maintenance_a");
                                                  array_push($tableValue,$maintenanceStatusProductA);
                                                  $stringType .=  "s";
                                             }    
                                        }
                                   }
                                   else
                                   {
                                        if($maintenanceStatusProductA)
                                        {
                                             array_push($tableName,"maintenance_a");
                                             array_push($tableValue,$maintenanceStatusProductA);
                                             $stringType .=  "s";
                                        }   
                                   }

                                   array_push($tableValue,$uid);
                                   $stringType .=  "s";
                                   $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                   if($updateBonusPool)
                                   {
                                        // header('Location: ../viewCheckoutList.php');
                                        // echo $userUplineUid;
                                        // echo "<br>";  
                                        // echo $uplineUsername;
                                        // echo "<br>";  
                                        // echo $uplineWalletA;
                                        // echo "<br>";  
          
                                        if($uplineRankA > 4)
                                        //means upline rank is 5
                                        {
                                             if($uplineRankA == $userCurrentRankA)
                                             // if($uplineRankA == $renewRankA)
                                             //check same ranking
                                             {
                                                  $bonusLvlAOne = ($quantity * 5);
                                                  $bonusLvlAOneName = "Commission Level 1 (A)" ;
     
                                                  $renewUplineWalletA = $uplineWalletA + $bonusLvlAOne ;
               
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($renewUplineWalletA)
                                                  {
                                                       array_push($tableName,"wallet_a");
                                                       array_push($tableValue,$renewUplineWalletA);
                                                       $stringType .=  "s";
                                                  }    
                                                  array_push($tableValue,$userUplineUid);
                                                  $stringType .=  "s";
                                                  // $updateLvlOneUplineWalletA = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  //add in maintenance 
                                                  $updateLvlOneUplineWalletA = updateDynamicData($conn,"user"," WHERE uid = ? AND maintenance_a = 'Active' ",$tableName,$tableValue,$stringType);
                                                  if($updateLvlOneUplineWalletA)
                                                  {
                                                       if(commissionProductALvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlAOne,$bonusLvlAOneName))
                                                       {
                                                            // header('Location: ../viewCheckoutList.php');
                                                            // echo "SUCCESS A 2";
     
                                                            if($upline2ndLvlRankA == $uplineRankA)
                                                            //check same ranking
                                                            {
                                                                 $bonusLvlATwo = ($quantity * 2);
                                                                 $bonusLvlATwoName = "Commission Level 2 (A)";
                              
                                                                 $renew2ndLvlUplineWalletA = $upline2ndLvlWalletA + $bonusLvlATwo;
                              
                                                                 $tableName = array();
                                                                 $tableValue =  array();
                                                                 $stringType =  "";
                                                                 //echo "save to database";
                                                                 if($renew2ndLvlUplineWalletA)
                                                                 {
                                                                      array_push($tableName,"wallet_a");
                                                                      array_push($tableValue,$renew2ndLvlUplineWalletA);
                                                                      $stringType .=  "s";
                                                                 }    
                                                                 array_push($tableValue,$upline2ndLvlUid);
                                                                 $stringType .=  "s";
                                                                 // $updateLvlTwoUplineWalletA = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                 //add in maintenance 
                                                                 $updateLvlTwoUplineWalletA = updateDynamicData($conn,"user"," WHERE uid = ? AND maintenance_a = 'Active' ",$tableName,$tableValue,$stringType);
                                                                 if($updateLvlTwoUplineWalletA)
                                                                 {
                                                                      if(commissionProductALvlTwo($conn,$orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlATwo,$bonusLvlATwoName))
                                                                      {
                                                                           header('Location: ../viewCheckoutList.php');
                                                                           // echo "SUCCESS A 1, 2";
                                                                      }
                                                                      else
                                                                      {
                                                                           echo "ERROR Upline Wallet A1 !";
                                                                      }
                                                                 }
                                                                 else
                                                                 {
                                                                      echo "ERROR Upline Wallet A3 !";
                                                                 }    
                                                            }
                                                            else
                                                            {
                                                                 header('Location: ../viewCheckoutList.php');
                                                            }
     
                                                       }
                                                       else
                                                       {
                                                            echo "ERROR Upline Wallet A2, Q300!";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "ERROR Upline Wallet A3, Q300!";
                                                  }
     
                                             }
                                             else
                                             {
                                                  header('Location: ../viewCheckoutList.php');
                                             }
                                        }                   
                                        // if($uplineRankA >= 3)
                                        // if($uplineRankA >= 3 && $uplineRankA < 5)
                                        elseif($uplineRankA >= 3 && $uplineRankA < 5)
                                        //means upline rank is 3 and 4 only
                                        {
                                             if($uplineRankA == $userCurrentRankA)
                                             // if($uplineRankA == $renewRankA)
                                             //check same ranking
                                             {
                                                  $bonusLvlAOne = ($quantity * 4);
                                                  // $bonusLvlAOneName = "Commission Level 1" ;
                                                  $bonusLvlAOneName = "Commission Level 1 (A)" ;
                                                  $renewUplineWalletA = $uplineWalletA + $bonusLvlAOne ;
               
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($renewUplineWalletA)
                                                  {
                                                       array_push($tableName,"wallet_a");
                                                       array_push($tableValue,$renewUplineWalletA);
                                                       $stringType .=  "s";
                                                  }    
                                                  array_push($tableValue,$userUplineUid);
                                                  $stringType .=  "s";
                                                  // $updateLvlOneUplineWalletA = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  //add in maintenance 
                                                  $updateLvlOneUplineWalletA = updateDynamicData($conn,"user"," WHERE uid = ? AND maintenance_a = 'Active' ",$tableName,$tableValue,$stringType);
                                                  if($updateLvlOneUplineWalletA)
                                                  {
                                                       if(commissionProductALvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlAOne,$bonusLvlAOneName))
                                                       {
                                                            header('Location: ../viewCheckoutList.php');
                                                            // echo "SUCCESS A 2";
                                                       }
                                                       else
                                                       {
                                                            echo "ERROR Upline Wallet A2, Q50 !";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "ERROR Upline Wallet A3, Q50 !";
                                                  }
                                             }
                                             else
                                             {
                                                  header('Location: ../viewCheckoutList.php');
                                             }
                                        }
                                        else
                                        {
                                             header('Location: ../viewCheckoutList.php');
                                        }
                                   }
                                   else
                                   {
                                        echo "fail renew wallet a";
                                   }
                              }
                              elseif($productName == 'Product B')
                              {
                                   $tableName = array();
                                   $tableValue =  array();
                                   $stringType =  "";
                                   //echo "save to database";
                                   if($renewValueB)
                                   {
                                        array_push($tableName,"value_b");
                                        array_push($tableValue,$renewValueB);
                                        $stringType .=  "s";
                                   }    

                                   if($renewValueBBuy)
                                   {
                                        array_push($tableName,"value_b_buy");
                                        array_push($tableValue,$renewValueBBuy);
                                        $stringType .=  "s";
                                   }  

                                   // if($remainCredit)
                                   if($remainCredit || !$remainCredit)
                                   {
                                        array_push($tableName,"credit");
                                        array_push($tableValue,$remainCredit);
                                        $stringType .=  "d";
                                   }    
                                   if($renewRankB)
                                   {
                                        array_push($tableName,"rank_b");
                                        array_push($tableValue,$renewRankB);
                                        $stringType .=  "d";
                                   }   

                                   if($currentYear == $joinYear)
                                   {
                                        if($currentMonth == $joinMonth)
                                        {
                                             $maintenanceStatusProductB = 'Active';

                                             if($maintenanceStatusProductB)
                                             {
                                                  array_push($tableName,"maintenance_b");
                                                  array_push($tableValue,$maintenanceStatusProductB);
                                                  $stringType .=  "s";
                                             } 
                                        }
                                   }

                                   // if($maintenanceStatusProductB)
                                   // {
                                   //      array_push($tableName,"maintenance_b");
                                   //      array_push($tableValue,$maintenanceStatusProductB);
                                   //      $stringType .=  "s";
                                   // }   
                                   
                                   array_push($tableValue,$uid);
                                   $stringType .=  "s";
                                   $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                   if($updateBonusPool)
                                   {
                                        // --------------- remove $totalValueProductB --------------- //

                                        // // header('Location: ../viewCheckoutList.php');
                                        // $totalValueProductB = $productBCurrentValue + $quantity ;
                                        // // if($quantity >= 50)
                                        // if($totalValueProductB >= 50)
                                        // {
                                             // if($uplineRankB >= 2)
                                             if($uplineRankB >= 2 && $uplineRankBStatus == 'Active')
                                             {
                                                  // if($uplineRankB == $userCurrentRankB)
                                                  // {
                                                       $bonusLvlBOne = ($quantity * 6);
                                                       $bonusLvlBOneName = "Commission Level 1 (B)" ;
                                                       echo $renewUplineWalletB = $uplineWalletB + $bonusLvlBOne;
                                                       echo "<br>";  
          
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($renewUplineWalletB)
                                                       {
                                                            array_push($tableName,"wallet_b");
                                                            array_push($tableValue,$renewUplineWalletB);
                                                            $stringType .=  "s";
                                                       }    
                                                       array_push($tableValue,$userUplineUid);
                                                       $stringType .=  "s";
                                                       // $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       // check 2nd level upline maintenance status
                                                       $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? AND maintenance_b = 'Active' ",$tableName,$tableValue,$stringType);
                                                       if($updateBonusPool)
                                                       {
                                                            if(commissionProductBLvlOne($conn,$orderUid,$uid,$username,$userUplineUid,$uplineUsername,$bonusLvlBOne,$bonusLvlBOneName))
                                                            {
                                                                 // if($uplineRankB == $upline2ndLvlRankB)
                                                                 if($upline2ndLvlRankB >= 2)
                                                                 {
                                                                      $bonusLvlBTwo = ($quantity * 4);
                                                                      $bonusLvlBTwoName = "Commission Level 2 (B)";
                                                                      echo $renew2ndLvlUplineWalletA = $upline2ndLvlWalletB + $bonusLvlBTwo ;
                                                                      echo "<br>";  
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      //echo "save to database";
                                                                      if($renew2ndLvlUplineWalletA)
                                                                      {
                                                                           array_push($tableName,"wallet_b");
                                                                           array_push($tableValue,$renew2ndLvlUplineWalletA);
                                                                           $stringType .=  "s";
                                                                      }    
                                                                      array_push($tableValue,$upline2ndLvlUid);
                                                                      $stringType .=  "s";
                                                                      // $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      // check 2nd level upline maintenance status
                                                                      $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? AND maintenance_b = 'Active' ",$tableName,$tableValue,$stringType);
                                                                      if($updateBonusPool)
                                                                      {
                                                                           if(commissionProductBLvlTwo($conn,$orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlBTwo,$bonusLvlBTwoName))
                                                                           {
                                                                                header('Location: ../viewCheckoutList.php');
                                                                           }
                                                                           else
                                                                           {
                                                                                echo "ERROR Upline Wallet A2 !";
                                                                           }
                                                                      }
                                                                      else
                                                                      {
                                                                           echo "ERROR Upline Wallet A3 !";
                                                                      } 
                                                                 }
                                                                 else
                                                                 {
                                                                      header('Location: ../viewCheckoutList.php');
                                                                 }
                                                            }
                                                            else
                                                            {
                                                                 echo "ERROR Upline Wallet A2 !";
                                                            }
                                                       }
                                                       else
                                                       {
                                                            echo "ERROR Upline Wallet A3 !";
                                                       } 
                                                  // }
                                                  // else
                                                  // {
                                                  //      header('Location: ../viewCheckoutList.php');
                                                  // }
                                             }
                                             else
                                             {
                                                  // re-check 2nd level upline ranking
                                                  // header('Location: ../viewCheckoutList.php');
                                                  if($upline2ndLvlRankB >= 2)
                                                  {
                                                       $bonusLvlBTwo = ($quantity * 4);
                                                       $bonusLvlBTwoName = "Commission Level 2 (B)";
                                                       echo $renew2ndLvlUplineWalletA = $upline2ndLvlWalletB + $bonusLvlBTwo ;
                                                       echo "<br>";  
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($renew2ndLvlUplineWalletA)
                                                       {
                                                            array_push($tableName,"wallet_b");
                                                            array_push($tableValue,$renew2ndLvlUplineWalletA);
                                                            $stringType .=  "s";
                                                       }    
                                                       array_push($tableValue,$upline2ndLvlUid);
                                                       $stringType .=  "s";
                                                       // $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       // check 2nd level upline maintenance status
                                                       $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? AND maintenance_b = 'Active' ",$tableName,$tableValue,$stringType);
                                                       if($updateBonusPool)
                                                       {
                                                            if(commissionProductBLvlTwo($conn,$orderUid,$uid,$username,$upline2ndLvlUid,$upline2ndLvlUsername,$bonusLvlBTwo,$bonusLvlBTwoName))
                                                            {
                                                                 header('Location: ../viewCheckoutList.php');
                                                            }
                                                            else
                                                            {
                                                                 echo "ERROR Upline Wallet A2 !";
                                                            }
                                                       }
                                                       else
                                                       {
                                                            echo "ERROR Upline Wallet A3 !";
                                                       } 
                                                  }
                                                  else
                                                  {
                                                       header('Location: ../viewCheckoutList.php');
                                                  }
                                             }
                                        // }
                                        // else
                                        // {
                                        //      header('Location: ../viewCheckoutList.php');
                                        // }
          
                                   }
                                   else
                                   {
                                        echo "fail renew wallet b";
                                   }
                              }

                              // elseif($productName == 'Product C')
                              elseif($productName == 'Package C')
                              {
                                   $tableName = array();
                                   $tableValue =  array();
                                   $stringType =  "";
                                   //echo "save to database";
                                   if($maintenanceStatusProductC)
                                   {
                                        array_push($tableName,"maintenance_b");
                                        array_push($tableValue,$maintenanceStatusProductC);
                                        $stringType .=  "s";
                                   }    
                                   // if($remainCredit)
                                   if($remainCredit || !$remainCredit)
                                   {
                                        array_push($tableName,"credit");
                                        array_push($tableValue,$remainCredit);
                                        $stringType .=  "d";
                                   }     
                                   array_push($tableValue,$uid);
                                   $stringType .=  "s";
                                   $updateBonusPool = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                   if($updateBonusPool)
                                   {
                                        header('Location: ../viewCheckoutList.php');
                                   }
                                   else
                                   {
                                        echo "FAIL TO PERFORM MAINTENANCE FOR PRODUCT B !!";
                                   }
                              }

                              else
                              {
                                   echo "Unknown Product !! FAIL TO UPDATE";
                              }
                         }
                         else
                         {
                              echo "fail to add product into order list";
                         }
                    }
                    else
                    {
                         echo "FAIL TO CLEAR  ORDER LIST";
                    }
               }
               else
               {
                    // echo "Insufficent Credit <br> Please Reload !!";
                    // echo "<br>";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../stockInventory.php?type=1');
               }
          }
     }
}
else 
{
     header('Location: ../index.php');
}
?>