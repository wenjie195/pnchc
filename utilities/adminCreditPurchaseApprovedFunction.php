<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    echo $paymentUid = rewrite($_POST["payment_uid"]);
    echo "<br>";
    $status = "Approved";

    // $payment = getPayment($conn," uid = ? ",array("uid"),array($paymentUid),"s");   
    $payment = getPayment($conn, "WHERE uid =? ",array("uid"),array($paymentUid),"s");
    $userUid = $payment[0]->getUserUid(); 
    $purchasedCredit = $payment[0]->getCredit();

    // $userDetails = getUser($conn," uid = ? ",array("uid"),array($userUid),"s");    
    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");    
    $userCurrentCredit = $userDetails[0]->getCredit();
    $newCreditValue = $userCurrentCredit + $purchasedCredit;

    // if(!$payment)
    if($payment)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$paymentUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"payment"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($newCreditValue)
            {
                array_push($tableName,"CREDIT");
                array_push($tableValue,$newCreditValue);
                $stringType .=  "d";
            }
            array_push($tableValue,$userUid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminCreditPurchasePending.php?type=1');
            }
            else
            {
                // $_SESSION['messageType'] = 1;
                // header('Location: ../adminCreditPurchasePending.php?type=2');
                echo "FAIL TO UPDATE NEW CREDIT";
            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminCreditPurchasePending.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminCreditPurchasePending.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>