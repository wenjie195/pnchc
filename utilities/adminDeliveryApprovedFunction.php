<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/DeliverRecord.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $withdrawalUid = md5(uniqid());

     $transactionUid = rewrite($_POST["transaction_uid"]);
     $status = "APPROVED";

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $withdrawalUid."<br>";

     if(isset($_POST['transaction_uid']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }
          // if($method)
          // {
          //      array_push($tableName,"method");
          //      array_push($tableValue,$method);
          //      $stringType .=  "s";
          // }
          // if($reference)
          // {
          //      array_push($tableName,"note");
          //      array_push($tableValue,$reference);
          //      $stringType .=  "s";
          // }
          array_push($tableValue,$transactionUid);
          $stringType .=  "s";
          $updateWithdrawalStatus = updateDynamicData($conn,"deliver_record"," WHERE transaction_uid = ? ",$tableName,$tableValue,$stringType);
          if($updateWithdrawalStatus)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminStockDeliverHistory.php?type=1');
          }    
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error level 222";
     }
}
else
{
     header('Location: ../index.php');
}
?>