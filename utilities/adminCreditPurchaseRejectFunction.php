<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $paymentUid = rewrite($_POST["payment_uid"]);

    $status = "Rejected";

    $payment = getPayment($conn," uid = ? ",array("uid"),array($paymentUid),"s");    
    // $payment = getPayment($conn," WHERE uid = ? ",array("uid"),array($paymentUid),"s");    
    if(!$payment)
    // if($payment)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$paymentUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"payment"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminCreditPurchasePending.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminCreditPurchasePending.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminCreditPurchasePending.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>