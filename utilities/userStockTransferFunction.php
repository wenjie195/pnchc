<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/TransferRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

$timestamp = time();

function transferStockRecord($conn,$uid,$userUid,$username,$stockType,$amount,$receiverUid,$receiverUsername,$status)
{
     if(insertDynamicData($conn,"transfer_record",array("transaction_uid","uid","username","stock_type","amount","receiver_uid","receiver","status"),
          array($uid,$userUid,$username,$stockType,$amount,$receiverUid,$receiverUsername,$status),"ssssssss") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $amount = rewrite($_POST['amount']);
     $receiverUid = rewrite($_POST['receiver_uid']);
     $stockType = rewrite($_POST['product_type']);
     
     $receiverDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($receiverUid),"s");
     $receiverUsername = $receiverDetails[0]->getUsername();
     $receiverValueA = $receiverDetails[0]->getValueA();
     $receiverValueB = $receiverDetails[0]->getValueB();

     // $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     // $userValueA = $userDetails[0]->getValueA();
     // $userValueB = $userDetails[0]->getValueB();

     $status = 'Success';

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $user."<br>";

     if($stockType == 'A')
     {  
          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
          // $username = $receiverDetails[0]->getUsername();
          $username = $userDetails[0]->getUsername();
          $userValueA = $userDetails[0]->getValueA();

          if($userValueA > $amount)
          {  
               $renewUserValueA = $userValueA - $amount;

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($renewUserValueA)
               {
                   array_push($tableName,"value_a");
                   array_push($tableValue,$renewUserValueA);
                   $stringType .=  "s";
               }
               array_push($tableValue,$userUid);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    // echo "success A1";
                    $renewReceiverValueA = $receiverValueA + $amount;

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($renewReceiverValueA)
                    {
                         array_push($tableName,"value_a");
                         array_push($tableValue,$renewReceiverValueA);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$receiverUid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                         // echo "success A2";
                         if(transferStockRecord($conn,$uid,$userUid,$username,$stockType,$amount,$receiverUid,$receiverUsername,$status))
                         {  
                              header('Location: ../stockInventory.php');
                         }
                         else
                         {
                              echo "ERROR";
                         } 
                    }
                    else
                    {
                         echo "fail";
                    }

               }
               else
               {
                   echo "fail";
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../stockInventory.php?type=2');
          } 
     }
     elseif($stockType == 'B')
     {
          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
          // $username = $receiverDetails[0]->getUsername();
          $username = $userDetails[0]->getUsername();
          $userValueB = $userDetails[0]->getValueB();

          if($userValueB > $amount)
          {  
               $renewUserValueB = $userValueB - $amount;

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($renewUserValueB)
               {
                   array_push($tableName,"value_b");
                   array_push($tableValue,$renewUserValueB);
                   $stringType .=  "s";
               }
               array_push($tableValue,$userUid);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    // echo "success A1";
                    $renewReceiverValueB = $receiverValueB + $amount;

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($renewReceiverValueB)
                    {
                         array_push($tableName,"value_b");
                         array_push($tableValue,$renewReceiverValueB);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$receiverUid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                         // echo "success A2";
                         if(transferStockRecord($conn,$uid,$userUid,$username,$stockType,$amount,$receiverUid,$receiverUsername,$status))
                         {  
                              header('Location: ../stockInventory.php');
                         }
                         else
                         {
                              echo "ERROR";
                         } 
                    }
                    else
                    {
                         echo "fail";
                    }

               }
               else
               {
                   echo "fail";
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../stockInventory.php?type=2');
          } 
     } 
     else
     {
          echo "Unknown Stock Type";
     } 

     // if(transferStockRecord($conn,$uid,$userUid,$username,$stockType,$amount,$receiverUid,$receiverUsername,$status))
     // {  
     //      header('Location: ../stockInventory.php');
     // }
     // else
     // {
     //      echo "ERROR";
     // }   
}
else 
{
     header('Location: ../index.php');
}
?>