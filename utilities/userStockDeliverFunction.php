<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/TransferRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

$timestamp = time();

function deliverStockRecord($conn,$deliverUid,$uid,$username,$stockType,$previousAmount,$amount,$receiver,$address,$contact,$status)
{
     if(insertDynamicData($conn,"deliver_record",array("transaction_uid","uid","username","stock_type","previous_amount","amount","receiver","address","contact","status"),
          array($deliverUid,$uid,$username,$stockType,$previousAmount,$amount,$receiver,$address,$contact,$status),"ssssssssss") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $deliverUid = md5(uniqid());

     $amount = rewrite($_POST['amount']);
     $receiver = rewrite($_POST['received_name']);
     $address = rewrite($_POST['address']);
     $contact = rewrite($_POST['contact']);
     $stockType = rewrite($_POST['product_type']);
     
     // $receiverDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($receiverUid),"s");
     // $receiverUsername = $receiverDetails[0]->getUsername();
     // $receiverValueA = $receiverDetails[0]->getValueA();
     // $receiverValueB = $receiverDetails[0]->getValueB();

     // $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     // $userValueA = $userDetails[0]->getValueA();
     // $userValueB = $userDetails[0]->getValueB();

     $status = 'Pending';

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $user."<br>";

     if($stockType == 'A')
     {  
          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
          $username = $userDetails[0]->getUsername();
          $userValueA = $userDetails[0]->getValueA();
          $previousAmount = $userDetails[0]->getValueA();

          if($userValueA > $amount)
          {  
               $renewUserValueA = $userValueA - $amount;

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($renewUserValueA)
               {
                   array_push($tableName,"value_a");
                   array_push($tableValue,$renewUserValueA);
                   $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    if(deliverStockRecord($conn,$deliverUid,$uid,$username,$stockType,$previousAmount,$amount,$receiver,$address,$contact,$status))
                    {  
                         // header('Location: ../stockInventory.php');
                         $_SESSION['messageType'] = 1;
                         header('Location: ../stockInventory.php?type=4');
                    }
                    else
                    {
                         echo "ERROR";
                    } 
               }
               else
               {
                   echo "fail";
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../stockInventory.php?type=3');
          } 
     }
     elseif($stockType == 'B')
     {
          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
          $username = $userDetails[0]->getUsername();
          $userValueB = $userDetails[0]->getValueB();
          $previousAmount = $userDetails[0]->getValueB();

          if($userValueB > $amount)
          {  
               $renewUserValueB = $userValueB - $amount;

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($renewUserValueB)
               {
                   array_push($tableName,"value_b");
                   array_push($tableValue,$renewUserValueB);
                   $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    if(deliverStockRecord($conn,$deliverUid,$uid,$username,$stockType,$previousAmount,$amount,$receiver,$address,$contact,$status))
                    {  
                         // header('Location: ../stockInventory.php');
                         $_SESSION['messageType'] = 1;
                         header('Location: ../stockInventory.php?type=4');
                    }
                    else
                    {
                         echo "ERROR";
                    } 
               }
               else
               {
                   echo "fail";
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../stockInventory.php?type=3');
          } 
     } 
     else
     {
          echo "Unknown Stock Type";
     } 
}
else 
{
     header('Location: ../index.php');
}
?>