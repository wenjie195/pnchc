<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/DeliverRecord.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $withdrawalUid = md5(uniqid());

     // $withdrawalUid = rewrite($_POST["withdrawal_uid"]);
     // $withdrawalStatus = "REJECTED";
     $transactionUid = rewrite($_POST["transaction_uid"]);
     $status = "REJECTED";


     $deliverDetails = getDeliverRecord($conn," WHERE transaction_uid = ? ",array("transaction_uid"),array($transactionUid),"s");
     $deliverAmount =  $deliverDetails[0]->getAmount();
     $stockType =  $deliverDetails[0]->getStockType();
     $userUid =  $deliverDetails[0]->getUid();

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
     if($stockType == 'A')
     {
          $userCurrentValueA =  $userDetails[0]->getValueA();
          $refundAmount = $deliverAmount + $userCurrentValueA;
     }
     elseif($stockType == 'B')
     {
          $userCurrentValueB =  $userDetails[0]->getValueB();
          $refundAmount = $deliverAmount + $userCurrentValueB;
     }
     else
     {}

     // $userCurrentCredit =  $userDetails[0]->getWallet();

     // $refundAmount = $withdrawalAmount + $userCurrentCredit;

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $withdrawalUid."<br>";

     if(isset($_POST['transaction_uid']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }
          array_push($tableValue,$transactionUid);
          $stringType .=  "s";
          $updateWithdrawalStatus = updateDynamicData($conn,"deliver_record"," WHERE transaction_uid = ? ",$tableName,$tableValue,$stringType);
          if($updateWithdrawalStatus)
          {
               if(isset($_POST['transaction_uid']))
               {

                    if($stockType == 'A')
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         // //echo "save to database";
          
                         if($refundAmount)
                         {
                              array_push($tableName,"value_a");
                              array_push($tableValue,$refundAmount);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$userUid);
                         $stringType .=  "s";
                         $updateWithdrawalStatus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($updateWithdrawalStatus)
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../adminStockDeliverHistory.php?type=2');
                         }    
                         else
                         {
                              echo "fail";
                         }
                    }
                    elseif($stockType == 'B')
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         // //echo "save to database";
          
                         if($refundAmount)
                         {
                              array_push($tableName,"value_b");
                              array_push($tableValue,$refundAmount);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$userUid);
                         $stringType .=  "s";
                         $updateWithdrawalStatus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($updateWithdrawalStatus)
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../adminStockDeliverHistory.php?type=2');
                         }    
                         else
                         {
                              echo "fail";
                         }
                    }
                    else
                    {
                         echo "Unknow Stock Type, Fail to Refund";
                    }

                    // $tableName = array();
                    // $tableValue =  array();
                    // $stringType =  "";
                    // // //echo "save to database";
     
                    // if($refundAmount)
                    // {
                    //      array_push($tableName,"wallet");
                    //      array_push($tableValue,$refundAmount);
                    //      $stringType .=  "s";
                    // }
                    // array_push($tableValue,$userUid);
                    // $stringType .=  "s";
                    // $updateWithdrawalStatus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    // if($updateWithdrawalStatus)
                    // {
                    //      $_SESSION['messageType'] = 1;
                    //      header('Location: ../adminStockDeliverHistory.php?type=2');
                    // }    
                    // else
                    // {
                    //      echo "fail";
                    // }
               }
               else
               {
                    echo "error level 2";
               }
          }    
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error level 222";
     }
}
else
{
     header('Location: ../index.php');
}
?>