<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $dateInput = $_POST["update_date"];
    $title = $_POST["update_title"];
    $message = $_POST["update_message"];
    $announcementUid = rewrite($_POST["announcement_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $announcementDetails = getAnnouncement($conn," WHERE uid = ? ",array("uid"),array($announcementUid),"s");   

    // if(!$announcementDetails)
    if($announcementDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($dateInput)
        {
            array_push($tableName,"date_input");
            array_push($tableValue,$dateInput);
            $stringType .=  "s";
        }
        if($title)
        {
            array_push($tableName,"title");
            array_push($tableValue,$title);
            $stringType .=  "s";
        }
        if($message)
        {
            array_push($tableName,"content");
            array_push($tableValue,$message);
            $stringType .=  "s";
        }

        array_push($tableValue,$announcementUid);
        $stringType .=  "s";
        $announcementUpdated = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($announcementUpdated)
        {
            header('Location: ../adminAnnoucementAll.php');
        }
        else
        {
            echo "<script>alert('fail to update annoucement !!');window.location='../adminAnnoucementAll.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminAnnoucementAll.php'</script>";  
    }
}
else 
{
    header('Location: ../index.php');
}
?>