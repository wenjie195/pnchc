<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$userType,$finalPassword,$salt,$fullname,$email,$phone,$icno,$wechat)
{
     if(insertDynamicData($conn,"user",array("uid","username","user_type","password","salt","fullname","email","phone_no","icno","wechat_id"),
          array($uid,$username,$userType,$finalPassword,$salt,$fullname,$email,$phone,$icno,$wechat),"ssisssssss") === null)
     {
          echo "<script>alert('ERROR, Please try again later !!'); window.history.go(-1);</script>";
     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function sendEmailForVerification($uid)
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.agentpnchc.com";
     $verifyUser_usernameThatSendEmail = "noreply@agentpnchc.com";                   // Sender Acc Username
     $verifyUser_password = "Joujt3wlLBJi";                                              // Sender Acc Password

     $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
     $verifyUser_port = 465;                                                              // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@agentpnchc.com";                    // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@agentpnchc.com";                        // Sender Email


     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                            // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Welcome To Pure & Cure";

     $verifyUser_body = "<p>Dear ".$userRows[0]->getUsername().",</p>";                      // Body
     $verifyUser_body .="<p>You have successfully registered for Pure & Cure.</p>";
     $verifyUser_body .="<p>https://agentpnchc.com/</p>";
     $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
     $verifyUser_body .="<p>Thank you.</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $fullname = rewrite($_POST['register_fullname']);
     $email = rewrite($_POST['register_email']);
     $phone = rewrite($_POST['register_phone']);
     $icno = rewrite($_POST['register_icno']);
     $wechat = rewrite($_POST['register_wechatid']);
     $username = rewrite($_POST['register_username']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $userType = "1";
     $sponsorID = rewrite($_POST['upline_uid']);

     // $register_password = "111111";
     // // $register_password_validation = strlen($register_password);
     // $register_retype_password = "111111";

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";

     // $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
     $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
     $usernameDetails = $usernameRows[0];

     // $icRows = getUser($conn," WHERE icno = ? ",array("icno"),array($icno),"s");
     // $icDetails = $icRows[0];

     // $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
     // $userEmailDetails = $userEmailRows[0];

     // $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_mobileno']),"s");
     // $userPhoneDetails = $userPhoneRows[0];

     // if (!$usernameDetails && !$icDetails)

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               if (!$usernameDetails)
               {
                    if($sponsorID)
                    {
                         $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $referrerName = $referrerUserRows[0]->getUsername();
                              // $referralUid = $referrerUserRows[0]->getUid();
                              $referralName = $username;
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $currentLevel = 1;
                              $getUplineCurrentLevel = 1;
          
                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                              if($referralHistoryRows)
                              {
                                   $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                                   $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }
                              $referralNewestRows = getReferralHistory($conn,"WHERE referral_id = ?", array("referral_id"),array($referrerUid), "s");
                              if($referralNewestRows)
                              {
                                   $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;
          
                                   if(registerNewUser($conn,$uid,$username,$userType,$finalPassword,$salt,$fullname,$email,$phone,$icno,$wechat))
                                   {
                         
                                        // $referrerUid = '70691b70ee884b1439e8a73ce33c0eef';
                                        // $referralName = $username;
                                        // $topReferrerUid = $referrerUid;
                                        // $currentLevel = 1;
                         
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             // echo "done register !";
                                             // header( "Location: ../success.php");
                                             sendEmailForVerification($uid);
                                             $_SESSION['uid'] = $uid;
                                             // $_SESSION['usertype_level'] = 1;
                                             $_SESSION['user_type'] = 1;
                                             // header('Location: ../refer.php');  
                                             header('Location: ../userDashboard.php');  
                                        }
                                        else
                                        {
                                             echo "FAIL 'referral history' ";
                                        }
                                   }
                                   else
                                   {
                                        echo "FAIL 'user' ";
                                   }
          
                              }
                         }
                    }
                    else
                    {
                         echo "<script>alert('invalid refer code'); window.history.go(-1);</script>";
                    }
               }
               else
               {
                    echo "<script>alert('register details has been used by others'); window.history.go(-1);</script>";
               }
          }
          else
          { 
               echo "<script>alert('password length must more than 5 !!'); window.history.go(-1);</script>";
          }
     }
     else
     { 
          echo "<script>alert('password does not match with retype-password !!'); window.history.go(-1);</script>";
     }

     // if (!$usernameDetails)
     // {

     //      if($sponsorID)
     //      {
     //           $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
     //           if($referrerUserRows)
     //           {
     //                $referrerUid = $referrerUserRows[0]->getUid();
     //                $referrerName = $referrerUserRows[0]->getUsername();
     //                // $referralUid = $referrerUserRows[0]->getUid();
     //                $referralName = $username;
     //                $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
     //                $currentLevel = 1;
     //                $getUplineCurrentLevel = 1;

     //                $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
     //                if($referralHistoryRows)
     //                {
     //                     $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
     //                     $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
     //                }
     //                $referralNewestRows = getReferralHistory($conn,"WHERE referral_id = ?", array("referral_id"),array($referrerUid), "s");
     //                if($referralNewestRows)
     //                {
     //                     $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

     //                     if(registerNewUser($conn,$uid,$username,$userType,$finalPassword,$salt))
     //                     {
               
     //                          // $referrerUid = '70691b70ee884b1439e8a73ce33c0eef';
     //                          // $referralName = $username;
     //                          // $topReferrerUid = $referrerUid;
     //                          // $currentLevel = 1;
               
     //                          if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
     //                          {
     //                               echo "done register !";
     //                          }
     //                          else
     //                          {
     //                               echo "FAIL 'referral history' ";
     //                          }
     //                     }
     //                     else
     //                     {
     //                          echo "FAIL 'user' ";
     //                     }

     //                }
     //           }
     //      }

     //      // if(registerNewUser($conn,$uid,$username,$userType,$finalPassword,$salt))
     //      // {

     //      //      $referrerUid = '70691b70ee884b1439e8a73ce33c0eef';
     //      //      $referralName = $username;
     //      //      $topReferrerUid = $referrerUid;
     //      //      $currentLevel = 1;

     //      //      if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
     //      //      {
     //      //           echo "done register !";
     //      //      }
     //      //      else
     //      //      {
     //      //           echo "FAIL 'referral history' ";
     //      //      }
     //      // }
     //      // else
     //      // {
     //      //      echo "FAIL 'user' ";
     //      // }
     // }
     // else
     // {
     //      // echo "fail to register register";
     //      // echo "<script>alert('register details has been used by others');window.location='../index.php'</script>";
     //      echo "<script>alert('register details has been used by others'); window.history.go(-1);</script>";
     // }

}
else
{
     header('Location: ../index.php');
}
?>