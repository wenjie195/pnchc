<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

function createOrder($conn,$orderUid,$uid,$name,$contact,$email,$subotal,$paymentMethod,$paymentStatus)
{
     if(insertDynamicData($conn,"orders",array("order_id","uid","name","contactNo","email","subtotal","payment_method","payment_status"),
          array($orderUid,$uid,$name,$contact,$email,$subotal,$paymentMethod,$paymentStatus),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());

     $name = rewrite($_POST['name']);
     $contact = rewrite($_POST['phone']);
     $email = rewrite($_POST['email']);

     // $orderUid = rewrite($_POST['order_uid']);
     $subotal = rewrite($_POST['subtotal']);
     // $totalPrice = $originalPrice * $quantity;

     // $paymentMethod = 'BILLPLZ';
     // $paymentStatus = 'WAITING';

     $paymentMethod = 'POINTS';
     $paymentStatus = 'APPROVED';

     $orderListStatus = "Sold";

     $tableName = array();
     $tableValue =  array();
     $stringType =  "";
     //echo "save to database";
     if($orderListStatus)
     {
         array_push($tableName,"status");
         array_push($tableValue,$orderListStatus);
         $stringType .=  "s";
     }    
     array_push($tableValue,$orderUid);
     $stringType .=  "s";
     $updateBonusPool = updateDynamicData($conn,"order_list"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
     if($updateBonusPool)
     {
          if(createOrder($conn,$orderUid,$uid,$name,$contact,$email,$subotal,$paymentMethod,$paymentStatus))
          {
               // echo "order success";
               header('Location: ../stockInventory.php');
               // header('Location: ../paymentDetails.php');
          }
          else
          {
               echo "fail to create order";
          }
     }
     else
     {
          echo "FAIL TO CLEAR  ORDER LIST";
     }

     // if(createOrder($conn,$orderUid,$uid,$name,$contact,$address,$subotal,$paymentMethod,$paymentStatus))
     // {
     //      // echo "order created";
     //      header('Location: ../payAndShip.php');
     // }
     // else
     // {
     //      echo "fail to create order";
     // }

}
else 
{
     header('Location: ../index.php');
}
?>