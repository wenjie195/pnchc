<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function walletConvert($conn,$xferUid,$uid,$username,$currentAmount,$amount,$walletType,$status)
{
     if(insertDynamicData($conn,"wallet_record",array("xfer_uid","uid","username","previous_wallet","amount","wallet_type","status"),
     array($xferUid,$uid,$username,$currentAmount,$amount,$walletType,$status),"sssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

function subscribePackage($conn,$xferUid,$uid,$username,$email,$phone,$credit,$reference,$file,$status,$amount)
{
     if(insertDynamicData($conn,"payment",array("uid","user_uid","username","email","phone_no","credit","bank_reference","receipt","status","amount"),
          array($xferUid,$uid,$username,$email,$phone,$credit,$reference,$file,$status,$amount),"ssssssssss") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $xferUid = md5(uniqid());

     $amount = rewrite($_POST["withdrawal_amount"]);

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $email = $userDetails[0]->getEmail();
     $phone = $userDetails[0]->getPhoneNo();
     $credit = rewrite($_POST["withdrawal_amount"]);
     $reference = 'Wallet To Credit';
     $file = NULL ;

     echo $uid = $userDetails[0]->getUid();
     echo "<br>";
     echo $username = $userDetails[0]->getUsername();
     echo "<br>";
     echo $currentWallet = $userDetails[0]->getWallet();
     echo "<br>";
     echo $currentCredit = $userDetails[0]->getCredit();
     echo "<br>";

     $spacing = ' ';
     $slash = ' / ';
     $currentAmount = $currentCredit.$spacing.$slash.$spacing.$currentWallet;

     $walletType = 'Convert To Credit';
     $status = 'Success';

     $renewWallet = $currentWallet - $amount;
     $renewCredit = $currentCredit + $amount;

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     
     if($amount > $currentWallet)
     {
          echo "<script>alert('convert amount is exceed than amount in wallet !!');window.location='../userCreditWithdrawal.php'</script>";
     }
     else
     {
          if(walletConvert($conn,$xferUid,$uid,$username,$currentAmount,$amount,$walletType,$status))
          {

               if(subscribePackage($conn,$xferUid,$uid,$username,$email,$phone,$credit,$reference,$file,$status,$amount))
               {
                    if(isset($_POST['submit']))
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($renewWallet)
                         {
                              array_push($tableName,"wallet");
                              array_push($tableValue,$renewWallet);
                              $stringType .=  "d";
                         }
                         if(!$renewWallet)
                         {    
                              array_push($tableName,"wallet");
                              array_push($tableValue,$renewWallet);
                              $stringType .=  "d";
                         }
                         if($renewCredit)
                         {
                              array_push($tableName,"credit");
                              array_push($tableValue,$renewCredit);
                              $stringType .=  "d";
                         }
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($orderUpdated)
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../userCreditTotal.php?type=1');
                         }
                         else
                         {
                              echo "<script>alert('FAIL');window.location='../userCreditWithdrawal.php'</script>";
                         }
                    }
                    else
                    {
                         echo "<script>alert('ERROR 1');window.location='../userCreditWithdrawal.php'</script>";
                    }
               }
               else
               {
                    echo "<script>alert('ERROR 3');window.location='../userCreditWithdrawal.php'</script>";
               }
          }
          else
          {
               echo "<script>alert('ERROR 2');window.location='../userCreditWithdrawal.php'</script>";
          }
     }
 
}
else
{
     header('Location: ../index.php');
}
?>