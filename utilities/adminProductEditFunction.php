<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $productUid = rewrite($_POST['product_uid']);
    $name = rewrite($_POST['product_name']);
    // $price = rewrite($_POST['product_price']);
    $descriptionOne = rewrite($_POST['description_one']);
    // $descriptionTwo = rewrite($_POST['description_two']);

    $oriFileOne = rewrite($_POST["ori_fileone"]);
    $newFileOne = $_FILES['image_one']['name'];
    if($newFileOne == '')
    {
        $file = $oriFileOne;
    }
    else
    {
        $file = $oriFileOne;
        $file = $timestamp.$_FILES['image_one']['name'];
        $target_dir = "../productImage/";
        $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif","pdf");
        if( in_array($imageFileType,$extensions_arr) )
        {
            move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$file);
        }
    }

    $productDetails = getProduct($conn, "WHERE uid =?",array("uid"),array($productUid),"s");

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $category."<br>";

    if(isset($_POST['submit']))
    // if(!$productDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        // if($price)
        // {
        //     array_push($tableName,"price");
        //     array_push($tableValue,$price);
        //     $stringType .=  "s";
        // } 
        if($descriptionOne)
        {
            array_push($tableName,"description");
            array_push($tableValue,$descriptionOne);
            $stringType .=  "s";
        } 
        // if($descriptionTwo)
        // {
        //     array_push($tableName,"description_two");
        //     array_push($tableValue,$descriptionTwo);
        //     $stringType .=  "s";
        // } 
        if($file)
        {
            array_push($tableName,"image_one");
            array_push($tableValue,$file);
            $stringType .=  "s";
        } 


        array_push($tableValue,$productUid);
        $stringType .=  "s";
        $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateProductDetails)
        {
            header('Location: ../adminProductAll.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "ERROR !!";
    }

}
else
{
    header('Location: ../index.php');
}
?>