<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function submitWithdrawal($conn,$uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status)
{
     if(insertDynamicData($conn,"withdrawal",array("uid","withdrawal_uid","bank_name","bank_account_holder","bank_account_no","contact","current_amount","amount","final_amount","status"),
     array($uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status),"ssssssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $withdrawalUid = md5(uniqid());

     $bankName = rewrite($_POST["bank_name"]);
     $bankAccHolder = rewrite($_POST["bank_acc_holder"]);
     $bankAccNo = rewrite($_POST["bank_account_no"]);
     $amount = rewrite($_POST["withdrawal_amount"]);

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $contact = $userDetails[0]->getPhoneNo();
     // $currentAmount = $userDetails[0]->getCredit();
     $currentAmount = $userDetails[0]->getWallet();

     $finalAmount = $currentAmount - $amount;
     $status = "PENDING";

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $withdrawalUid."<br>";
     // echo $bankName."<br>";
     // echo $bankAccHolder."<br>";
     // echo $bankAccNo."<br>";
     // echo $amount."<br>";
     // echo $contact."<br>";
     // echo $currentAmount."<br>";
     // echo $finalAmount."<br>";
     
     if($amount > $currentAmount)
     {
          echo "<script>alert('withdrawal amount is exceed than amount in wallet !!');window.location='../userCreditWithdrawal.php'</script>";
     }
     else
     {

          if($amount >= 50)
          {
               if(submitWithdrawal($conn,$uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status))
               {
                    if(isset($_POST['submit']))
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         // if($finalAmount)
                         // {
                         //      array_push($tableName,"credit");
                         //      array_push($tableValue,$finalAmount);
                         //      $stringType .=  "d";
                         // }
                         // if(!$finalAmount)
                         // {    
                         //      // $finalAmount = 0;
                         //      array_push($tableName,"credit");
                         //      array_push($tableValue,$finalAmount);
                         //      $stringType .=  "d";
                         // }
                         if($finalAmount)
                         {
                              array_push($tableName,"wallet");
                              array_push($tableValue,$finalAmount);
                              $stringType .=  "d";
                         }
                         if(!$finalAmount)
                         {    
                              // $finalAmount = 0;
                              array_push($tableName,"wallet");
                              array_push($tableValue,$finalAmount);
                              $stringType .=  "d";
                         }
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($orderUpdated)
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../userCreditWithdrawalHistory.php?type=1');
                              // echo "<script>alert('Submit Withdrawal Request Successfully !');window.location='../userCreditWithdrawalHistory.php'</script>";
                         }
                         else
                         {
                              echo "<script>alert('FAIL');window.location='../userCreditWithdrawal.php'</script>";
                         }
                    }
                    else
                    {
                         echo "<script>alert('ERROR 1');window.location='../userCreditWithdrawal.php'</script>";
                    }
               }
               else
               {
                    echo "<script>alert('ERROR 2');window.location='../userCreditWithdrawal.php'</script>";
               }
          }
          else
          {
               echo "<script>alert('withdrawal amount must more than 50');window.location='../userCreditWithdrawal.php'</script>";
          }

          // if(submitWithdrawal($conn,$uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status))
          // {
          //      if(isset($_POST['submit']))
          //      {
          //           $tableName = array();
          //           $tableValue =  array();
          //           $stringType =  "";
          //           //echo "save to database";
          //           if($finalAmount)
          //           {
          //                array_push($tableName,"credit");
          //                array_push($tableValue,$finalAmount);
          //                $stringType .=  "d";
          //           }
          //           if(!$finalAmount)
          //           {    
          //                // $finalAmount = 0;
          //                array_push($tableName,"credit");
          //                array_push($tableValue,$finalAmount);
          //                $stringType .=  "d";
          //           }
          //           array_push($tableValue,$uid);
          //           $stringType .=  "s";
          //           $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          //           if($orderUpdated)
          //           {
          //                // $_SESSION['messageType'] = 1;
          //                // header('Location: ../adminShipping.php?type=11');
          //                echo "<script>alert('Submit Withdrawal Request Successfully !');window.location='../userCreditWithdrawalHistory.php'</script>";
          //           }
          //           else
          //           {
          //                echo "<script>alert('FAIL');window.location='../userCreditWithdrawal.php'</script>";
          //           }
          //      }
          //      else
          //      {
          //           echo "<script>alert('ERROR 1');window.location='../userCreditWithdrawal.php'</script>";
          //      }
          // }
          // else
          // {
          //      echo "<script>alert('ERROR 2');window.location='../userCreditWithdrawal.php'</script>";
          // }
     }
 
}
else
{
     header('Location: ../index.php');
}
?>