<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';


function generateRandomString($length = 10) 
{
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
     // $username = rewrite($_POST['username']);
     $forgotPassword_email = rewrite($_POST['email']);
     $forgotPassword_email = filter_var($forgotPassword_email, FILTER_SANITIZE_EMAIL);
     
     if(filter_var($forgotPassword_email, FILTER_VALIDATE_EMAIL))
     {
          $tempPass = generateRandomString();

          $tempPass = hash('sha256',$tempPass);
          $salt = substr(sha1(mt_rand()), 0, 100);
          $finalPassword = hash('sha256', $salt.$tempPass);

          // $passwordUpdated = updateDynamicData($conn,"user"," WHERE email = ? AND username = ? ",array("password","salt"),array($finalPassword,$salt,$forgotPassword_email,$username),"ssss");
          $passwordUpdated = updateDynamicData($conn,"user"," WHERE email = ? ",array("password","salt"),array($finalPassword,$salt,$forgotPassword_email),"sss");
          // var_dump($passwordUpdated);

          // echo $forgotPassword_email;

          if($passwordUpdated)
          {
               $user = getUser($conn," WHERE email = ? ",array("email"),array($forgotPassword_email),"s");
               // $user = getUser($conn," WHERE email = ? AND username = ?",array("email","username"),array($forgotPassword_email,$username),"ss");
               // var_dump($user);
               $verifyUser_debugMode = 2;
               $verifyUser_host = "mail.agentpnchc.com";
               $verifyUser_usernameThatSendEmail = "noreply@agentpnchc.com";                   // Sender Acc Username
               $verifyUser_password = "Joujt3wlLBJi";                                                      // Sender Acc Password
               $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
               $verifyUser_port = 465;                                                                   // SMTP port no
               $verifyUser_sentFromThisEmailName = "noreply@agentpnchc.com";                                       // Sender Username
               $verifyUser_sentFromThisEmail = "noreply@agentpnchc.com";                          // Sender Email
               $verifyUser_sendToThisEmailName = $user[0]->getUsername();                                // Recipient Username
               $verifyUser_sendToThisEmail = $forgotPassword_email;                                      // Recipient Email
               $verifyUser_isHtml = true;                                                                // Set To Html
               $verifyUser_subject = "Reset Password";  
     
               $verifyUser_body = "<p>Please reset your password in this ";
               $verifyUser_body .="<a href='https://agentpnchc.com/resetPassword.php?uid=".$user[0]->getUid()."'>link</a>  ";
               $verifyUser_body .="using this key code below</p>";
               $verifyUser_body .="<p>Link to Reset Password  =  <a href='https://agentpnchc.com/resetPassword.php?uid=".$user[0]->getUid()."'>https://agentpnchc.com/resetPassword.php?uid=".$user[0]->getUid()."</a></p>";
               $verifyUser_body .="<p>Key Code = ".$tempPass."</p>";

               sendMailTo(
                    null,
                    $verifyUser_host,
                    $verifyUser_usernameThatSendEmail,
                    $verifyUser_password,
                    $verifyUser_smtpSecure,
                    $verifyUser_port, 
                    $verifyUser_sentFromThisEmailName,
                    $verifyUser_sentFromThisEmail,
                    $verifyUser_sendToThisEmailName,
                    $verifyUser_sendToThisEmail,
                    $verifyUser_isHtml,
                    $verifyUser_subject,
                    $verifyUser_body,
                    null
               );
               // $_SESSION['messageType'] = 1;
               // header('Location: ../index.php?type=8');
               // //echo "// forgot email success send email ";
               echo "<script>alert('reset password link has been sent to your email');window.location='../index.php'</script>"; 
          }
          else 
          {
               // echo "no user with ths email ";
               echo "<script>alert('no user with ths email !');window.location='../index.php'</script>"; 
          }

     }
     else 
     {
          // echo "wrong email format ";
          echo "<script>alert('wrong email format  !');window.location='../index.php'</script>"; 
     }
}
else 
{
     header('Location: ../index.php');
}
?>