<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/User.php';
// require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo "Product Amount On Hand: ";
     echo $totalProductBAmount = rewrite($_POST['fix_amount']);
     echo "<br>";
     echo "Order Amount : ";
     echo $quantity = rewrite($_POST['order_amount']);
     echo "<br>";

     echo "Total Product B Amount : ";
     echo $totalProductB = $totalProductBAmount + $quantity ;
     echo "<br>";
     if($totalProductB >= 300)
     {
          $unitPrice = '199';
          echo "Upgrade To Level : ";
          echo $level = '4';
          echo "<br>";
          // $profit = '80';

          if($totalProductBAmount >= 300)
          {
               $Q4Alpha = $totalProductB - $totalProductBAmount;
               echo "Total Profit : ";
               echo $profit = $Q4Alpha * 80;
               echo "<br>";
          }
          elseif($totalProductBAmount >= 100)
          {
               $Q4Alpha = $totalProductB - 299;
               $profitQ4Alpha = $Q4Alpha * 80;
               $Q4Beta = $totalProductB - $Q4Alpha - $totalProductBAmount ;
               $profitQ4Beta = $Q4Beta * 60;
               $profitQ4Charlie = 0;
               $profitQ4Delta = 0;

               echo "Total Profit : ";
               echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
               echo "<br>";
          }
          elseif($totalProductBAmount >= 50)
          {
               $Q4Alpha = $totalProductB - 299;
               $profitQ4Alpha = $Q4Alpha * 80;
               $Q4Beta = $totalProductB - $Q4Alpha - 99 ;
               $profitQ4Beta = $Q4Beta * 60;
               $Q4Charlie = $totalProductB - $Q4Alpha - $Q4Beta - $totalProductBAmount ;
               $profitQ4Charlie = $Q4Charlie * 40;
               $profitQ4Delta = 0;

               echo "Total Profit : ";
               echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
               echo "<br>";
          }
          else
          {
               $Q4Alpha = $totalProductB - 299;
               $profitQ4Alpha = $Q4Alpha * 80;
               $Q4Beta = $totalProductB - $Q4Alpha - 99 ;
               $profitQ4Beta = $Q4Beta * 60;
               $Q4Charlie = $totalProductB - $Q4Alpha - $Q4Beta - 49 ;
               $profitQ4Charlie = $Q4Charlie * 40;
               $Q4Delta = $totalProductB - $Q4Alpha - $Q4Beta - $Q4Charlie - $totalProductBAmount ;
               $profitQ4Delta = $Q4Delta * 30;

               echo "Total Profit : ";
               echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
               echo "<br>";
          }


          // echo "Total Profit : ";
          // echo $profit = $profitQ4Alpha + $profitQ4Beta + $profitQ4Charlie + $profitQ4Delta;
          // echo "<br>";

          // $lvlFourOrderQuantity = $quantity - 49 - 50 - 200;
          // $profitOne = '30';
          // $profitLvlOne = $profitOne * 49;
          // $profitTwo = '40';
          // $profitLvlTwo = $profitTwo * 50;
          // $profitThree = '60';
          // $profitLvlThree = $profitThree * 200;
          // $profitFour = '80';
          // $profitLvlFour = $profitFour * $lvlFourOrderQuantity;
          // // $profitLvlThree = $profitThree * $lvlThreeOrderQuantity;
          // echo "Total Profit : ";
          // echo $profit = $profitLvlOne + $profitLvlTwo + $profitLvlThree + $profitLvlFour;
          // echo "<br>";

     }
     // elseif($totalProductB >= 100)
     elseif($totalProductB >= 100 && $totalProductB <= 299)
     {
          $unitPrice = '199';
          // $level = '3';
          echo "Upgrade To Level : ";
          echo $level = '3';
          echo "<br>";
          // $profit = '60';

          // $lvlThreeOrderQuantity = $quantity - 49 - 50;
          // $profitOne = '30';
          // $profitLvlOne = $profitOne * 49;
          // $profitTwo = '40';
          // // $profitLvlTwo = $profitTwo * $lvlTwoOrderQuantity;
          // $profitLvlTwo = $profitTwo * 50;
          // $profitThree = '60';
          // $profitLvlThree = $profitThree * $lvlThreeOrderQuantity;

          if($totalProductBAmount >= 100)
          {
               $Q3Alpha = $totalProductB - $totalProductBAmount;
               echo "Total Profit : ";
               echo $profit = $Q3Alpha * 60;
               echo "<br>";
          }
          else
          {
               $Q3Alpha = $totalProductB - 99;
               $profitQ3Alpha = $Q3Alpha * 60;

               if($totalProductBAmount >= 50)
               {
                    $Q3Beta = $totalProductB - $Q3Alpha - ($totalProductBAmount - 50);
                    $profitQ3Beta = $Q3Beta * 40;
                    $profitQ3Charlie = 0;
               }
               else
               {
                    $Q3Beta = $totalProductB - $Q3Alpha - 49;
                    $profitQ3Beta = $Q3Beta * 40;
                    $Q3Charlie = $totalProductB - $Q3Alpha - $Q3Beta - $totalProductBAmount;
                    $profitQ3Charlie = $Q3Charlie * 30;
               }

               echo "Total Profit : ";
               echo $profit = $profitQ3Alpha + $profitQ3Beta + $profitQ3Charlie;
               echo "<br>";

               // $Q3Beta = $totalProductB - $Q3Alpha - 49;
               // $profitQ3Beta = $Q3Beta * 40;
               // // $Q3Charlie = $totalProductB - $Q3Alpha - $Q3Beta - $totalProductBAmount;
               // $Q3Charlie = $totalProductB - $Q3Alpha - $Q3Beta - 49;
               // $profitQ3Charlie = $Q3Charlie * 30;
               // $Q2Alpha = $totalProductB - $totalProductBAmount;
               // $profitQ2Alpha = $Q2Alpha * 40;
               // $Q2Beta = $totalProductB - $totalProductBAmount - $Q2Alpha;
               // $profitQ2Beta = $Q2Beta * 30;
               // echo "Total Profit : ";
               // echo $profit = $profitQ3Alpha + $profitQ3Beta + $profitQ3Charlie;
               // echo "<br>";
          }

          // echo "Total Profit : ";
          // echo $profit = $profitLvlOne + $profitLvlTwo + $profitLvlThree;
          // echo "<br>";
          // $profit = $profitLvlOne + $profitLvlTwo + $profitLvlThree;

     }
     // elseif($totalProductB >= 50)
     elseif($totalProductB >= 50 && $totalProductB <= 99)
     {
          $unitPrice = '199';
          // $level = '2';
          echo "Upgrade To Level : ";
          echo $level = '2';
          echo "<br>";
          // $profit = '40';
          // $lvlTwoOrderQuantity = $quantity - 49;
          // $profitOne = '30';
          // $profitLvlOne = $profitOne * 49;
          // $profitTwo = '40';
          // $profitLvlTwo = $profitTwo * $lvlTwoOrderQuantity;
          // echo "Total Profit : ";
          // echo $profit = $profitLvlOne + $profitLvlTwo;
          // echo "<br>";
          // $profit = $profitLvlOne + $profitLvlTwo;
          if($totalProductBAmount >= 50)
          {
               $Q2Alpha = $totalProductB - $totalProductBAmount;
               echo "Total Profit : ";
               echo $profit = $Q2Alpha * 40;
               echo "<br>";
          }
          else
          {
               $Q2Alpha = $totalProductB - $totalProductBAmount;
               $profitQ2Alpha = $Q2Alpha * 40;
               $Q2Beta = $totalProductB - $totalProductBAmount - $Q2Alpha;
               $profitQ2Beta = $Q2Beta * 30;
               echo "Total Profit : ";
               echo $profit = $profitQ2Alpha + $profitQ2Beta;
               echo "<br>";
          }

     }
     elseif($totalProductB < 50)
     {
          $unitPrice = '199';
          $level = '1';
          // $profit = '30';
          // $profit = $quantity * 30;
          // echo "Total Profit : ";
          // echo $profit = $quantity * 30;
          // echo "<br>";

          $Q1Alpha = 49 - $totalProductBAmount;
          echo "Total Profit : ";
          echo $profit = $Q1Alpha * 30;
          echo "<br>";

     }
     else
     {    
          echo "ERROR";
     }

     echo "Price (Before Deduct Profit) : ";
     echo $finalPrice = ($quantity * $unitPrice);
     echo "<br>";
     echo "Total Discount : ";
     echo $totalDiscount = $profit ;
     echo "<br>";
     echo "Actual Price : ";
     echo $totalPrice = $finalPrice - $totalDiscount;
     echo "<br>";

}
else 
{
     header('Location: ../index.php');
}
?>