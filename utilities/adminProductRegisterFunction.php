<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

// function registerNewProduct($conn,$uid,$name,$descriptionOne,$descriptionTwo,$imageOne,$status)
// {
//      if(insertDynamicData($conn,"product",array("uid","name","description","description_two","image_one","status"),
//           array($uid,$name,$descriptionOne,$descriptionTwo,$imageOne,$status),"ssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function registerNewProduct($conn,$uid,$name,$descriptionOne,$status)
{
     if(insertDynamicData($conn,"product",array("uid","name","description","status"),
          array($uid,$name,$descriptionOne,$status),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $stringTwo = ($_POST['register_name']);
     // $name = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringTwo);

     $name = rewrite($_POST['product_name']);
     // $price = rewrite($_POST['product_price']);
     $descriptionOne = rewrite($_POST['description_one']);
     // $descriptionTwo = rewrite($_POST['description_two']);
     $status = "Available";

     // $imageOne = $timestamp.$_FILES['image_one']['name'];
     // $target_dir = "../productImage/";
     // $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // // Select file type
     // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // // Valid file extensions
     // $extensions_arr = array("jpg","jpeg","png","gif");
     // if( in_array($imageFileType,$extensions_arr) )
     // {
     //      move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     // }

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $name."<br>";
     // echo $productName."<br>";
     // echo $price."<br>";

     $productNameRows = getProduct($conn," WHERE name = ? ",array("name"),array($_POST['product_name']),"s");
     $existingProductName = $productNameRows[0];

     if(!$existingProductName)
     {
          // if(registerNewProduct($conn,$uid,$name,$descriptionOne,$descriptionTwo,$imageOne,$status))
          if(registerNewProduct($conn,$uid,$name,$descriptionOne,$status))
          {
               $_SESSION['product_uid'] = $uid;
               header('Location: ../adminProductVariationAdd.php');
          }
          else
          {
               echo "FAIL";
          }
     }
     else
     {
          echo "ERROR";
     }
}
else
{
     header('Location: ../index.php');
}
?>