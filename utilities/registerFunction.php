<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$email,$firstname,$lastname,$icno,$dob,$country,$phoneNo,$address,$addresstwo,$zipcode,$state,$userType,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","firstname","lastname","icno","birth_date","country","phone_no","address","address_two","zipcode","state","user_type","password","salt"),
          array($uid,$username,$email,$firstname,$lastname,$icno,$dob,$country,$phoneNo,$address,$addresstwo,$zipcode,$state,$userType,$finalPassword,$salt),"sssssssssssssiss") === null)
     {
          // echo "gg";
          // header('Location: ../index.php?promptError=1');
          echo "<script>alert('Error during registration, please try again later !');window.location='../index.php'</script>";
          // echo "<script>alert('register details has been used by others');window.location='../register.php?referrerUID=.'$sponsorID'.</script>";
     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function sendEmailForVerification($uid)
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.hygeniegroup.com";
     $verifyUser_usernameThatSendEmail = "noreply@hygeniegroup.com";                   // Sender Acc Username
     $verifyUser_password = "M7vXtsRMSJO8";                                              // Sender Acc Password

     $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
     $verifyUser_port = 465;                                                              // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@hygeniegroup.com";                    // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@hygeniegroup.com";                        // Sender Email


     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                            // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Welcome To Hygenie Group";

     $verifyUser_body = "<p>Dear ".$userRows[0]->getUsername().",</p>";                      // Body
     $verifyUser_body .="<p>You have successfully registered for Hygenie Group.</p>";
     $verifyUser_body .="<p>Here are your temporary login details :</p>";
     $verifyUser_body .="<p>https://hygeniegroup.com/</p>";
     $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
     $verifyUser_body .="<p>Password : 111111</p>";
     $verifyUser_body .="<p>Thank you.</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}



if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $username = rewrite($_POST['register_username']);
     // $email = $username.'@gmail.com';
     // $firstname = $username.'1st';
     // $lastname = $username.'last';
     // $phoneNo = rewrite($_POST['register_mobileno']);
     // $icno = $phoneNo.'1808';
     // $dob = '18-08-2008';
     // $country = 'SG';
     // // $phoneNo = rewrite($_POST['register_mobileno']);
     // $address = '123, ABC';
     // $addresstwo = 'Tmn ASD, Jln Zxc';
     // $zipcode = '123333';
     // $state = 'Red Zone';
     // $userType = "1";
     // $sponsorID = rewrite($_POST['upline_uid']);

     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $firstname = rewrite($_POST['register_firstname']);
     $lastname = rewrite($_POST['register_lastname']);
     $icno = rewrite($_POST['register_icno']);
     $dob = rewrite($_POST['register_dob']);
     $country = rewrite($_POST['register_country']);
     $phoneNo = rewrite($_POST['register_mobileno']);
     $address = rewrite($_POST['register_address']);
     $addresstwo = rewrite($_POST['register_addresstwo']);
     $zipcode = rewrite($_POST['register_zipcode']);
     $state = rewrite($_POST['register_state']);
     $userType = "1";
     $sponsorID = rewrite($_POST['upline_uid']);

     // $register_password = "123321";
     // // $register_password_validation = strlen($register_password);
     // $register_retype_password = "123321";

     $register_password = "111111";
     // $register_password_validation = strlen($register_password);
     $register_retype_password = "111111";

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $email."<br>";
     // echo $finalPassword."<br>";
     // echo $salt."<br>";
     // echo $phoneNo ."<br>";
     // echo $fullName."<br>";
     // echo $nationality."<br>";
     // echo $userType."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

     // $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
     // $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
     // $usernameDetails = $usernameRows[0];

     $icRows = getUser($conn," WHERE icno = ? ",array("icno"),array($icno),"s");
     $icDetails = $icRows[0];

     // $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
     // $userEmailDetails = $userEmailRows[0];

     // $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_mobileno']),"s");
     // $userPhoneDetails = $userPhoneRows[0];

     // if (!$usernameDetails && !$icDetails)
     // if (!$usernameDetails)
     if (!$icDetails)
     {
          if($sponsorID)
          {
               $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
               if($referrerUserRows)
               {
                    $referrerUid = $referrerUserRows[0]->getUid();
                    $referrerName = $referrerUserRows[0]->getUsername();
                    // $referralUid = $referrerUserRows[0]->getUid();
                    $referralName = $username;
                    $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                    $currentLevel = 1;
                    $getUplineCurrentLevel = 1;

                    $currentRaking = $referrerUserRows[0]->getRank();
                    
                    // $currentValueDownlineLvlOne = $referrerUserRows[0]->getDownlineLvlOne();
                    // $currentValueDownlineLvlTwo = $referrerUserRows[0]->getDownlineLvlTwo();
                    // $currentValueDownlineLvlThree = $referrerUserRows[0]->getDownlineLvlThree();
                    // $currentValueDownlineLvlFour = $referrerUserRows[0]->getDownlineLvlFour();
                    // $currentValueDownlineLvlFive = $referrerUserRows[0]->getDownlineLvlFive();

                    // echo $currentValueDownline;

                    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                    if($referralHistoryRows)
                    {
                         $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                         $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                    }
                    $referralNewestRows = getReferralHistory($conn,"WHERE referral_id = ?", array("referral_id"),array($referrerUid), "s");
                    if($referralNewestRows)
                    {
                         $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

                              if(registerNewUser($conn,$uid,$username,$email,$firstname,$lastname,$icno,$dob,$country,$phoneNo,$address,$addresstwo,$zipcode,$state,$userType,$finalPassword,$salt))
                              {

                                   if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                   {

                                        // $newTotalDownlineLvlOne = $currentValueDownlineLvlOne + 1;
                                        // $newStatusManager = "Manager";
                                        // echo $newTotalDownline;

                                        if($currentRaking == 'Member')
                                        {                         
                                             $currentValueDownlineLvlOne = $referrerUserRows[0]->getDownlineLvlOne();
                                             $newTotalDownlineLvlOne = $currentValueDownlineLvlOne + 1;
                                             // if($currentValueDownline < 5)
                                             // if($newTotalDownlineLvlOne <= 5)
                                             if($newTotalDownlineLvlOne <= 6)
                                             {                         
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($newTotalDownlineLvlOne)
                                                  {
                                                       array_push($tableName,"downline_lvlone");
                                                       array_push($tableValue,$newTotalDownlineLvlOne);
                                                       $stringType .=  "s";
                                                  }                                
                                                  array_push($tableValue,$referrerUid);
                                                  $stringType .=  "s";
                                                  $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($updatedValue)
                                                  {                         
                                                       // $_SESSION['uid'] = $uid;
                                                       // $_SESSION['usertype_level'] = 1;
                                                       // header('Location: ../userDashboard.php');     
                                                       sendEmailForVerification($uid);
                                                       header('Location: ../logoutRS.php');
                                                  }
                                                  else
                                                  {    
                                                       echo "ERROR 1A";
                                                  }
                                             }
                                             //else
                                             // elseif($newTotalDownlineLvlOne > 5)
                                             elseif($newTotalDownlineLvlOne > 6)
                                             {    

                                                  $verifiedDownline = getReferralHistory($conn, "WHERE referrer_id = ? AND order_status = 'YES' ", array("referrer_id"), array($referrerUid), "s");
                                                  if($verifiedDownline)
                                                  {   
                                                      $totalConfirmDownline = count($verifiedDownline);
                                                  }
                                                  else
                                                  {   $totalConfirmDownline = 0;   }

                                                  if($totalConfirmDownline > 6)
                                                  {   
                                                       $newStatusManager = "Manager";

                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newTotalDownlineLvlOne)
                                                       {
                                                            array_push($tableName,"downline_lvlone");
                                                            array_push($tableValue,$newTotalDownlineLvlOne);
                                                            $stringType .=  "s";
                                                       }   
                                                       if($newStatusManager)
                                                       {
                                                            array_push($tableName,"rank");
                                                            array_push($tableValue,$newStatusManager);
                                                            $stringType .=  "s";
                                                       }                               
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($updatedValue)
                                                       {                         
                                                            $tableName = array();
                                                            $tableValue =  array();
                                                            $stringType =  "";
                                                            //echo "save to database";
                                                            if($newStatusManager)
                                                            {
                                                                 array_push($tableName,"current_status");
                                                                 array_push($tableValue,$newStatusManager);
                                                                 $stringType .=  "s";
                                                            }                               
                                                            array_push($tableValue,$referrerUid);
                                                            $stringType .=  "s";
                                                            $updatedValue = updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",$tableName,$tableValue,$stringType);
                                                            if($updatedValue)
                                                            {                         
                                                                 // $_SESSION['uid'] = $uid;
                                                                 // $_SESSION['usertype_level'] = 1;
                                                                 // header('Location: ../userDashboard.php');      
                                                                 sendEmailForVerification($uid);
                                                                 header('Location: ../logoutRS.php');
                                                            }
                                                            else
                                                            {    
                                                                 echo "ERROR 1B";
                                                            }
                                                       }
                                                       else
                                                       {    
                                                            echo "ERROR 1C";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newTotalDownlineLvlOne)
                                                       {
                                                            array_push($tableName,"downline_lvlone");
                                                            array_push($tableValue,$newTotalDownlineLvlOne);
                                                            $stringType .=  "s";
                                                       }                                
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($updatedValue)
                                                       {                         
                                                            // $_SESSION['uid'] = $uid;
                                                            // $_SESSION['usertype_level'] = 1;
                                                            // header('Location: ../userDashboard.php');       
                                                            sendEmailForVerification($uid);
                                                            header('Location: ../logoutRS.php');
                                                       }
                                                       else
                                                       {    
                                                            echo "ERROR 1A";
                                                       }
                                                  }



                                                  // $newStatusManager = "Manager";

                                                  // $tableName = array();
                                                  // $tableValue =  array();
                                                  // $stringType =  "";
                                                  // //echo "save to database";
                                                  // if($newTotalDownlineLvlOne)
                                                  // {
                                                  //      array_push($tableName,"downline_lvlone");
                                                  //      array_push($tableValue,$newTotalDownlineLvlOne);
                                                  //      $stringType .=  "s";
                                                  // }   
                                                  // if($newStatusManager)
                                                  // {
                                                  //      array_push($tableName,"rank");
                                                  //      array_push($tableValue,$newStatusManager);
                                                  //      $stringType .=  "s";
                                                  // }                               
                                                  // array_push($tableValue,$referrerUid);
                                                  // $stringType .=  "s";
                                                  // $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  // if($updatedValue)
                                                  // {                         
                                                  //      $tableName = array();
                                                  //      $tableValue =  array();
                                                  //      $stringType =  "";
                                                  //      //echo "save to database";
                                                  //      if($newStatusManager)
                                                  //      {
                                                  //           array_push($tableName,"current_status");
                                                  //           array_push($tableValue,$newStatusManager);
                                                  //           $stringType .=  "s";
                                                  //      }                               
                                                  //      array_push($tableValue,$referrerUid);
                                                  //      $stringType .=  "s";
                                                  //      $updatedValue = updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",$tableName,$tableValue,$stringType);
                                                  //      if($updatedValue)
                                                  //      {                         
                                                  //           $_SESSION['uid'] = $uid;
                                                  //           $_SESSION['usertype_level'] = 1;
                                                  //           header('Location: ../userDashboard.php');       
                                                  //      }
                                                  //      else
                                                  //      {    
                                                  //           echo "ERROR 1B";
                                                  //      }
                                                  // }
                                                  // else
                                                  // {    
                                                  //      echo "ERROR 1C";
                                                  // }
                                             }
                                        }
                                        elseif($currentRaking == 'Manager')
                                        {    
                                             // $currentValueDownlineLvlTwo = $referrerUserRows[0]->getDownlineLvlTwo();
                                             $currentValueDownlineLvlTwo = $referrerUserRows[0]->getDownlineLvlTwo();
                                             $newTotalDownlineLvlTwo = $currentValueDownlineLvlTwo + 1;

                                             // if($currentValueDownline < 3)
                                             if($currentValueDownline)
                                             {                         
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($newTotalDownlineLvlTwo)
                                                  {
                                                       array_push($tableName,"downline_lvltwo");
                                                       array_push($tableValue,$newTotalDownlineLvlTwo);
                                                       $stringType .=  "s";
                                                  }                                
                                                  array_push($tableValue,$referrerUid);
                                                  $stringType .=  "s";
                                                  $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($updatedValue)
                                                  {                         
                                                       // $_SESSION['uid'] = $uid;
                                                       // $_SESSION['usertype_level'] = 1;
                                                       // header('Location: ../userDashboard.php');       
                                                       sendEmailForVerification($uid);
                                                       header('Location: ../logoutRS.php');
                                                  }
                                                  else
                                                  {    
                                                       echo "ERROR 2A";
                                                  }
                                             }
                                             else
                                             { 
                                                  //echo "manager to senior manager";
                                             }
                                        }
                                        elseif($currentRaking == 'Senior Manager')
                                        {    

                                             $currentValueDownlineLvlThree = $referrerUserRows[0]->getDownlineLvlThree();
                                             // $currentValueDownlineLvlFour = $referrerUserRows[0]->getDownlineLvlFour();
                                             // $currentValueDownlineLvlFive = $referrerUserRows[0]->getDownlineLvlFive();

                                             $newTotalDownlineLvlThree = $currentValueDownlineLvlThree + 1;

                                             if($newTotalDownlineLvlThree)
                                             {                         
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($newTotalDownlineLvlThree)
                                                  {
                                                       array_push($tableName,"downline_lvlthree");
                                                       array_push($tableValue,$newTotalDownlineLvlThree);
                                                       $stringType .=  "s";
                                                  }                                
                                                  array_push($tableValue,$referrerUid);
                                                  $stringType .=  "s";
                                                  $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($updatedValue)
                                                  {                         
                                                       // $_SESSION['uid'] = $uid;
                                                       // $_SESSION['usertype_level'] = 1;
                                                       // header('Location: ../userDashboard.php');   
                                                       sendEmailForVerification($uid);
                                                       header('Location: ../logoutRS.php');
                                                  }
                                                  else
                                                  {    
                                                       echo "ERROR 3A";
                                                  }
                                             }
                                             else
                                             {    }
                                        }
                                        elseif($currentRaking == 'Area Manager')
                                        {    
                                             $currentValueDownlineLvlFour = $referrerUserRows[0]->getDownlineLvlFour();
                                             // $currentValueDownlineLvlFive = $referrerUserRows[0]->getDownlineLvlFive();

                                             $newTotalDownlineLvlFour = $currentValueDownlineLvlFour + 1;

                                             if($newTotalDownlineLvlFour)
                                             {                         
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($newTotalDownlineLvlFour)
                                                  {
                                                       array_push($tableName,"downline_lvlfour");
                                                       array_push($tableValue,$newTotalDownlineLvlFour);
                                                       $stringType .=  "s";
                                                  }                                
                                                  array_push($tableValue,$referrerUid);
                                                  $stringType .=  "s";
                                                  $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($updatedValue)
                                                  {                         
                                                       // $_SESSION['uid'] = $uid;
                                                       // $_SESSION['usertype_level'] = 1;
                                                       // header('Location: ../userDashboard.php');       
                                                       sendEmailForVerification($uid);
                                                       header('Location: ../logoutRS.php');
                                                  }
                                                  else
                                                  {    
                                                       echo "ERROR 4A";
                                                  }
                                             }
                                             else
                                             {    }
                                        }
                                        elseif($currentRaking == 'District Manager')
                                        {    
                                             $currentValueDownlineLvlFive = $referrerUserRows[0]->getDownlineLvlFive();

                                             $newTotalDownlineLvlFive = $currentValueDownlineLvlFive + 1;

                                             if($newTotalDownlineLvlFive)
                                             {                         
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($newTotalDownlineLvlFive)
                                                  {
                                                       array_push($tableName,"downline_lvlfive");
                                                       array_push($tableValue,$newTotalDownlineLvlFive);
                                                       $stringType .=  "s";
                                                  }                                
                                                  array_push($tableValue,$referrerUid);
                                                  $stringType .=  "s";
                                                  $updatedValue = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($updatedValue)
                                                  {                         
                                                       // $_SESSION['uid'] = $uid;
                                                       // $_SESSION['usertype_level'] = 1;
                                                       // header('Location: ../userDashboard.php');       
                                                       sendEmailForVerification($uid);
                                                       header('Location: ../logoutRS.php');
                                                  }
                                                  else
                                                  {    
                                                       echo "ERROR 5A";
                                                  }
                                             }
                                             else
                                             {    }
                                        }
                                        else
                                        {    
                                             echo "STATUS ERROR";
                                        }
                                   }
                                   else
                                   {
                                        // echo "fail to register via upline";
                                        echo "<script>alert('fail to register with upline');window.location='../index.php'</script>";
                                   }

                              }
                              else
                              {
                                   // echo "fail to register";
                                   echo "<script>alert('Fail To Register !');window.location='../index.php'</script>";
                              }
                    }
                    else
                    {
                         // echo "register error with referral ";
                         echo "<script>alert('register error with referral !');window.location='../index.php'</script>";
                    }

               }
               else
               {
                    // echo "unable to find related data sponsor ID!!";
                    echo "<script>alert('unable to find related data sponsor ID!!');window.location='../index.php'</script>";
               }
          }
          else
          {
               // echo "sponsor ID is unavailible !!";
               echo "<script>alert('sponsor ID is unavailible !!');window.location='../index.php'</script>";
          }
     }
     else
     {
          // echo "fail to register register";
          // echo "<script>alert('register details has been used by others');window.location='../index.php'</script>";
          // echo "<script>alert('register details has been used by others'); window.history.go(-1);</script>";
          echo "<script>alert('this IC number has been used by others'); window.history.go(-1);</script>";
     }

}
else
{
     header('Location: ../index.php');
}
?>