<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $announcementUid = rewrite($_POST["announcement_uid"]);
    $status = "Deleted";
    $type = "4";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    // $liveIdDetails = getLiveShare($conn," id = ?   ",array("id"),array($liveId),"s");   
    $announcementDetails = getAnnouncement($conn," uid = ? ",array("uid"),array($announcementUid),"s");   

    if(!$announcementDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }

        array_push($tableValue,$announcementUid);
        $stringType .=  "s";
        $statusUpdated = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($statusUpdated)
        {
            header('Location: ../adminAnnoucementAll.php');
        }
        else
        {
            echo "<script>alert('fail to delete annoucement !!');window.location='../adminAnnoucementAll.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminAnnoucementAll.php'</script>";  
    }

}
else 
{
    header('Location: ../index.php');
}
?>
