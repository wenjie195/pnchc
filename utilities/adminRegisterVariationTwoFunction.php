<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$moq,$comLvlOne,$comLvlTwo,$status,$level,$profit)
{
     if(insertDynamicData($conn,"variation",array("uid","product_uid","name","price","quantity","com_one","com_two","status","level","profit"),
          array($variationUid,$mainProductUid,$name,$price,$moq,$comLvlOne,$comLvlTwo,$status,$level,$profit),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $variationUid = md5(uniqid());
     
     // $name = rewrite($_POST['product_name']);
     $level = rewrite($_POST['level']);
     $moq = rewrite($_POST['moq']);
     $price = rewrite($_POST['unit_price']);
     $profit = rewrite($_POST['profit']);
     $comLvlOne = rewrite($_POST['com_lvl_one']);
     $comLvlTwo = rewrite($_POST['com_lvl_two']);
     $mainProductUid = rewrite($_POST['main_product_uid']);

     $mainProductDetails = getProduct($conn, "WHERE uid =?",array("uid"),array($mainProductUid),"s");
     $name = $mainProductDetails[0]->getName();

     $status = "Available";

     // $productNameDetails = getVariation($conn," WHERE name = ? ",array("name"),array($name),"s");
     // $registeredVariationName = $productNameDetails[0];

     if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$moq,$comLvlOne,$comLvlTwo,$status,$level,$profit))
     {
          $_SESSION['product_uid'] = $mainProductUid;
          header('Location: ../adminProductVariationTwoAdd.php');
     }
     else
     {
          echo "fail";
     }

     // if(!$registeredVariationName)
     // {
     //      if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$moq,$comLvlOne,$comLvlTwo,$status))
     //      {
     //           $_SESSION['product_uid'] = $mainProductUid;
     //           header('Location: ../adminProductVariationTwoAdd.php');
     //      }
     //      else
     //      {
     //           echo "fail";
     //      }
     // }
     // else
     // {
     //      echo "product name already register !!";
     // }
}
else
{
     header('Location: ../index.php');
}
?>