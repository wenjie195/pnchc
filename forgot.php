<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/forgot.php" />
<link rel="canonical" href="https://agentpnchc.com/forgot.php" />
<meta property="og:title" content="<?php echo _INDEX_FORGOT ?> | Pure & Cure" />
<title><?php echo _INDEX_FORGOT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding min-height100 padding-top login-bg padding-top-bottom">
	<p class="text-center"><img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="center-logo"></p>
    <h1 class="h1 red-text text-center login-h1"><?php echo _INDEX_FORGOT ?></b></h1>
    <div class="login-div margin-auto">
        <form action="utilities/forgotPasswordFunction.php" method="POST">
                <div class="fake-input-div">
                    <input type="email" class="input-css input-css2 clean dark-tur-text2" placeholder="<?php echo _SIGN_UP_YOUR_EMAIL ?>" id="email" name="email">
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <button class="clean white-button ow-red-bg white-text">
                    <?php echo _INDEX_SUBMIT ?>
                </button>
            </form>
        <div class="clear"></div>
        <p class="signup-p text-center"><?php echo _INDEX_NOT_MEMBER ?> <a href="signup.php" class="dark-tur-link signup-a"><?php echo _INDEX_SIGN_UP_NOW ?></a></p>
    </div>
</div>







<?php include 'js.php'; ?>
</body>
</html>