<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$creditDetails = getPayment($conn, " WHERE status = 'Pending' ");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminCreditPurchasePending.php" />
<link rel="canonical" href="https://agentpnchc.com/adminCreditPurchasePending.php" />
<meta property="og:title" content="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?> | Pure & Cure" />
<title><?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
        	<thead>
            	<tr>
                	<th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _INDEX_USERNAME ?></th>
                    <th><?php echo _PROFILE_PURCHASE_CREDIT ?></th>
                    <th><?php echo _TOPUP_REFERENCE ?></th>
                    <th><?php echo _TOPUP_RECEIPT ?></th>
                    <th><?php echo _TOPUP_STATUS ?></th>
                    <th><?php echo _TOPUP_DATE ?></th>

                    <?php
                    if($uid == '7ea1cc3effbf37e0381104ce39aa06e5')
                    {}
                    else
                    {
                    ?>
                       <th><?php echo _ADMIN_ACTION ?></th>
                    <?php
                    }
                    ?>

                    <!-- <th><?php //echo _ADMIN_ACTION ?></th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                    if($creditDetails)
                    {
                        for($cnt = 0;$cnt < count($creditDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $creditDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $creditDetails[$cnt]->getCredit();?></td>
                                <td><?php echo $creditDetails[$cnt]->getBankReference();?></td>

                                <td>
                                    <a href="./receipt/<?php echo $creditDetails[$cnt]->getReceipt();?>" data-fancybox="images-preview" title="<?php echo _TOPUP_RECEIPT ?>" class="dark-tur-link" target="_blank"><img src="img/order.png" class="icon-size opacity-hover" title="<?php echo _TOPUP_RECEIPT ?>" alt="<?php echo _TOPUP_RECEIPT ?>"></a>
                                </td>

                                <td><?php echo $creditDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $creditDetails[$cnt]->getDateCreated();?></td>

                                <?php
                                if($uid == '7ea1cc3effbf37e0381104ce39aa06e5')
                                {}
                                else
                                {
                                ?>
                                
                                    <td>
                                        <form action="utilities/adminCreditPurchaseApprovedFunction.php" method="POST" class="inline-block">
                                            <button class="clean transparent-button" type="submit" name="payment_uid" value="<?php echo $creditDetails[$cnt]->getUid();?>">
                                            <img src="img/approved.png" class="icon-size opacity-hover" title="<?php echo _ADMIN_APPROVE ?>" alt="<?php echo _ADMIN_APPROVE ?>">
                                            </button>
                                        </form> 

                                        <form action="utilities/adminCreditPurchaseRejectFunction.php" method="POST" class="inline-block">
                                            <button class="clean transparent-button" type="submit" name="payment_uid" value="<?php echo $creditDetails[$cnt]->getUid();?>">
                                                <img src="img/reject.png" class="icon-size opacity-hover" title="<?php echo _ADMIN_REJECT ?>" alt="<?php echo _ADMIN_REJECT ?>">
                                            </button>
                                        </form> 
                                    </td>

                                <?php
                                }
                                ?>

                                <!-- <td>
                                    <form action="utilities/adminCreditPurchaseApprovedFunction.php" method="POST" class="inline-block">
                                        <button class="clean transparent-button" type="submit" name="payment_uid" value="<?php //echo $creditDetails[$cnt]->getUid();?>">
                                           <img src="img/approved.png" class="icon-size opacity-hover" title="<?php //echo _ADMIN_APPROVE ?>" alt="<?php //echo _ADMIN_APPROVE ?>">
                                        </button>
                                    </form> 

                                    <form action="utilities/adminCreditPurchaseRejectFunction.php" method="POST" class="inline-block">
                                        <button class="clean transparent-button" type="submit" name="payment_uid" value="<?php //echo $creditDetails[$cnt]->getUid();?>">
                                            <img src="img/reject.png" class="icon-size opacity-hover" title="<?php //echo _ADMIN_REJECT ?>" alt="<?php //echo _ADMIN_REJECT ?>">
                                        </button>
                                    </form> 
                                </td> -->

                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

   
</div></div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>