<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$annoucement = getAnnouncement($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userAnnouncement.php" />
<link rel="canonical" href="https://agentpnchc.com/userAnnouncement.php" />
<meta property="og:title" content="<?php echo _PROFILE_ANNOUNCEMENT ?> | Pure & Cure" />
<title><?php echo _PROFILE_ANNOUNCEMENT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_ANNOUNCEMENT ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
	

    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

        <?php
            if($annoucement)
            {
                for($cnt = 0;$cnt < count($annoucement) ;$cnt++)
                {
                ?>

                    <div class="white-card-div ow-width100 ow-margin-bottom30">
                        <p class="dark-tur-text ann-title-p"><?php echo $annoucement[$cnt]->getTitle();?></p>
                        <p class="light-tur-text ann-date-p"><?php echo $annoucement[$cnt]->getDateCreated();?></p>
                        <p class="dark-tur-text ann-content-p"><?php echo $annoucement[$cnt]->getContent();?></p>
                    </div>

                <?php
                }
            }
        ?>   
               
    </div>
</div>
</div>

<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>