<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productVariation = getProduct($conn,"WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>



<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userProductDetailsB.php" />
<link rel="canonical" href="https://agentpnchc.com/userProductDetailsB.php" />
<meta property="og:title" content="<?php echo _STOCK_PRODUCT_DETAILS ?> | Pure & Cure" />
<title><?php echo _STOCK_PRODUCT_DETAILS ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _STOCK_PRODUCT_DETAILS ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow ">
    	<div class="center-div2">
       
       
    		    <img src="img/loveoil.png" alt="Eye Love Oil"   title="Eye Love Oil" class="product-image2">
        
            <div class="clear"></div>
            <h3 class="center-div-h3">Eye Love Oil</h3>  
            <p class="product-details-p dark-tur-text"><?php echo _STOCK_LO1 ?></p>
            <ul class="product-ul dark-tur-text ow-product-ul">
            	<li><?php echo _STOCK_LOLI1 ?></li>
                <li><?php echo _STOCK_LOLI2 ?></li>
                <li><?php echo _STOCK_LOLI3 ?></li>
                <li><?php echo _STOCK_LOLI4 ?></li>
                <li><?php echo _STOCK_LOLI5 ?></li>
                <li><?php echo _STOCK_LOLI6 ?></li>
            </ul>
       
   
            
                
                <!-- <a href="productPackageDetails.php"> -->
                <a href='productPackageDetails.php?id=5db36e7812212f57d9b1f21057491a01'>
                    <div class="pill-button yellow-hover-bg middle-center-button margin-top30">
						<?php echo _PROFILE_ADD_STOCK ?>
                    </div>
                </a>


            <div class="clear"></div>                
             
             
             
                
    </div>

</div></div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>