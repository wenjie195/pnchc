<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminProductAdd.php" />
<link rel="canonical" href="https://agentpnchc.com/adminProductAdd.php" />
<meta property="og:title" content="<?php echo _ADMIN_ADD_PRODUCT ?> | Pure & Cure" />
<title><?php echo _ADMIN_ADD_PRODUCT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>


<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ADMIN_ADD_PRODUCT ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

   
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x text-center">


    <form action="utilities/adminProductRegisterFunction.php" method="POST" enctype="multipart/form-data">

      
                <input type="text" class="rec-input clean ow-margin-left0 ow-width100" placeholder="<?php echo _ADMIN_PRODUCT_NAME ?>" id="product_name" name="product_name" required>
            

            <div class="clear"></div>

           
                <textarea  type="text" class="rec-input clean ow-margin-left0 ow-width100 text-area-height" placeholder="<?php echo _ADMIN_DESCRIPTION ?>" id="description_one" name="description_one"></textarea>
            


            <div class="clear"></div>       

            <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _PROFILE_CONFIRM ?></button>

        </div>

    </form>

    <div class="clear"></div>
   
</div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>