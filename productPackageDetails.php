<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $productA = getProduct($conn,"WHERE name = 'Product A' AND status = 'Available' ");
// $productADetails = $productA[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/productPackageDetails.php" />
<link rel="canonical" href="https://agentpnchc.com/productPackageDetails.php" />
<meta property="og:title" content="<?php echo _PROFILE_ADD_STOCK ?> | Pure & Cure" />
<title><?php echo _PROFILE_ADD_STOCK ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_ADD_STOCK ?></h1><?php include 'header.php'; ?>
	
</div>
<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
		    
        <?php
        // if($puppiesDetails)
        if(isset($_GET['id']))
        {
        $conn = connDB();
        $productADetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
        for($cnt = 0;$cnt < count($productADetails) ;$cnt++)
        {
        ?>

            <form method="POST" action="utilities/preOrderFunction.php">
                <div class="center-div2">

                <img src="productImage/<?php echo $productADetails[$cnt]->getImageOne();?>" alt="<?php echo _STOCK_CURRENT ?>"   title="<?php echo _STOCK_CURRENT ?>" class="center-icon">

                <div class="clear"></div>

                <?php 
                    $productName = $productADetails[$cnt]->getName();
                    if($productName == 'Product A')
                    {
                        $renameProductName = 'Colloid Plus';
                    }
                    elseif($productName == 'Product B')
                    {
                        $renameProductName = 'Eye Love Oil';
                    }
                    else
                    {
                        $renameProductName = $productName;
                    }
                ?>

                <h3 class="center-div-h3"><?php echo _PROFILE_ADD_STOCK ?> - <?php echo $renameProductName;?></h3> 

                <input type="hidden" class="rec-input clean ow-margin-left0" value="<?php echo $productADetails[$cnt]->getUid();?>" id="product_uid" name="product_uid" readonly>
                <input type="number" class="rec-input clean ow-margin-left0" placeholder="<?php echo _STOCK_ADD_AMOUNT ?>"  id="product_quantity" name="product_quantity" required>
                
                <div class="clear"></div>
                
                <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _STOCK_ADD_TO_CART ?></button>
            </form>

        <?php
        }
        ?>
        <?php
        }
        ?>



    </div>
</div>

<div class="clear"></div>
</div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>