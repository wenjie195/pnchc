<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $allUser = getUser($conn, " WHERE user_type = 1 ");
$orderDetails = getOrderList($conn, " WHERE status = 'Pending' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminSalesPending.php" />
<link rel="canonical" href="https://agentpnchc.com/adminSalesPending.php" />
<meta property="og:title" content="<?php echo _PROFILE_CURRENT_ORDER ?> | Pure & Cure" />
<title><?php echo _PROFILE_CURRENT_ORDER ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_CURRENT_ORDER ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
        <div class="width100 overflow-x">
            <table class="width100 tur-table">
                <thead>
                    <tr>
                        <th><?php echo _TOPUP_NO ?></th>
                        <th><?php echo _COMMISSION_PURCHASER ?></th>
                        <th><?php echo _STOCK_PRODUCT ?></th>
                        <th><?php echo _ORDER_UNIT_PRICE ?> (RM)</th>
                        <th><?php echo _STOCK_QUANTITY ?></th>
                        <th><?php echo _ORDER_ORIGINAL_PRICE ?> (RM)</th>
                        <th><?php echo _ORDER_DISCOUNT ?></th>
                        <th><?php echo _ORDER_SUBTOTAL ?> (RM)</th>
                        <th><?php echo _TOPUP_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($orderDetails)
                        {
                            for($cnt = 0;$cnt < count($orderDetails) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td>
                                        <?php 
                                            $userUid = $orderDetails[$cnt]->getUseruid();
                                            $conn = connDB();
                                            $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
                                            echo $usename = $userDetails[0]->getUsername();
                                        ?>
                                    </td>
                                    <td><?php echo $orderDetails[$cnt]->getProductName();?></td>
                                    <td><?php echo $orderDetails[$cnt]->getOriginalPrice();?></td>
                                    <td><?php echo $orderDetails[$cnt]->getQuantity();?></td>
                                    <td><?php echo $orderDetails[$cnt]->getFinalPrice();?></td>
                                    <td><?php echo $orderDetails[$cnt]->getDiscount();?></td>
                                    <td><?php echo $orderDetails[$cnt]->getTotalPrice();?></td>
                                    <td><?php echo $orderDetails[$cnt]->getDateCreated();?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>