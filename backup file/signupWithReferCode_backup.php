<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _INDEX_SIGN_UP ?> | Pure & Cure" />
<title><?php echo _INDEX_SIGN_UP ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding dark-tur-bg min-height100 padding-top">
	<p class="text-center"><img src="img/account.png"  class="center-logo"></p>
    <h1 class="h1 white-text text-center login-h1 ow-margin-bottom10"><?php echo _SIGN_UP_CREATE ?> <b><?php echo _SIGN_UP_ACCOUNT ?></b></h1>
    
    <div class="login-div margin-auto">
        <form action="utilities/registerWithRCFunction.php" method="POST">

            <?php
            // Program to display URL of current page.
            if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
            $link = "https";
            else
            $link = "http";

            // Here append the common URL characters.
            $link .= "://";

            // Append the host(domain name, ip) to the URL.
            $link .= $_SERVER['HTTP_HOST'];

            // Append the requested resource location to the URL
            $link .= $_SERVER['REQUEST_URI'];

            // Print the link
            // echo $link;
            ?>

            <?php
            $str1 = $link;
            if(isset($_GET['referrerUID']))
            {
            $referrerUidLink = $_GET['referrerUID'];
            }
            else
            {
            $referrerUidLink = "";
            }
            ?>

            <div class="white-border margin-auto"></div>

            <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php echo $referrerUidLink;?>" id="upline_uid" name="upline_uid" readonly> 

            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="<?php echo _INDEX_USERNAME ?>" id="register_username" name="register_username">
            </div>

            <div class="clear"></div>

            <div class="fake-input-div before-forgot">
                <img src="img/password.png" class="input-icon">
                <input type="password" class="input-css clean password-input dark-tur-text2" id="password" name="password" placeholder="<?php echo _INDEX_PASSWORD ?>">
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionC()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div>

            <div class="clear"></div>
        
            <button class="clean yellow-button white-text margin-top30" name="submit"> 
                <?php echo _SIGN_UP_CREATE ?>
            </button>
            
            <div class="clear"></div>

            <p class="signup-p text-center"><?php echo _INDEX_NOT_MEMBER ?> <a href="signup.php" class="light-green-link signup-a"><?php echo _INDEX_SIGN_UP_NOW ?></a></p>
        </form>
    </div>
</div>

<?php include 'js.php'; ?>
</body>
</html>