<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _INDEX_SIGN_UP ?> | Pure & Cure" />
<title><?php echo _INDEX_SIGN_UP ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding dark-tur-bg min-height100 padding-top">
	<p class="text-center"><img src="img/account.png"  class="center-logo"></p>
    <h1 class="h1 white-text text-center login-h1 ow-margin-bottom10"><?php echo _SIGN_UP_CREATE ?> <b><?php echo _SIGN_UP_ACCOUNT ?></b></h1>
    
    <div class="login-div margin-auto">
    	<div class="white-border margin-auto"></div>
    	<div class="fake-input-div">
        	
    		<input type="text" class="input-css input-css2 clean white-text" placeholder="<?php echo _SIGN_UP_YOUR_NAME ?>" required>
        </div>
        <div class="clear"></div>
      
    	<div class="fake-input-div">
        	
    		<input type="email" class="input-css input-css2 clean white-text" placeholder="<?php echo _SIGN_UP_YOUR_EMAIL ?>" required>
        </div>
        <div class="clear"></div>       
    	<div class="fake-input-div">
        	
    		<input type="text" class="input-css input-css2 clean white-text" placeholder="<?php echo _SIGN_UP_YOUR_PHONE_NO ?>" required>
        </div>
        <div class="clear"></div>          
    	<div class="fake-input-div">
        	
    		<input type="text" class="input-css input-css2 clean white-text" placeholder="<?php echo _SIGN_UP_IC_NO ?>" required>
        </div>
        <div class="clear"></div>  
     	<div class="fake-input-div">
        	
    		<input type="text" class="input-css input-css2 clean white-text" placeholder="<?php echo _SIGN_UP_WECHAT_ID ?>" required>
        </div>
        <div class="clear"></div>        
     	<div class="fake-input-div">
        	
    		<input type="text" class="input-css input-css2 clean white-text" placeholder="<?php echo _SIGN_UP_REFERRAL_CODE ?>" required>
        </div>
        <div class="clear"></div>                 
    	<div class="fake-input-div">
        	
    		<input type="text" class="input-css input-css2 clean white-text" placeholder="<?php echo _INDEX_USERNAME ?>" required>
        </div>
        <div class="clear"></div>           
        <div class="fake-input-div before-forgot">
        	
        	<input type="password" class="input-css clean icon-input password-input2 white-text" id="register_password" placeholder="<?php echo _INDEX_PASSWORD ?>">
            <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionA()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
        </div>
        <div class="clear"></div>
        <div class="fake-input-div before-forgot">
        	
        	<input type="password" class="input-css clean icon-input password-input2 white-text" id="register_retype_password" placeholder="<?php echo _SIGN_UP_RETYPE_PASSWORD ?>">
            <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionB()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
        </div>
        <div class="clear"></div>       
        
        <button class="clean yellow-button white-text margin-top30">
        	<?php echo _SIGN_UP_CREATE ?>
        </button>
        <div class="clear"></div>
        <p class="signup-p signup-p2 text-center"><?php echo _SIGN_UP_ALREADY_MEMBER ?> <a href="index.php" class="light-green-link signup-a"><?php echo _SIGN_UP_LOGIN_NOW ?></a></p>
        
        
    </div>
</div>




<style>
::-webkit-input-placeholder { /* Edge */
  color: white;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: white;
}

::placeholder {
  color: white;
}

</style>


<?php include 'js.php'; ?>
</body>
</html>