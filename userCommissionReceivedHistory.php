<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$commissionDetails = getBonus($conn, "WHERE receiver_uid = ? ORDER BY date_created DESC ",array("receiver_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCommissionReceivedHistory.php" />
<link rel="canonical" href="https://agentpnchc.com/userCommissionReceivedHistory.php" />
<meta property="og:title" content="<?php echo _COMMISSION_RECEIVED ?> | Pure & Cure" />
<title><?php echo _COMMISSION_RECEIVED ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _COMMISSION_RECEIVED ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow overflow-x">

    <?php $wallet = $userData->getWallet();?>
    <h3 class="top-title brown-text"><?php echo _COMMISSION_RECEIVED ?> : <?php echo number_format("$wallet",2);?></h3>
    <div class="clear"></div>
    <a href="userCreditWithdrawal.php">
        <div class="pill-button yellow-hover-bg text-center mid-cen">
            <?php echo _PROFILE_WITHDRAW ?>
        </div>
    </a>

    <div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _COMMISSION_ORDER_ID ?></th>
                    <th><?php echo _COMMISSION_PURCHASER ?></th>
                    <th><?php echo _STOCK_AMOUNT ?></th>
                    <th><?php echo _COMMISSION_TYPE ?></th>
                    <th><?php echo _TOPUP_DATE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($commissionDetails)
                    {
                        for($cnt = 0;$cnt < count($commissionDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>
                                    <a href='userCommissionOrderHistory.php?id=<?php echo $commissionDetails[$cnt]->getOrderUid();?>' class="light-green-link">
                                        <?php echo $commissionDetails[$cnt]->getOrderUid();?>
                                    </a>
                                </td>
                                <td><?php echo $commissionDetails[$cnt]->getUsername();?></td>

                                <!-- <td><?php echo $commissionDetails[$cnt]->getAmount();?></td> -->
                                
                                <?php $amount = $commissionDetails[$cnt]->getAmount();?>
                                <td><?php echo number_format("$amount",2);?></td>

                                <td><?php echo $commissionDetails[$cnt]->getBonusType();?></td>
                                <td><?php echo $commissionDetails[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
        </div>
    </div>
</div>


</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>