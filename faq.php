<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/faq.php" />
<link rel="canonical" href="https://agentpnchc.com/faq.php" />
<meta property="og:title" content="<?php echo _HEADER_FAQ ?> | Pure & Cure" />
<title><?php echo _HEADER_FAQ ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _HEADER_FAQ ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

   
    
    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
		  
           <!-- <h3 class="center-div-h3"><?php echo _HEADER_FAQ ?></h3>  -->
            

            <p class="pink-text note-p3 margin-bottom0">
				<?php echo _FAQ_Q1 ?> 
            </p> 
            <p class="dark-tur-text note-p3 margin-top3">
				<b><?php echo _FAQ_A1 ?></b><br>
                <a style="word-break:break-all;" href="./userCreditWithdrawal.php" class="pink-link info-p2"><?php echo _FAQ_LINK ?></a> 				<a href="./img/tutorial1.jpg" data-fancybox="images-preview"><img src="img/tutorial1.jpg" class="tutorial-img"></a>
            </p>            
    </div>
</div>
</div>
<div class="clear"></div>


<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>