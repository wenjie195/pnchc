<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/DeliverRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getUser($conn, " WHERE user_type = 1 ");
$allProduct = getProduct($conn, " WHERE status = 'Available' ");

$paymentRequest = getPayment($conn, " WHERE status = 'Pending' ");
$deliveryDetails = getDeliverRecord($conn, " WHERE status = 'Pending' ");
$withdrawalRequest = getWithdrawal($conn, " WHERE status = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminDashboard.php" />
<link rel="canonical" href="https://agentpnchc.com/adminDashboard.php" />
<meta property="og:title" content="<?php echo _PROFILE_DASHBOARD ?> | Pure & Cure" />
<title><?php echo _PROFILE_DASHBOARD ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="left-logo"><h1 class="top-title brown-text"><?php echo _PROFILE_DASHBOARD ?></h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    
	<div class="same-padding width100 min-sp-height padding-top overflow">
	<div class="width103 overflow">

        <?php
            if($paymentRequest)
            {   
                $totalPayment = count($paymentRequest);
            }
            else
            {   $totalPayment = 0;   }
        ?>

        <?php
            if($totalPayment > 0)
            {   
            ?>
                <a href="adminCreditPurchasePending.php">
                
                    <div class="four-div">
                        <div class="inner-green-box">
                            <img src="img/credit-with-dot.png" class="width100" alt="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" title="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" >
                            <p class="brown-text text-center"><?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?></p>
                        </div>
                    </div>
                </a>
            <?php
            }
            else
            {
            ?>
                <a href="adminCreditPurchasePending.php">
                    <div class="four-div">
                        <div class="inner-green-box">
                            <img src="img/credit.png" class="width100" alt="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" title="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" >
                            <p class="brown-text text-center"><?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?></p>
                        </div>
                    </div>
                </a>
            <?php
            }
        ?>

    	<!-- <a href="adminCreditPurchasePending.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/credit.png" class="width100" alt="<?php //echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" title="<?php //echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" >
                    <p class="brown-text text-center"><?php //echo _PROFILE_PURCHASE_CREDIT_REQUEST ?></p>
                </div>
            </div>
        </a> -->
     	
        <?php
            if($withdrawalRequest)
            {   
                $totalWithdrawal = count($withdrawalRequest);
            }
            else
            {   $totalWithdrawal = 0;   }
        ?>

        <?php
            if($withdrawalRequest > 0)
            {
            ?>
                <a href="adminWithdrawalPending.php">
                    <div class="four-div">
                        <div class="inner-green-box">
                            <img src="img/withdraw-dot.png" class="width100" alt="<?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" title="<?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" >
                            <p class="brown-text text-center"><?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?></p>
                        </div>
                    </div>
                </a>
            <?php
            }
            else
            {
            ?>
                <a href="adminWithdrawalPending.php">
                    <div class="four-div">
                        <div class="inner-green-box">
                            <img src="img/withdraw.png" class="width100" alt="<?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" title="<?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" >
                            <p class="brown-text text-center"><?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?></p>
                        </div>
                    </div>
                </a>
            <?php
            }
        ?>

        <!-- <a href="#"> -->
        <!-- <a href="adminWithdrawalPending.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/withdraw.png" class="width100" alt="<?php //echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" title="<?php //echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" >
                    <p class="brown-text text-center"><?php //echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?></p>
                </div>
            </div>
        </a> -->

    	<!-- <a href="#"> -->
        <a href="adminSalesPending.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/order.png" class="width100" alt="<?php echo _PROFILE_CURRENT_ORDER ?>" title="<?php echo _PROFILE_CURRENT_ORDER ?>" >
                    <p class="brown-text text-center"><?php echo _PROFILE_CURRENT_ORDER ?></p>
                </div>
            </div>
        </a> 
    	<!-- <a href="#"> -->

        <?php
            if($deliveryDetails)
            {   
                $totalDelivery = count($deliveryDetails);
            }
            else
            {   $totalDelivery = 0;   }
        ?>

        <?php
            if($deliveryDetails > 0)
            {
            ?>
                <a href="adminStockDeliverPending.php">
                    <div class="four-div">
                        <div class="inner-green-box">
                            <img src="img/stock-dot.png" class="width100" alt="<?php echo _ADMIN_TOTAL_DELIVERY ?>" title="<?php echo _ADMIN_TOTAL_DELIVERY ?>" >
                            <p class="brown-text text-center"><?php echo _ADMIN_TOTAL_DELIVERY ?></p>
                        </div>
                    </div>
                </a>
            <?php   
            }
            else
            {
            ?>
                <a href="adminStockDeliverPending.php">
                    <div class="four-div">
                        <div class="inner-green-box">
                            <img src="img/stock.png" class="width100" alt="<?php echo _ADMIN_TOTAL_DELIVERY ?>" title="<?php echo _ADMIN_TOTAL_DELIVERY ?>" >
                            <p class="brown-text text-center"><?php echo _ADMIN_TOTAL_DELIVERY ?></p>
                        </div>
                    </div>
                </a>
            <?php
            }
        ?>

        <!-- <a href="adminStockDeliverPending.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/stock.png" class="width100" alt="<?php //echo _ADMIN_TOTAL_DELIVERY ?>" title="<?php //echo _ADMIN_TOTAL_DELIVERY ?>" >
                    <p class="brown-text text-center"><?php //echo _ADMIN_TOTAL_DELIVERY ?></p>
                </div>
            </div>
        </a> -->

     	<a href="adminCommissionAll.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/credit3.png" class="width100" alt="<?php echo _COMMISSION ?>" title="<?php echo _COMMISSION ?>" >
                    <p class="brown-text text-center"><?php echo _COMMISSION ?></p>
                </div>
            </div>
        </a>
    	<a href="adminProductAll.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/package.png" class="width100" alt="<?php echo _PROFILE_PRODUCT_PACKAGE ?>" title="<?php echo _PROFILE_PRODUCT_PACKAGE ?>" >
                    <p class="brown-text text-center"><?php echo _PROFILE_PRODUCT_PACKAGE ?></p>
                </div>
            </div>
        </a>      
    	<!-- <a href="#"> -->
        <a href="adminWithdrawalTotalCommission.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/coin-in-hand.png" class="width100" alt="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAW ?>" title="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAW ?>" >
                    <p class="brown-text text-center"><?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAW ?></p>
                </div>
            </div>
        </a>           
    	<a href="adminMemberAll.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/team.png" class="width100" alt="<?php echo _ADMIN_TOTAL_MEMBER ?>" title="<?php echo _ADMIN_TOTAL_MEMBER ?>" >
                    <p class="brown-text text-center"><?php echo _ADMIN_TOTAL_MEMBER ?></p>
                </div>
            </div>
        </a>	
    	<a href="adminSalesAll.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/sales.png" class="width100" alt="<?php echo _PROFILE_SALES_DASHBOARD ?>" title="<?php echo _PROFILE_SALES_DASHBOARD ?>" >
                    <p class="brown-text text-center"><?php echo _PROFILE_SALES_DASHBOARD ?></p>
                </div>
            </div>
        </a>        
        <a href="adminCheckMaintenance.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/profile2.png" class="width100" alt="<?php echo _ADMIN_RUN ?>" title="<?php echo _ADMIN_RUN ?>" >
                    <p class="brown-text text-center"><?php echo _ADMIN_RUN ?></p>
                </div>
            </div>
        </a> 
    </div>

<!--            <?php
            if($allUser)
            {   
                $totalUser = count($allUser);
            }
            else
            {   $totalUser = 0;   }
            ?>
            <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Total User : <?php echo $totalUser;?>" readonly>

            <?php
            if($allProduct)
            {   
                $totalProduct = count($allProduct);
            }
            else
            {   $totalProduct = 0;   }
            ?>
            <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Total Product : <?php echo $totalProduct;?>" readonly>

    <p class="signup-p text-center"><a href="adminProductAll.php" class="dark-tur-link signup-a">View All Product</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="adminProductAdd.php" class="dark-tur-link signup-a">Add Product</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="adminCreditPurchasePending.php" class="dark-tur-link signup-a">Purchase Credit (Pending)</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="adminCreditPurchaseHistory.php" class="dark-tur-link signup-a">Purchase Credit (History)</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="logout.php" class="dark-tur-link signup-a">Logout</a></p>

    <div class="clear"></div>-->
  </div> 
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>