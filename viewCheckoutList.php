<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
// require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
$products = getOrderList($conn, "WHERE user_uid = ? AND order_id = ? ",array("user_uid","order_id"),array($uid,$orderUid),"ss");

// $states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/viewCheckoutList.php" />
<link rel="canonical" href="https://agentpnchc.com/viewCheckoutList.php" />
<meta property="og:title" content="<?php echo _ORDER_DETAILS ?> | Pure & Cure" />
<title><?php echo _ORDER_DETAILS ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ORDER_DETAILS ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
        
    <div class="width100 same-padding details-min-height padding-top2 overflow overflow-x">
    	<div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _STOCK_PRODUCT ?></th>
                    <th><?php echo _ORDER_UNIT_PRICE ?> (RM)</th>
                    <th><?php echo _STOCK_QUANTITY ?></th>
                    <th><?php echo _ORDER_ORIGINAL_PRICE ?></th>
                    <th><?php echo _ORDER_DISCOUNT ?></th>
                    <th><?php echo _ORDER_SUBTOTAL ?> (RM)</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($products)
                    {
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>
                                    <!-- <?php //echo $products[$cnt]->getProductName();?> -->
                                    <?php 
                                        $productName = $products[$cnt]->getProductName();
                                        if($productName == 'Product A')
                                        {
                                            $renameProductName = 'Colloid Plus';
                                        }
                                        elseif($productName == 'Product B')
                                        {
                                            $renameProductName = 'Eye Love Oil';
                                        }
                                        else
                                        {
                                            $renameProductName = $productName;
                                        }
                                        echo $renameProductName;
                                    ?>
                                </td>
                                <td><?php echo $products[$cnt]->getOriginalPrice();?></td>
                                <td><?php echo $products[$cnt]->getQuantity();?></td>

                                <!-- <td><?php echo $products[$cnt]->getFinalPrice();?></td> -->
                                <?php $finalPrice = $products[$cnt]->getFinalPrice();?>
                                <td><?php echo number_format("$finalPrice",2);?></td>

                                <!-- <td><?php echo $products[$cnt]->getDiscount();?></td> -->
                                <?php 
                                    $discount = $products[$cnt]->getDiscount();
                                    if($discount == 0)
                                    {
                                        $renameDiscount = 0;
                                    }
                                    else
                                    {
                                        $renameDiscount = $discount;
                                    }
                                ?>
                                <td><?php echo number_format("$renameDiscount",2);?></td>

                                <!-- <td><?php echo $products[$cnt]->getTotalPrice();?></td> -->
                                <?php $totalPrice = $products[$cnt]->getTotalPrice();?>
                                <td><?php echo number_format("$totalPrice",2);?></td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>

        <!-- <div class="width100 same-padding details-min-height padding-top2 overflow center-div2"> -->
            <?php
            if($products)
            {
            $totalOrderAmount = 0;
            for ($cnt=0; $cnt <count($products) ; $cnt++)
            {
                $totalOrderAmount += $products[$cnt]->getTotalPrice();
                // echo "<br>";
                // echo '123';
            }
            }
            else
            {
                $totalOrderAmount = 0 ;
            }
            ?>
		</div>
		<div class="clear"></div>

        
            <form method="POST" action="utilities/createOrderFunction.php">

                <!-- <p class="review-product-name">Delivery Details</p> -->
                <!-- <p class="center-div-h3">Delivery Details</p> -->

                <input class="rec-input clean" type="hidden" id="name" name="name" placeholder="<?php echo _SIGN_UP_NAME ?>" value="<?php echo $userDetails[0]->getUsername();?>" required>
                
                <input class="rec-input clean" type="hidden" id="phone" name="phone" placeholder="<?php echo _SIGN_UP_PHONE_NO ?>" value="<?php echo $userDetails[0]->getPhoneNo();?>" required> 

                <input class="rec-input clean" type="hidden" id="email" name="email" placeholder="<?php echo _SIGN_UP_EMAIL ?>" value="<?php echo $userDetails[0]->getEmail();?>" required>   
        
                <div class="clear"></div> 

                <input type="hidden" id="order_uid" name="order_uid" value="<?php echo $orderUid ?>" readonly>
                <input type="hidden" id="subtotal" name="subtotal" value="<?php echo $totalOrderAmount;?>" readonly> 

                <div class="clear"></div>  

                <button class="clean yellow-btn edit-profile-width margin-top30" name="submit"><?php echo _PROFILE_CONFIRM ?></button>

            </form>

   

    </div>

</div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>