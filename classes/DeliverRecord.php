<?php
class DeliverRecord {
    /* Member variables */
    var $id,$transactionUid,$uid,$username,$stockType,$previousAmount,$amount,$receiver,$address,$contact,$status,$shippingStatus,$shippingMethod,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTransactionUid()
    {
        return $this->transactionUid;
    }

    /**
     * @param mixed $transactionUid
     */
    public function setTransactionUid($transactionUid)
    {
        $this->transactionUid = $transactionUid;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStockType()
    {
        return $this->stockType;
    }

    /**
     * @param mixed $stockType
     */
    public function setStockType($stockType)
    {
        $this->stockType = $stockType;
    }

    /**
     * @return mixed
     */
    public function getPreviousAmount()
    {
        return $this->previousAmount;
    }

    /**
     * @param mixed $previousAmount
     */
    public function setPreviousAmount($previousAmount)
    {
        $this->previousAmount = $previousAmount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param mixed $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getShippingStatus()
    {
        return $this->shippingStatus;
    }

    /**
     * @param mixed $shippingStatus
     */
    public function setShippingStatus($shippingStatus)
    {
        $this->shippingStatus = $shippingStatus;
    }

    /**
     * @return mixed
     */
    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }

    /**
     * @param mixed $shippingMethod
     */
    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }
        /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }
}

function getDeliverRecord($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","transaction_uid","uid","username","stock_type","previous_amount","amount","receiver","address","contact","status",
                            "shipping_status","shipping_method","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"deliver_record");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$transactionUid,$uid,$username,$stockType,$previousAmount,$amount,$receiver,$address,$contact,$status,$shippingStatus,$shippingMethod,
                            $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new DeliverRecord();
            $class->setId($id);
            $class->setTransactionUid($transactionUid);
            $class->setUid($uid);
            $class->setUsername($username);
            $class->setStockType($stockType);
            $class->setPreviousAmount($previousAmount);
            $class->setAmount($amount);
            $class->setReceiver($receiver);
            $class->setAddress($address);
            $class->setContact($contact);
            $class->setStatus($status);
            $class->setShippingStatus($shippingStatus);
            $class->setShippingMethod($shippingMethod);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
