<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" />
<meta property="og:title" content="<?php echo _PROFILE_EDIT_PROFILE ?> | Pure & Cure" />
<title><?php echo _PROFILE_EDIT_PROFILE ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_EDIT_PROFILE ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">

	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding normal-min-height padding-top overflow">
		      
        <form action="utilities/editProfileFunction.php" method="POST">
            <input type="text" class="rec-input clean" placeholder="<?php echo _SIGN_UP_YOUR_NAME ?>" value="<?php echo $userData->getFullname();?>" id="update_fullname" name="update_fullname" required>

            <div class="clear"></div>

            <input type="email" class="rec-input clean" placeholder="<?php echo _SIGN_UP_YOUR_EMAIL ?>" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>

            <div class="clear"></div>    

            <input type="text" class="rec-input clean" placeholder="<?php echo _SIGN_UP_YOUR_PHONE_NO ?>" value="<?php echo $userData->getPhoneNo();?>" id="update_phone" name="update_phone" required>

            <div class="clear"></div>   

            <input type="text" class="rec-input clean" placeholder="<?php echo _SIGN_UP_IC_NO ?>" value="<?php echo $userData->getIcno();?>" id="update_icno" name="update_icno" required>

            <div class="clear"></div>  

            <input type="text" class="rec-input clean" placeholder="<?php echo _SIGN_UP_WECHAT_ID ?>" value="<?php echo $userData->getWeChatId();?>" id="update_wechatid" name="update_wechatid" required>

            <div class="clear"></div>      

            <!--<input type="text" class="rec-input clean" placeholder="<?php echo _INDEX_USERNAME ?>" value="<?php echo $userData->getUsername();?>" id="update_username" name="update_username" readonly>-->

            <div class="clear"></div>               
            
            <select type="text" class="rec-input clean" id="update_nationality" name="update_nationality" required>

                <?php
                if($userData->getCountry() == '')
                {
                ?>
                    <option value='Malaysia'>Malaysia</option>
                    <option value='Singapore'>Singapore</option>
                <?php
                }
                else if($userData->getCountry() == 'Malaysia')
                {
                ?>
                    <option selected value='Malaysia'>Malaysia</option>
                    <option value='Singapore'>Singapore</option>
                <?php
                }
                elseif($userData->getCountry() == 'Singapore')
                {
                ?>
                    <option value='Malaysia'>Malaysia</option>
                    <option selected value='Singapore'>Singapore</option>
                <?php
                }
                else
                {
                ?>
                    <option value='Malaysia'>Malaysia</option>
                    <option value='Singapore'>Singapore</option>
                <?php
                }
                ?>
                <!-- <option value='Malaysia'>Malaysia</option>
                <option value='Singapore'>Singapore</option> -->
            </select>

            <div class="clear"></div> 

            <input type="text" class="rec-input clean" placeholder="<?php echo _PROFILE_ADDRESS ?>" value="<?php echo $userData->getAddress();?>" id="update_address" name="update_address">

            <div class="clear"></div> 

            <input type="text" class="rec-input clean" placeholder="<?php echo _TOPUP_BANK ?>" value="<?php echo $userData->getBankName();?>" id="update_bank" name="update_bank">

            <div class="clear"></div> 

            <input type="text" class="rec-input clean" placeholder="<?php echo _TOPUP_BANK_ACC_HOLDER ?>" value="<?php echo $userData->getBankAccName();?>" id="update_bank_holder" name="update_bank_holder">    

            <div class="clear"></div> 
            
            <input type="text" class="rec-input clean" placeholder="<?php echo _TOPUP_BANK_ACC_NO ?>" value="<?php echo $userData->getBankAccNumber();?>" id="update_bank_account" name="update_bank_account">           
            <div class="clear"></div>  


            <div class="fake-input-div2">
                <input type="password" class="rec-input clean" id="register_password" name="register_password" placeholder="<?php echo _INDEX_PASSWORD ?>">
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionA()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div>

            <div class="clear"></div>

            <div class="fake-input-div2">
                <input type="password" class="rec-input clean" id="register_retype_password" name="register_retype_password" placeholder="<?php echo _SIGN_UP_RETYPE_PASSWORD ?>">
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionB()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div> 

            <button class="clean yellow-btn edit-profile-width" name="submit"><?php echo _PROFILE_CONFIRM ?></button>          
        </form>    
    </div>
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>