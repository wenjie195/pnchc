<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
// $orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productA = getUser($conn, "WHERE uid = ? AND value_a != '' ",array("uid"),array($uid),"s");
$productB = getUser($conn, "WHERE uid = ? AND value_b != '' ",array("uid"),array($uid),"s");

$productAUid = getProduct($conn,"WHERE name = 'Product A' ");
$productBUid = getProduct($conn,"WHERE name = 'Product B' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/stockInventory.php" />
<link rel="canonical" href="https://agentpnchc.com/stockInventory.php" />
<meta property="og:title" content="<?php echo _PROFILE_STOCK_INVENTORY ?> | Pure & Cure" />
<title><?php echo _PROFILE_STOCK_INVENTORY ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_STOCK_INVENTORY ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	
  <div class="width100 inner-bg inner-padding">

    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow  ow-white-card-czz">
        <div class="center-div2">
            <img src="img/stock2.png" alt="<?php echo _STOCK_CURRENT ?>"   title="<?php echo _STOCK_CURRENT ?>" class="center-icon">
            <div class="clear"></div>
            <h3 class="center-div-h3"><?php echo _STOCK_CURRENT ?></h3>  
        </div>

        <div class="white-card-div">
            <p class="dark-tur-text card-left-p ow-left-card-p">Colloid Plus</p>
            <?php
            if($productA)
            {
                for($cnt = 0;$cnt < count($productA) ;$cnt++)
                {
                    $productAValue = $productA[$cnt]->getValueA();
                }
            }
            else
            {
                $productAValue = 0 ;
            }
            ?>    

            <p class="dark-tur-text card-left-p ow-card-right-p"><?php echo $productAValue;?> <?php echo _STOCK_PCS ?></p>
        </div> 

        <div class="clear"></div>

        <div class="center-div2">
            <!-- <a href="addStock.php"><div class="pill-button yellow-hover-bg open-topup three-btn-row ow-flex-three"><?php echo _PROFILE_ADD_STOCK ?></div></a> -->

            <?php
            if($productAUid)
            {
                for($cnt = 0;$cnt < count($productAUid) ;$cnt++)
                {
                ?>
                <a href='productPackageDetails.php?id=<?php echo $productAUid[$cnt]->getUid();?>'>
                    <div class="pill-button yellow-hover-bg open-topup five-btn">
                        <?php echo _PROFILE_ADD_STOCK ?>
                    </div>
                </a>
                <?php
                }
                ?>
            <?php
            }
            ?>

            <!-- <a href="productPackage.php"><div class="pill-button yellow-hover-bg open-topup three-btn-row ow-flex-three"><?php echo _PROFILE_ADD_STOCK ?></div></a> -->

            <!-- <a href="userStockTransfer.php"> -->
            <form action="userStockTransfer.php" method="POST"  class="five-btn five-second" >
                <input type="hidden" value='A' id="product_name" name="product_name" readonly>
                <button class="clean transparent-btn width100" name="submit">
                    <div class="pill-button yellow-hover-bg width100">
                        <?php echo _PROFILE_TRANSFER_STOCK ?>
                    </div>
                </button>
            </form>
                <a href='userStockTransferHistory.php'>
                    <div class="pill-button yellow-hover-bg open-topup five-btn">
                        <?php echo _PROFILE_TRANSFER_STOCK_HISTORY ?>
                    </div>
                </a>                
            <form action="userStockDeliver.php" method="POST"  class="five-btn five-second" >
                <input type="hidden" value='A' id="product_name" name="product_name" readonly>
                <button class="clean transparent-btn width100" name="submit"><div class="pill-button green-hover-bg width100">
                <?php echo _PROFILE_DELIVER_STOCK ?></div></button>
            </form>
                <a href='userStockDeliveryHistory.php'>
                    <div class="pill-button green-hover-bg open-topup five-btn last-five-btn">
                        <?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?>
                    </div>
                </a>               
        </div>

        <div class="clear"></div>   

        <!-- <div class="white-card-div margin-top30">
            <p class="dark-tur-text card-left-p ow-left-card-p">Eye Love Oil</p>
            <?php
            if($productB)
            {
                for($cnt = 0;$cnt < count($productB) ;$cnt++)
                {
                    $productBValue = $productB[$cnt]->getValueB();
                }
            }
            else
            {
                $productBValue = 0 ;
            }
            ?>  
            <p class="dark-tur-text card-left-p ow-card-right-p"><?php echo $productBValue;?> <?php echo _STOCK_PCS ?></p>
        </div>  -->

        <div class="clear"></div>

        <!-- <div class="center-div2">
            <?php
            if($productBUid)
            {
                for($cnt = 0;$cnt < count($productBUid) ;$cnt++)
                {
                ?>
                <a href='productPackageDetails.php?id=<?php echo $productBUid[$cnt]->getUid();?>'>
                    <div class="pill-button yellow-hover-bg open-topup five-btn">
                        <?php echo _PROFILE_ADD_STOCK ?>
                    </div>
                </a>
                <?php
                }
                ?>
            <?php
            }
            ?>

            <form action="userStockTransfer.php" method="POST"  class="five-btn five-second" >
                <input type="hidden" value='B' id="product_name" name="product_name" readonly>
                <button class="clean transparent-btn width100" name="submit">
                    <div class="pill-button yellow-hover-bg width100">
                        <?php echo _PROFILE_TRANSFER_STOCK ?>
                    </div>
                </button>
            </form>
                <a href='userStockTransferHistory.php'>
                    <div class="pill-button yellow-hover-bg open-topup five-btn">
                        <?php echo _PROFILE_TRANSFER_STOCK_HISTORY ?>
                    </div>
                </a> 
            <form action="userStockDeliver.php" method="POST"  class="five-btn five-second" >
                <input type="hidden" value='B' id="product_name" name="product_name" readonly>
                <button class="clean transparent-btn width100" name="submit">
                    <div class="pill-button green-hover-bg width100">
                        <?php echo _PROFILE_DELIVER_STOCK ?>
                    </div>
                </button>
            </form>
                <a href='userStockDeliveryHistory.php'>
                    <div class="pill-button green-hover-bg open-topup five-btn last-five-btn">
                        <?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?>
                    </div>
                </a>             
        </div> -->
        
        <div class="clear"></div>     
    </div>

</div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Insufficent Credit <br> Please Reload !!"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Insufficent Credit for Transfer !!"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Insufficent Credit for Deliver !!"; 
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Delivery Request Submitted !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>