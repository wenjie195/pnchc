<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $myOrders = getOrders($conn);
// $myOrders = getOrders($conn, " WHERE payment_status = 'APPROVED' ");
// $myOrders = getOrders($conn, "WHERE uid =? AND payment_status = 'APPROVED' ",array("uid"),array($uid),"s");

$groupSales = 0; // initital
$groupSalesFormat = number_format(0,4); // initital
$directDownline = 0; // initital
$personalSales = 0; // initital

$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $uid, false);
if ($referrerDetails)
{
  for ($i=0; $i <count($referrerDetails) ; $i++)
  {
    $currentLevel = $referrerDetails[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel)
    {
      $directDownline++;
    }
    $referralId = $referrerDetails[$i]->getReferralId();
    // $downlineDetails = getOrders($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"),array($referralId), "s");
    if ($downlineDetails)
    {
      for ($b=0; $b <count($downlineDetails) ; $b++)
      {
        $personalSales += $downlineDetails[$b]->getSubtotal();
      }
    }
  }
  $groupSales += $personalSales;
  $groupSalesFormat = number_format($groupSales,4);
}

$myDownline = getReferralHistory($conn, "WHERE referrer_id =?",array("referrer_id"),array($uid),"s");

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
if ($getWho)
{
    $groupMember = count($getWho);
}
else
{
    $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userdashboard.php" />
<link rel="canonical" href="https://agentpnchc.com/userdashboard.php" />
<meta property="og:title" content="<?php echo _PROFILE_DASHBOARD ?> | Pure & Cure" />
<title><?php echo _PROFILE_DASHBOARD ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>


<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _FOOTER_HOME ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
	
    	
        
<!--	<div class="width100 same-padding padding-top overflow">
	<div class="top-profile-div">
            <div class="right-profile-div">
        	<div class="profile-img-div">
            
                <?php 
                    $profileIamge = $userData->getImage();
                    if($profileIamge != '')
                    {
                    ?>
                        <img src="profileImage/<?php echo $userData->getImage();?>" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
                    <?php
                    }
                    else
                    {
                    ?>
                        <img src="img/account.png" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
                    <?php
                    }
                ?>

            </div>
            <a href="editProfile.php"><div class="line-button"><img src="img/edit.png" class="edit-icon" alt="<?php echo _PROFILE_EDIT_PROFILE ?>" title="<?php echo _PROFILE_EDIT_PROFILE ?>"><?php echo _PROFILE_EDIT ?></div></a>
        </div>
    	<div class="left-profile-detail">
        	<table class="profile-table">
            	<tr>
                	<td><?php echo _PROFILE_USER_ID ?></td>
                    <td>:&nbsp;</td>
                    <td><?php echo $userData->getId();?></td>
                </tr>
             	<tr>
                	<td><?php echo _INDEX_USERNAME ?></td>
                    <td>:&nbsp;</td>
                    <td><?php echo $userData->getUsername();?></td>
                </tr> 
             	<tr>
                	<td><?php echo _PROFILE_NATIONALITY ?></td>
                    <td>:&nbsp;</td>
                    <td><?php echo $userData->getCountry();?></td>
                </tr>                 
             	<tr>
                	<td><?php echo _SIGN_UP_WECHAT_ID ?></td>
                    <td>:&nbsp;</td>
                    <td><?php echo $userData->getWeChatId();?></td>
                </tr>   
             	<tr>
                	
                    <td>Colloid Plus</td>
                    <td>:&nbsp;</td>
                    <td>
                        <?php 
                            $rankA = $userData->getRankA();
                            if($rankA == '1')
                            {
                                $renameRankA = 'Member';
                            }
                            elseif($rankA == '2')
                            {
                                $renameRankA = 'Angel Agent';
                            }
                            elseif($rankA == '3')
                            {
                                $renameRankA = 'Silver Dealer';
                            }
                            elseif($rankA == '4')
                            {
                                $renameRankA = 'Ace Dealer';
                            }
                            elseif($rankA == '5')
                            {
                                $renameRankA = 'CO-Founder';
                            }
                            else
                            {
                                $renameRankA = $rankA;
                            }
                            echo $renameRankA;
                        ?> | <?php echo $userData->getMaintenanceA();?>
                    </td>
                </tr>     
                
             

             	<tr>
                	<td><?php echo _SUCCESS_REFER_LINK ?></td>
                    <td>:&nbsp;</td>
                    <td>
                    
                        <?php
                            $actual_link = $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                            $fullPath = dirname($path);
                        ?>

                        <input type="hidden" id="linkCopy" value="<?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?>">
                        <a id="invest-now-referral-link" style="word-break:break-all;" href="<?php echo 'https://'.$fullPath.'/signupWithReferCode.php?referrerUID='.$_SESSION['uid'] ?>" class="dark-tur-link info-p2" target="_blank"><?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?></a></h3>
                        <button class="invite-btn  clean ow-red-bg white-text clean-button" id="copy-referral-link">Copy</button>

                    </td>
                </tr>                                            
            </table>
        </div>

    </div>
</div>-->

<div class="clear"></div>
<div class="same-padding width100 min-sp-height overflow">
	<div class="width103 overflow xx">
    	<a href="profile.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/profile2.png" class="width100" alt="<?php echo _PROFILE ?>" title="<?php echo _PROFILE ?>" >
                    <p class="brown-text"><?php echo _PROFILE ?></p>
                </div>
            </div>
        </a>
     	<!-- <a href="totalCredit.php"> -->
         <a href="userCreditTotal.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/credit.png" class="width100" alt="<?php echo _PROFILE_TOTAL_CREDIT ?>" title="<?php echo _PROFILE_TOTAL_CREDIT ?>" >
                    <p class="brown-text"><?php echo _PROFILE_TOTAL_CREDIT ?></p>
                </div>
            </div>
        </a>
    	<a href="stockInventory.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/stock.png" class="width100" alt="<?php echo _PROFILE_STOCK_INVENTORY ?>" title="<?php echo _PROFILE_STOCK_INVENTORY ?>" >
                    <p class="brown-text"><?php echo _PROFILE_STOCK_INVENTORY ?></p>
                </div>
            </div>
        </a> 
    	<a href="userDownline.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/team.png" class="width100" alt="<?php echo _PROFILE_DOWNLINE_DASHBOARD ?>" title="<?php echo _PROFILE_DOWNLINE_DASHBOARD ?>" >
                    <p class="brown-text"><?php echo _PROFILE_DOWNLINE_DASHBOARD ?></p>
                </div>
            </div>
        </a>
     	<a href="productPackage.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/package.png" class="width100" alt="<?php echo _PROFILE_PRODUCT_PACKAGE ?>" title="<?php echo _PROFILE_PRODUCT_PACKAGE ?>" >
                    <p class="brown-text"><?php echo _PROFILE_PRODUCT_PACKAGE ?></p>
                </div>
            </div>
        </a>
    	<a href="userSalesDashboard.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/sales.png" class="width100" alt="<?php echo _PROFILE_SALES_DASHBOARD ?>" title="<?php echo _PROFILE_SALES_DASHBOARD ?>" >
                    <p class="brown-text"><?php echo _PROFILE_SALES_DASHBOARD ?></p>
                </div>
            </div>
        </a>      
    	<a href="userCommissionReceivedHistory.php">
            <div class="four-div">
                <div class="inner-green-box">
                    <img src="img/commission.png" class="width100" alt="<?php echo _PROFILE_COMMISSION_DASHBOARD ?>" title="<?php echo _PROFILE_COMMISSION_DASHBOARD ?>" >
                    <p class="brown-text"><?php echo _PROFILE_COMMISSION_DASHBOARD ?></p>
                </div>
            </div>
        </a>           
        
                        
    </div>
</div>
</div>
<div class="clear"></div>

</div>

<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>



</body>
</html>