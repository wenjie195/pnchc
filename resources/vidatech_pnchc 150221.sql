-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2021 at 10:44 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_pnchc`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `date_input` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `uid`, `title`, `content`, `date_input`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '7f316b3b2967595d9e21b5f6f2522230', 'Happy CNY', 'Heng Ong Huat', '2021-02-11', 'Available', 1, '2021-02-10 07:36:21', '2021-02-10 08:00:22'),
(2, 'cab77052ab29447800a71a01bd3c6699', 'qwert aaaa', 'aaaaaaa bbbbbbbb', '2021-02-27', 'Available', 1, '2021-02-10 07:59:10', '2021-02-10 07:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `receiver`, `amount`, `bonus_type`, `status`, `date_created`, `date_updated`) VALUES
(1, 'f2b62c643a0368e3c16e3fb2cc86de671613379439', 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', '611613f39ec19c7b251982d41f216c3f', 'oliver', '1500', 'Commission Level 1 (A)', NULL, '2021-02-15 08:57:19', '2021-02-15 08:57:19'),
(2, 'f2b62c643a0368e3c16e3fb2cc86de671613379439', 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', '70691b70ee884b1439e8a73ce33c0eef', 'company', '600', 'Commission Level 2 (A)', NULL, '2021-02-15 08:57:19', '2021-02-15 08:57:19'),
(3, 'f2b62c643a0368e3c16e3fb2cc86de671613379463', 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', '611613f39ec19c7b251982d41f216c3f', 'oliver', '900', 'Commission Level 1 (B)', NULL, '2021-02-15 08:57:43', '2021-02-15 08:57:43'),
(4, 'f2b62c643a0368e3c16e3fb2cc86de671613379463', 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', '70691b70ee884b1439e8a73ce33c0eef', 'company', '600', 'Commission Level 2 (B)', NULL, '2021-02-15 08:57:43', '2021-02-15 08:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `uid`, `username`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_status`, `shipping_status`, `shipping_method`, `date_created`, `date_updated`) VALUES
(1, 'f2b62c643a0368e3c16e3fb2cc86de671613379431', 'f2b62c643a0368e3c16e3fb2cc86de67', NULL, 'thea', '01211223304', 'thea@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '25500', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-02-15 08:57:12', '2021-02-15 08:57:12'),
(2, 'f2b62c643a0368e3c16e3fb2cc86de671613379439', 'f2b62c643a0368e3c16e3fb2cc86de67', NULL, 'thea', '01211223304', 'thea@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '25500', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-02-15 08:57:20', '2021-02-15 08:57:20'),
(3, 'f2b62c643a0368e3c16e3fb2cc86de671613379452', 'f2b62c643a0368e3c16e3fb2cc86de67', NULL, 'thea', '01211223304', 'thea@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '27800', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-02-15 08:57:33', '2021-02-15 08:57:33'),
(4, 'f2b62c643a0368e3c16e3fb2cc86de671613379463', 'f2b62c643a0368e3c16e3fb2cc86de67', NULL, 'thea', '01211223304', 'thea@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '17850', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-02-15 08:57:44', '2021-02-15 08:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', 'f2b62c643a0368e3c16e3fb2cc86de671613379431', '300', '85', '25500', NULL, '25500', 'Sold', '2021-02-15 08:57:11', '2021-02-15 08:57:12'),
(2, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', 'f2b62c643a0368e3c16e3fb2cc86de671613379439', '300', '85', '25500', NULL, '25500', 'Sold', '2021-02-15 08:57:19', '2021-02-15 08:57:20'),
(3, 'f2b62c643a0368e3c16e3fb2cc86de67', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', 'f2b62c643a0368e3c16e3fb2cc86de671613379452', '200', '199', '39800', '12000', '27800', 'Sold', '2021-02-15 08:57:32', '2021-02-15 08:57:33'),
(4, 'f2b62c643a0368e3c16e3fb2cc86de67', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', 'f2b62c643a0368e3c16e3fb2cc86de671613379463', '150', '199', '29850', '12000', '17850', 'Sold', '2021-02-15 08:57:43', '2021-02-15 08:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_holder` varchar(255) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uid`, `user_uid`, `username`, `email`, `phone_no`, `company_name`, `amount`, `credit`, `package`, `bank`, `bank_holder`, `bank_reference`, `receipt`, `duration`, `manufactured`, `expired`, `status`, `date_created`, `date_updated`) VALUES
(1, '64a036ceda093184fe4d428ed587708a', '611613f39ec19c7b251982d41f216c3f', 'oliver', 'oliver@gmail.com', '01211223303', NULL, '200000', '200000', NULL, NULL, NULL, '613123123', '1613379311urock.jpg', NULL, NULL, NULL, 'Approved', '2021-02-15 08:55:11.7', '2021-02-15 08:55:58.4'),
(2, 'f9a15441892f2e8cc39e3a4386be1161', 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', 'thea@gmail.com', '01211223304', NULL, '150000', '150000', NULL, NULL, NULL, '63321321', '1613379339dc motor.png', NULL, NULL, NULL, 'Approved', '2021-02-15 08:55:39.6', '2021-02-15 08:55:59.4'),
(3, '3d16b5bfc1e7a02961383747da93645b', '611613f39ec19c7b251982d41f216c3f', 'oliver', 'oliver@gmail.com', '01211223303', NULL, '500', '500', NULL, NULL, NULL, 'Wallet To Credit', NULL, NULL, NULL, NULL, 'Success', '2021-02-15 09:04:47.0', '2021-02-15 09:05:49.0'),
(4, '4348e94672b49759e8b2c02856d0cb55', '611613f39ec19c7b251982d41f216c3f', 'oliver', 'oliver@gmail.com', '01211223303', NULL, '250', '250', NULL, NULL, NULL, 'Wallet To Credit', NULL, NULL, NULL, NULL, 'Success', '2021-02-15 09:06:01.1', '2021-02-15 09:06:01.1');

-- --------------------------------------------------------

--
-- Table structure for table `preorder_list`
--

CREATE TABLE `preorder_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preorder_list`
--

INSERT INTO `preorder_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '30', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:34:05', '2021-02-09 04:34:06'),
(2, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '50', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:34:19', '2021-02-09 04:34:20'),
(3, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '20', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:34:41', '2021-02-09 04:34:42'),
(4, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '10', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:34:59', '2021-02-09 04:35:00'),
(5, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '100', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:41:56', '2021-02-09 04:41:57'),
(6, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '300', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:42:16', '2021-02-09 04:44:59'),
(7, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '50', NULL, NULL, NULL, '0', 'Sold', '2021-02-09 04:45:11', '2021-02-09 04:45:12'),
(8, '611613f39ec19c7b251982d41f216c3f', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '50', NULL, NULL, NULL, '0', 'Sold', '2021-02-15 05:50:26', '2021-02-15 05:50:28'),
(9, '611613f39ec19c7b251982d41f216c3f', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '55', NULL, NULL, NULL, '0', 'Sold', '2021-02-15 05:51:01', '2021-02-15 05:51:02'),
(10, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '300', NULL, NULL, NULL, '0', 'Sold', '2021-02-15 08:57:09', '2021-02-15 08:57:11'),
(11, 'f2b62c643a0368e3c16e3fb2cc86de67', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '300', NULL, NULL, NULL, '0', 'Sold', '2021-02-15 08:57:18', '2021-02-15 08:57:19'),
(12, 'f2b62c643a0368e3c16e3fb2cc86de67', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', NULL, '200', NULL, NULL, NULL, '0', 'Sold', '2021-02-15 08:57:31', '2021-02-15 08:57:32'),
(13, 'f2b62c643a0368e3c16e3fb2cc86de67', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', NULL, '150', NULL, NULL, NULL, '0', 'Sold', '2021-02-15 08:57:42', '2021-02-15 08:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_two` text DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `category`, `brand`, `name`, `sku`, `slug`, `price`, `status`, `quantity`, `description`, `description_two`, `keyword_one`, `image_one`, `image_two`, `date_created`, `date_updated`) VALUES
(1, '6cac5d1e947baf95271cf106ecb1dbcb', NULL, NULL, 'Product A', NULL, NULL, NULL, 'Available', NULL, 'Product A Description', NULL, NULL, NULL, NULL, '2021-01-15 03:21:17', '2021-02-15 06:33:59'),
(2, '5db36e7812212f57d9b1f21057491a01', NULL, NULL, 'Product B', NULL, NULL, NULL, 'Available', NULL, 'Product B Description', NULL, NULL, NULL, NULL, '2021-01-15 03:32:15', '2021-01-29 09:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `current_status` varchar(255) DEFAULT 'Member',
  `order_status` varchar(255) DEFAULT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `current_status`, `order_status`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, '4a613b65db94817d1539ff823a52da3a', '70691b70ee884b1439e8a73ce33c0eef', 'company', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-03-11 01:50:18', '2021-02-08 05:26:04'),
(2, '', '4a613b65db94817d1539ff823a52da3a', 'company2', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-02-08 05:25:24', '2021-02-08 05:26:06'),
(3, '70691b70ee884b1439e8a73ce33c0eef', '611613f39ec19c7b251982d41f216c3f', 'oliver', 1, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-02-08 05:29:10', '2021-02-08 05:29:10'),
(4, '611613f39ec19c7b251982d41f216c3f', 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-02-08 05:32:37', '2021-02-08 05:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_record`
--

CREATE TABLE `transfer_record` (
  `id` int(11) NOT NULL,
  `transaction_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `stock_type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'PENDING',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `icno` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `bank_acc_name` varchar(255) DEFAULT NULL,
  `bank_acc_type` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `rank` varchar(255) DEFAULT 'Member',
  `wechat_id` varchar(255) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `credit` decimal(20,4) NOT NULL,
  `value_a` varchar(255) DEFAULT NULL,
  `value_b` varchar(255) DEFAULT NULL,
  `wallet` decimal(20,4) NOT NULL,
  `wallet_a` varchar(255) DEFAULT NULL,
  `wallet_b` varchar(255) DEFAULT NULL,
  `rank_a` varchar(255) DEFAULT NULL,
  `rank_b` varchar(255) DEFAULT NULL,
  `maintenance_a` varchar(255) DEFAULT NULL,
  `maintenance_b` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `fullname`, `firstname`, `lastname`, `icno`, `password`, `salt`, `birth_date`, `country`, `phone_no`, `address`, `zipcode`, `state`, `image`, `bank_name`, `bank_acc_number`, `bank_acc_name`, `bank_acc_type`, `login_type`, `user_type`, `rank`, `wechat_id`, `order_status`, `credit`, `value_a`, `value_b`, `wallet`, `wallet_a`, `wallet_b`, `rank_a`, `rank_b`, `maintenance_a`, `maintenance_b`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', 'admin', 'admin@gmail.com', NULL, 'admin', 'admin pnchc', NULL, '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'Admin', NULL, NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-11 01:40:01', '2020-12-30 08:13:11'),
(2, '70691b70ee884b1439e8a73ce33c0eef', 'company', 'company@gmail.com', 'company', 'company', 'company pnchc', '11223301', '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, 'Malaysia', '01211223301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', 'company01', NULL, '0.0000', NULL, NULL, '1200.0000', NULL, NULL, '5', '3', NULL, NULL, '2020-03-11 09:23:46', '2021-02-15 09:00:51'),
(3, '7ea1cc3effbf37e0381104ce39aa06e5', 'mex', 'mex@gmail.com', NULL, 'mex', 'mex pnchc', NULL, '0ba9793c54e6f630d55390cd85f3bef8878c4311f83e886f12ed5f60e1f2e95e', '53088dfb56d409d76d9c908c094d77def297d116', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Banker', NULL, NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-15 09:36:15', '2021-02-15 07:03:58'),
(4, '4a613b65db94817d1539ff823a52da3a', 'company2', 'company2@gmail.com', 'company2', 'company2', 'company2 pnchc', '11223302', 'a75e0982c6605093819ac2955480d46099df75614011f04af522e600f9771255', '114f4c591425e5efd0ad66fc49b2ab8f31a81908', NULL, NULL, '01211223302', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', 'company02', NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 05:25:24', '2021-02-15 08:53:55'),
(5, '611613f39ec19c7b251982d41f216c3f', 'oliver', 'oliver@gmail.com', 'oliver', NULL, NULL, '11223303', 'aa73cbbc5901a7a9f618496f8a84667d32d0118f8f623d62c3030a0bbc6a7c00', '8c5d9f0b0d8c1d02f17141734846729d517315c9', NULL, NULL, '01211223303', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', 'oliver03', NULL, '200750.0000', NULL, NULL, '1150.0000', NULL, NULL, '5', '3', NULL, NULL, '2021-02-08 05:29:10', '2021-02-15 09:39:58'),
(6, 'f2b62c643a0368e3c16e3fb2cc86de67', 'thea', 'thea@gmail.com', 'thea', NULL, NULL, '11223304', '7f87cfc38a11fb74f795695d2886055dbd5a2327381c2463650838f8c6769f4e', 'd10581cfca805e17789e73b22909ecab37c34c16', NULL, NULL, '01211223304', NULL, NULL, NULL, '1612936487Ivy Play666.jpg', NULL, NULL, NULL, NULL, 1, 1, 'Member', 'thea04', NULL, '53350.0000', '600', '350', '0.0000', NULL, NULL, '5', '4', NULL, NULL, '2021-02-08 05:32:37', '2021-02-15 08:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `variation`
--

CREATE TABLE `variation` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `profit` varchar(255) DEFAULT NULL,
  `com_one` varchar(255) DEFAULT NULL,
  `com_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `variation`
--

INSERT INTO `variation` (`id`, `uid`, `product_uid`, `name`, `level`, `quantity`, `price`, `profit`, `com_one`, `com_two`, `status`, `date_created`, `date_updated`) VALUES
(1, '46a642e4e8f46caac4381adfe18059bc', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '1', '0', '148', '0', '0', '0', 'Available', '2021-01-15 03:21:29', '2021-01-15 03:34:08'),
(2, '89ace5349e2fe4b048414df7b49b9d8e', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '2', '10', '120', '0', '0', '0', 'Available', '2021-01-15 03:21:47', '2021-01-15 03:34:10'),
(3, 'b9eddaa875b02faef0511d449165064b', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '3', '50', '105', '0', '4', '0', 'Available', '2021-01-15 03:21:59', '2021-01-15 03:34:11'),
(4, '3930153dbb15869f6b2a909f5050a201', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '4', '100', '95', '0', '4', '0', 'Available', '2021-01-15 03:22:09', '2021-01-15 03:34:13'),
(5, '67563c77e04b6cea9b3e9227b82cd6bd', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '5', '300', '85', '0', '5', '2', 'Available', '2021-01-15 03:22:19', '2021-01-15 03:34:14'),
(6, '44c0b7b111fa2b42de305400870be10a', '5db36e7812212f57d9b1f21057491a01', 'Product B', '1', '49', '199', '30', '0', '0', 'Available', '2021-01-15 03:32:54', '2021-01-15 03:32:54'),
(7, '8cdc0f46feb685e9a46a4092e2b2778d', '5db36e7812212f57d9b1f21057491a01', 'Product B', '2', '99', '199', '40', '6', '4', 'Available', '2021-01-15 03:33:17', '2021-01-15 03:33:17'),
(8, '30ec303c05133723af4bd33a8dbf5ec7', '5db36e7812212f57d9b1f21057491a01', 'Product B', '3', '299', '199', '60', '6', '4', 'Available', '2021-01-15 03:33:40', '2021-01-15 03:33:40'),
(9, '48d019c2a9f0287a051f247fa1e27dc8', '5db36e7812212f57d9b1f21057491a01', 'Product B', '4', '300', '199', '80', '6', '4', 'Available', '2021-01-15 03:33:57', '2021-01-15 03:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_record`
--

CREATE TABLE `wallet_record` (
  `id` int(11) NOT NULL,
  `xfer_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `previous_wallet` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `wallet_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'PENDING',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallet_record`
--

INSERT INTO `wallet_record` (`id`, `xfer_uid`, `uid`, `username`, `previous_wallet`, `amount`, `wallet_type`, `status`, `date_created`, `date_updated`) VALUES
(1, 'd1c9b5623a3e12f422086c1ee8239dc2', '70691b70ee884b1439e8a73ce33c0eef', 'company', '0.0000', '600', 'Wallet A', 'Success', '2021-02-15 09:00:44', '2021-02-15 09:00:44'),
(2, '2f7e7534250a27bcce2644736b32db62', '611613f39ec19c7b251982d41f216c3f', 'oliver', '0.0000', '1500', 'Wallet A', 'Success', '2021-02-15 09:00:44', '2021-02-15 09:00:44'),
(3, '39b020aac243939cf39873ec150d7b7b', '70691b70ee884b1439e8a73ce33c0eef', 'company', '600.0000', '600', 'Wallet B', 'Success', '2021-02-15 09:00:51', '2021-02-15 09:00:51'),
(4, 'ac3f8da481717893850afd918d7a39a5', '611613f39ec19c7b251982d41f216c3f', 'oliver', '1500.0000', '900', 'Wallet B', 'Success', '2021-02-15 09:00:51', '2021-02-15 09:00:51'),
(5, '3d16b5bfc1e7a02961383747da93645b', '611613f39ec19c7b251982d41f216c3f', 'oliver', '200000.0000  /  1500.0000', '500', 'Convert To Credit', 'Success', '2021-02-15 09:04:47', '2021-02-15 09:04:47'),
(6, '4348e94672b49759e8b2c02856d0cb55', '611613f39ec19c7b251982d41f216c3f', 'oliver', '200500.0000  /  1000.0000', '250', 'Convert To Credit', 'Success', '2021-02-15 09:06:01', '2021-02-15 09:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `withdrawal_uid` varchar(255) DEFAULT NULL,
  `current_amount` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`id`, `uid`, `withdrawal_uid`, `current_amount`, `amount`, `final_amount`, `method`, `note`, `bank_name`, `bank_account_holder`, `bank_account_no`, `contact`, `receipt`, `status`, `reason`, `date_created`, `date_updated`) VALUES
(1, '611613f39ec19c7b251982d41f216c3f', '0b582ab229b30beae2eb497900b50d57', '2400.0000', '400', '2000', NULL, NULL, 'CIMB', 'Oliver Queen', '123321123', '01211223303', NULL, 'REJECTED', NULL, '2021-02-15 09:02:09', '2021-02-15 09:39:58'),
(2, '611613f39ec19c7b251982d41f216c3f', '072c6300d2840d8de02258a38934ce17', '2000.0000', '500', '1500', NULL, NULL, 'CIMB', 'Oliver Queen', '123321123', '01211223303', NULL, 'APPROVED', NULL, '2021-02-15 09:03:59', '2021-02-15 09:39:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preorder_list`
--
ALTER TABLE `preorder_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_record`
--
ALTER TABLE `transfer_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `variation`
--
ALTER TABLE `variation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_record`
--
ALTER TABLE `wallet_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `preorder_list`
--
ALTER TABLE `preorder_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transfer_record`
--
ALTER TABLE `transfer_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `variation`
--
ALTER TABLE `variation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wallet_record`
--
ALTER TABLE `wallet_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
