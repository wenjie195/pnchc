<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$products = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/refer.php" />
<link rel="canonical" href="https://agentpnchc.com/refer.php" />
<meta property="og:title" content="User Dashboard | Pure & Cure" />
<title>User Dashboard | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding tur-bg min-height100 padding-top">

    <p class="text-center"><img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="center-logo"></p>
    <h1 class="h1 white-text text-center login-h1"><b>Cart</b></h1>

    <div class="login-div margin-auto">

        <div class="width100 scroll-div border-separation">
            <table class="green-table width100">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Product Name</th>
                        <th>Unit Price (RM)</th>
                        <th>Quantity</th>
                        <th>Subtotal (RM)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($products)
                        {
                            for($cnt = 0;$cnt < count($products) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $products[$cnt]->getProductName();?></td>
                                    <td><?php echo $products[$cnt]->getOriginalPrice();?></td>
                                    <td><?php echo $products[$cnt]->getQuantity();?></td>
                                    <td><?php echo $products[$cnt]->getTotalPrice();?></td>

                                    <td>
                                        <form method="POST" action="utilities/deletePreOrderFunction.php" class="hover1">
                                            <button class="clean hover1 img-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>

            <?php
                if($products)
                {
                ?>
                    <form method="POST" action="utilities/orderListFunction.php" >
                        <button class="green-button white-text clean2 width100" style="margin-top:50px;" type="submit" name="user_uid" value="<?php echo $products[0]->getUserUid();?>">Checkout</button>
                    </form>
                <?php
                }
            ?> 

                <p class="text-center" style="margin-top:20px;"><a style="font-size:18px;" href="productOrder.php" class="green-a">Continue Shopping</a></p>
        
        </div>

    </div>

    <!-- <div class="clear"></div>

    <p class="signup-p text-center"><a href="productOrder.php" class="dark-tur-link signup-a">Ordering</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="refer.php" class="dark-tur-link signup-a">Refer Link</a></p> -->

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="logout.php" class="dark-tur-link signup-a">Logout</a></p>

    <div class="clear"></div>
   
</div>

<?php include 'js.php'; ?>

</body>
</html>