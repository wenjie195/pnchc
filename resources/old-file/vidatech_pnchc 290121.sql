-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2021 at 11:42 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_pnchc`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `uid`, `username`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_status`, `shipping_status`, `shipping_method`, `date_created`, `date_updated`) VALUES
(1, 'b1bb93a84245d3d401cf0d9c1d1948a21611916444', 'b1bb93a84245d3d401cf0d9c1d1948a2', NULL, 'oliver', '01211223301', 'oliver@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '7950', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-01-29 10:36:34', '2021-01-29 10:36:34'),
(2, 'b1bb93a84245d3d401cf0d9c1d1948a21611916630', 'b1bb93a84245d3d401cf0d9c1d1948a2', NULL, 'oliver', '01211223301', 'oliver@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '9500', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-01-29 10:37:17', '2021-01-29 10:37:17'),
(3, 'b1bb93a84245d3d401cf0d9c1d1948a21611916739', 'b1bb93a84245d3d401cf0d9c1d1948a2', NULL, 'oliver', '01211223301', 'oliver@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '29750', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-01-29 10:39:03', '2021-01-29 10:39:03'),
(4, 'b1bb93a84245d3d401cf0d9c1d1948a21611916767', 'b1bb93a84245d3d401cf0d9c1d1948a2', NULL, 'oliver', '01211223301', 'oliver@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '1800', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-01-29 10:39:29', '2021-01-29 10:39:29'),
(5, 'b1bb93a84245d3d401cf0d9c1d1948a21611916874', 'b1bb93a84245d3d401cf0d9c1d1948a2', NULL, 'oliver', '01211223301', 'oliver@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '6380', NULL, 'POINTS', NULL, NULL, 'APPROVED', NULL, NULL, '2021-01-29 10:41:16', '2021-01-29 10:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b1bb93a84245d3d401cf0d9c1d1948a2', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', 'b1bb93a84245d3d401cf0d9c1d1948a21611916444', '50', '199', '9950', '2000', '7950', 'Sold', '2021-01-29 10:34:04', '2021-01-29 10:36:34'),
(2, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', 'b1bb93a84245d3d401cf0d9c1d1948a21611916630', '100', '95', '9500', NULL, '9500', 'Sold', '2021-01-29 10:37:10', '2021-01-29 10:37:17'),
(3, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', 'b1bb93a84245d3d401cf0d9c1d1948a21611916739', '350', '85', '29750', NULL, '29750', 'Sold', '2021-01-29 10:38:59', '2021-01-29 10:39:03'),
(4, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', 'b1bb93a84245d3d401cf0d9c1d1948a21611916767', '15', '120', '1800', NULL, '1800', 'Sold', '2021-01-29 10:39:27', '2021-01-29 10:39:29'),
(5, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', 'b1bb93a84245d3d401cf0d9c1d1948a21611916874', '25', '120', '3000', NULL, '3000', 'Sold', '2021-01-29 10:41:14', '2021-01-29 10:41:16'),
(6, 'b1bb93a84245d3d401cf0d9c1d1948a2', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', 'b1bb93a84245d3d401cf0d9c1d1948a21611916874', '20', '199', '3980', '600', '3380', 'Sold', '2021-01-29 10:41:14', '2021-01-29 10:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_holder` varchar(255) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uid`, `user_uid`, `username`, `email`, `phone_no`, `company_name`, `amount`, `credit`, `package`, `bank`, `bank_holder`, `bank_reference`, `receipt`, `duration`, `manufactured`, `expired`, `status`, `date_created`, `date_updated`) VALUES
(1, '02020e358886c38c90d063204b0cdf05', 'b1bb93a84245d3d401cf0d9c1d1948a2', 'oliver', 'oliver@gmail.com', '01211223301', NULL, '100000', '100000', NULL, NULL, NULL, '7744556982', '1611739139testreceipt.jpg', NULL, NULL, NULL, 'Approved', '2021-01-27 09:18:59.0', '2021-01-29 08:40:54.3');

-- --------------------------------------------------------

--
-- Table structure for table `preorder_list`
--

CREATE TABLE `preorder_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preorder_list`
--

INSERT INTO `preorder_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b1bb93a84245d3d401cf0d9c1d1948a2', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', NULL, '50', NULL, NULL, NULL, '0', 'Sold', '2021-01-29 10:34:02', '2021-01-29 10:34:04'),
(2, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '100', NULL, NULL, NULL, '0', 'Sold', '2021-01-29 10:37:08', '2021-01-29 10:37:10'),
(3, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '15', NULL, NULL, NULL, '0', 'Delete', '2021-01-29 10:38:33', '2021-01-29 10:38:37'),
(4, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '350', NULL, NULL, NULL, '0', 'Sold', '2021-01-29 10:38:57', '2021-01-29 10:38:59'),
(5, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '15', NULL, NULL, NULL, '0', 'Sold', '2021-01-29 10:39:26', '2021-01-29 10:39:27'),
(6, 'b1bb93a84245d3d401cf0d9c1d1948a2', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '6cac5d1e947baf95271cf106ecb1dbcb', NULL, '25', NULL, NULL, NULL, '0', 'Sold', '2021-01-29 10:41:08', '2021-01-29 10:41:14'),
(7, 'b1bb93a84245d3d401cf0d9c1d1948a2', '5db36e7812212f57d9b1f21057491a01', 'Product B', '5db36e7812212f57d9b1f21057491a01', NULL, '20', NULL, NULL, NULL, '0', 'Sold', '2021-01-29 10:41:13', '2021-01-29 10:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_two` text DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `category`, `brand`, `name`, `sku`, `slug`, `price`, `status`, `quantity`, `description`, `description_two`, `keyword_one`, `image_one`, `image_two`, `date_created`, `date_updated`) VALUES
(1, '6cac5d1e947baf95271cf106ecb1dbcb', NULL, NULL, 'Product A', NULL, NULL, NULL, 'Available', NULL, 'Product A Description', NULL, NULL, NULL, NULL, '2021-01-15 03:21:17', '2021-01-15 03:21:17'),
(2, '5db36e7812212f57d9b1f21057491a01', NULL, NULL, 'Product B', NULL, NULL, NULL, 'Available', NULL, 'Product B Description', NULL, NULL, NULL, NULL, '2021-01-15 03:32:15', '2021-01-29 09:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `current_status` varchar(255) DEFAULT 'Member',
  `order_status` varchar(255) DEFAULT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `current_status`, `order_status`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, '', '70691b70ee884b1439e8a73ce33c0eef', 'company', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-03-11 01:50:18', '2020-12-21 07:45:19'),
(2, '70691b70ee884b1439e8a73ce33c0eef', 'b1bb93a84245d3d401cf0d9c1d1948a2', 'oliver', 1, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 09:14:38', '2021-01-07 09:14:38'),
(3, 'b1bb93a84245d3d401cf0d9c1d1948a2', '75deabde55e82dba7357bc532714cbe3', 'thea', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 09:15:38', '2021-01-07 09:15:38');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `icno` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `bank_acc_name` varchar(255) DEFAULT NULL,
  `bank_acc_type` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `rank` varchar(255) DEFAULT 'Member',
  `wechat_id` varchar(255) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `credit` decimal(20,4) NOT NULL,
  `value_a` varchar(255) DEFAULT NULL,
  `value_b` varchar(255) DEFAULT NULL,
  `wallet` decimal(20,4) NOT NULL,
  `wallet_a` varchar(255) DEFAULT NULL,
  `wallet_b` varchar(255) DEFAULT NULL,
  `rank_a` varchar(255) DEFAULT NULL,
  `rank_b` varchar(255) DEFAULT NULL,
  `maintenance_a` varchar(255) DEFAULT NULL,
  `maintenance_b` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `fullname`, `firstname`, `lastname`, `icno`, `password`, `salt`, `birth_date`, `country`, `phone_no`, `address`, `zipcode`, `state`, `image`, `bank_name`, `bank_acc_number`, `bank_acc_name`, `bank_acc_type`, `login_type`, `user_type`, `rank`, `wechat_id`, `order_status`, `credit`, `value_a`, `value_b`, `wallet`, `wallet_a`, `wallet_b`, `rank_a`, `rank_b`, `maintenance_a`, `maintenance_b`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', 'admin', 'admin@gmail.com', NULL, 'admin', 'admin pnchc', NULL, '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'Admin', NULL, NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-11 01:40:01', '2020-12-30 08:13:11'),
(2, '70691b70ee884b1439e8a73ce33c0eef', 'company', 'companyacc@gmail.com', NULL, 'company', 'company pnchc', NULL, '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-11 09:23:46', '2020-12-30 08:13:13'),
(3, '7ea1cc3effbf37e0381104ce39aa06e5', 'mex', 'mex@gmail.com', NULL, 'mex', 'mex pnchc', NULL, '0ba9793c54e6f630d55390cd85f3bef8878c4311f83e886f12ed5f60e1f2e95e', '53088dfb56d409d76d9c908c094d77def297d116', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Banker', NULL, NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-15 09:36:15', '2020-12-30 08:13:15'),
(4, 'b1bb93a84245d3d401cf0d9c1d1948a2', 'oliver', 'oliver@gmail.com', 'oliver', NULL, NULL, '997700-77-8899', '60f33865bd670cf2ee7d13cc96846cfa7ce3d56075e9e992f2088db07ce81bd1', '9ecc9f3b39139527368dd41f75c79930de9ab502', NULL, 'Malaysia', '01211223301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', 'oliver123', NULL, '47620.0000', '490', '70', '0.0000', NULL, NULL, '5', '2', NULL, NULL, '2021-01-07 09:14:38', '2021-01-29 10:41:14'),
(5, '75deabde55e82dba7357bc532714cbe3', 'thea', 'thea@gmail.com', 'thea', NULL, NULL, '1211-2233-02', '85f9610a6a41c890ca4cefda790d5f8d9229d0193af490d1f1a9f39e5850a3e0', 'd362078313bcd53372bcb3940a9a32c4b3288c50', NULL, NULL, '01211223302', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', 'thea223302', NULL, '0.0000', NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-07 09:15:38', '2021-01-07 09:15:38');

-- --------------------------------------------------------

--
-- Table structure for table `variation`
--

CREATE TABLE `variation` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `profit` varchar(255) DEFAULT NULL,
  `com_one` varchar(255) DEFAULT NULL,
  `com_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `variation`
--

INSERT INTO `variation` (`id`, `uid`, `product_uid`, `name`, `level`, `quantity`, `price`, `profit`, `com_one`, `com_two`, `status`, `date_created`, `date_updated`) VALUES
(1, '46a642e4e8f46caac4381adfe18059bc', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '1', '0', '148', '0', '0', '0', 'Available', '2021-01-15 03:21:29', '2021-01-15 03:34:08'),
(2, '89ace5349e2fe4b048414df7b49b9d8e', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '2', '10', '120', '0', '0', '0', 'Available', '2021-01-15 03:21:47', '2021-01-15 03:34:10'),
(3, 'b9eddaa875b02faef0511d449165064b', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '3', '50', '105', '0', '4', '0', 'Available', '2021-01-15 03:21:59', '2021-01-15 03:34:11'),
(4, '3930153dbb15869f6b2a909f5050a201', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '4', '100', '95', '0', '4', '0', 'Available', '2021-01-15 03:22:09', '2021-01-15 03:34:13'),
(5, '67563c77e04b6cea9b3e9227b82cd6bd', '6cac5d1e947baf95271cf106ecb1dbcb', 'Product A', '5', '300', '85', '0', '5', '2', 'Available', '2021-01-15 03:22:19', '2021-01-15 03:34:14'),
(6, '44c0b7b111fa2b42de305400870be10a', '5db36e7812212f57d9b1f21057491a01', 'Product B', '1', '49', '199', '30', '0', '0', 'Available', '2021-01-15 03:32:54', '2021-01-15 03:32:54'),
(7, '8cdc0f46feb685e9a46a4092e2b2778d', '5db36e7812212f57d9b1f21057491a01', 'Product B', '2', '99', '199', '40', '6', '4', 'Available', '2021-01-15 03:33:17', '2021-01-15 03:33:17'),
(8, '30ec303c05133723af4bd33a8dbf5ec7', '5db36e7812212f57d9b1f21057491a01', 'Product B', '3', '299', '199', '60', '6', '4', 'Available', '2021-01-15 03:33:40', '2021-01-15 03:33:40'),
(9, '48d019c2a9f0287a051f247fa1e27dc8', '5db36e7812212f57d9b1f21057491a01', 'Product B', '4', '300', '199', '80', '6', '4', 'Available', '2021-01-15 03:33:57', '2021-01-15 03:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `withdrawal_uid` varchar(255) DEFAULT NULL,
  `current_amount` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preorder_list`
--
ALTER TABLE `preorder_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `variation`
--
ALTER TABLE `variation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `preorder_list`
--
ALTER TABLE `preorder_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `variation`
--
ALTER TABLE `variation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
