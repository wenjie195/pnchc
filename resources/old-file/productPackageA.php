<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productA = getProduct($conn,"WHERE name = 'Product A' AND status = 'Available' ");
$productADetails = $productA[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCreditTotal.php" />
<link rel="canonical" href="https://agentpnchc.com/userCreditTotal.php" />
<meta property="og:title" content="<?php echo _PROFILE_TOTAL_CREDIT ?> | Pure & Cure" />
<title><?php echo _PROFILE_TOTAL_CREDIT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<?php include 'header.php'; ?>
	<h1 class="top-title brown-text">Product A</h1>
</div>
<div id="main-start">
	<div class="width100 inner-bg inner-padding"><div class="brown-border"></div>
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
		    
        <form method="POST" action="utilities/preOrderFunction.php">

            <h3 class="center-div-h3">
                Product Name : <?php echo $productADetails->getName();?>
            </h3>  

            <input type="hidden" class="rec-input clean ow-margin-left0" value="<?php echo $productADetails->getUid();?>" id="product_uid" name="product_uid" readonly>

            <h3 class="center-div-h3">Quantity</h3> 
            <input type="number" class="rec-input clean ow-margin-left0" placeholder="Quantity" id="product_quantity" name="product_quantity" required>

            <div class="clear"></div>

            <button class="pill-button yellow-hover-bg three-btn-row mid-btn-row" name="submit">Add To Cart</button>
        
        </form>

    </div>
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>