<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $myOrders = getOrders($conn);
// $myOrders = getOrders($conn, " WHERE payment_status = 'APPROVED' ");
// $myOrders = getOrders($conn, "WHERE uid =? AND payment_status = 'APPROVED' ",array("uid"),array($uid),"s");

$groupSales = 0; // initital
$groupSalesFormat = number_format(0,4); // initital
$directDownline = 0; // initital
$personalSales = 0; // initital

$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $uid, false);
if ($referrerDetails)
{
  for ($i=0; $i <count($referrerDetails) ; $i++)
  {
    $currentLevel = $referrerDetails[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel)
    {
      $directDownline++;
    }
    $referralId = $referrerDetails[$i]->getReferralId();
    // $downlineDetails = getOrders($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"),array($referralId), "s");
    if ($downlineDetails)
    {
      for ($b=0; $b <count($downlineDetails) ; $b++)
      {
        $personalSales += $downlineDetails[$b]->getSubtotal();
      }
    }
  }
  $groupSales += $personalSales;
  $groupSalesFormat = number_format($groupSales,4);
}

$myDownline = getReferralHistory($conn, "WHERE referrer_id =?",array("referrer_id"),array($uid),"s");

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
if ($getWho)
{
    $groupMember = count($getWho);
}
else
{
    $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/refer.php" />
<link rel="canonical" href="https://agentpnchc.com/refer.php" />
<meta property="og:title" content="User Dashboard | Pure & Cure" />
<title>User Dashboard | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding tur-bg min-height100 padding-top">

    <p class="text-center"><img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="center-logo"></p>
    <h1 class="h1 white-text text-center login-h1"><b>User Dashboard</b></h1>

    <div class="login-div margin-auto">
        <div class="fake-input-div">
            <?php
            if($myDownline)
            {   
            $totalDirectDownline = count($myDownline);
            }
            else
            {   $totalDirectDownline = 0;   }
            ?>
            <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Total Direct Downline : <?php echo $totalDirectDownline;?>" readonly>
        </div>

        <div class="clear"></div>

        <!-- <div class="fake-input-div before-forgot"> -->
        <div class="fake-input-div">
            <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Group Member : <?php echo $groupMember;?>" readonly>
        </div>
    </div>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="userCreditPurchase.php" class="dark-tur-link signup-a">Purchase Credit</a></p>

    <div class="clear"></div>
    
    <p class="signup-p text-center"><a href="userCreditPurchaseHistory.php" class="dark-tur-link signup-a">Purchase Credit History</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="productOrder.php" class="dark-tur-link signup-a">Ordering</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="refer.php" class="dark-tur-link signup-a">Refer Link</a></p>

    <div class="clear"></div>

    <p class="signup-p text-center"><a href="logout.php" class="dark-tur-link signup-a">Logout</a></p>

    <div class="clear"></div>
   
</div>

<?php include 'js.php'; ?>

</body>
</html>