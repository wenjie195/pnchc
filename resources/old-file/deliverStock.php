<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
// $orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productA = getUser($conn, "WHERE uid = ? AND value_a != '' ",array("uid"),array($uid),"s");
$productB = getUser($conn, "WHERE uid = ? AND value_b != '' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/deliverStock.php" />
<link rel="canonical" href="https://agentpnchc.com/deliverStock.php" />
<meta property="og:title" content="<?php echo _PROFILE_DELIVER_STOCK ?> | Pure & Cure" />
<title><?php echo _PROFILE_DELIVER_STOCK ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<?php include 'header.php'; ?>
	<h1 class="top-title brown-text"><?php echo _PROFILE_DELIVER_STOCK ?></h1>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding tur-bg padding-top48 overflow extra-padding-tb text-center">
        <div class="right-profile-div ow-float-left2 ow-float-none">
        	<div class="profile-img-div">
        		<img src="img/account.png" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
            </div>
        </div>
    	<div class="left-profile-detail left-profile-detail2 overflow-auto ow-float-right2 ow-float-none">
            <h3 class="ranking-h3"><?php echo _PROFILE_RANKING ?>: <?php echo $userData->getRank();?></h3>
            <p class="white-text info-p2"><?php echo _PROFILE_USER_ID ?>: <?php echo $userData->getId();?></p>
            <p class="white-text info-p2"><?php echo _SUCCESS_REFER_LINK ?>: 
            
            <?php
                $actual_link = $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $fullPath = dirname($path);
            ?>

            <input type="hidden" id="linkCopy" value="<?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?>">
            <a id="invest-now-referral-link" style="word-break:break-all;" href="<?php echo 'https://'.$fullPath.'/signupWithReferCode.php?referrerUID='.$_SESSION['uid'] ?>" class="white-text info-p2" target="_blank"><?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?></a></h3>
            <button class="invite-btn  clean green-button" id="copy-referral-link">Copy</button>
            
            </p>
        </div>        
    </div>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow ">
    	<div class="center-div2">
    		    <img src="img/stock2.png" alt="<?php echo _STOCK_CURRENT ?>"   title="<?php echo _STOCK_CURRENT ?>" class="center-icon">
            <div class="clear"></div>
            <h3 class="center-div-h3"><?php echo _PROFILE_DELIVER_STOCK ?></h3> 
            <p class="dark-tur-text note-p3 ow-font-weight400 stock-text-top"><?php echo _STOCK_CURRENT_FOR ?> Product A</p> 
            <h3 class="center-div-h3 stock-text-bottom">300  <?php echo _STOCK_PCS ?></h3>
            
<!--                <?php
                    if($productA)
                    {
                        for($cnt = 0;$cnt < count($productA) ;$cnt++)
                        {
                            $productAValue = $productA[$cnt]->getValueA();
                        }
                    }
                    else
                    {
                        $productBValue = 0 ;
                    }
                ?>    
        </div>

-->  
	<form>
		<input type="text" class="rec-input clean ow-margin-left0 margin-top30" placeholder="<?php echo _STOCK_AMOUNT ?>"  id="amount" name="amount" required>
        <div class="clear"></div>
		<input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _STOCK_RECEIVER_NAME ?>"  id="amount" name="amount" required>
        <div class="clear"></div>        
		<input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _PROFILE_ADDRESS ?>"  id="amount" name="amount" required>
        <div class="clear"></div>        
		<input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _STOCK_CONTACT ?>"  id="amount" name="amount" required>
        <div class="clear"></div>         
        
        <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _PROFILE_CONFIRM ?></button>
        </form>
              

    </div>

</div>
</div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>