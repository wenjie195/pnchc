<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$creditDetails = getPayment($conn, "WHERE user_uid = 'asd' ");
// $creditDetails = getPayment($conn, "WHERE user_uid = ? ",array("user_uid"),array($uid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCreditHistory.php" />
<link rel="canonical" href="https://agentpnchc.com/userCreditHistory.php" />
<meta property="og:title" content="<?php echo _PROFILE_CREDIT_HISTORY ?> | Pure & Cure" />
<title><?php echo _PROFILE_CREDIT_HISTORY ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_CREDIT_HISTORY ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow overflow-x">
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <!--<th><?php echo _TOPUP_AMOUNT ?>Amount (USD)</th>-->
                    <th><?php echo _TOPUP_CHANGES ?></th>
                    <th><?php echo _TOPUP_REFERENCE ?></th>
                    <th><?php echo _TOPUP_BALANCE ?></th>
                    <!--<th>Status</th>-->
                    <th><?php echo _TOPUP_DATE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($creditDetails)
                    {
                        for($cnt = 0;$cnt < count($creditDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>+-100</td>
                                <td>Withdraw/Top Up/Purchase Stock</td>
                                <td>100</td>
                                <!--<td><?php echo $creditDetails[$cnt]->getAmount();?></td>-->
                                <!--<td><?php echo $creditDetails[$cnt]->getCredit();?></td>-->
                                <!--<td><?php echo $creditDetails[$cnt]->getBankReference();?></td>-->
                                <!--<td><?php echo $creditDetails[$cnt]->getStatus();?></td>-->
                                <td><?php echo $creditDetails[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
        </div>
    </div>
</div>


</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>