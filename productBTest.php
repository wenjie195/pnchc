<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/productBTest.php" />
<link rel="canonical" href="https://agentpnchc.com/productBTest.php" />
<meta property="og:title" content="Product B Test Commission | Pure & Cure" />
<title>Product B Test Commission | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding"><h1 class="top-title brown-text">Product B Test Commission</h1><div class="brown-border"></div>
    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
        <form action="utilities/productBOrderFunction.php" method="POST" enctype="multipart/form-data">
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="Amount In Hand"  id="fix_amount" name="fix_amount" required>
            <div class="clear"></div>
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="Amount To Order"  id="order_amount" name="order_amount" required>
            <div class="clear"></div>
            <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _INDEX_SUBMIT ?></button>
        </form>      
    </div>
</div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>



</body>
</html>