<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$PersonalSales = 0;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userDownline.php" />
<link rel="canonical" href="https://agentpnchc.com/userDownline.php" />
<meta property="og:title" content="<?php echo _PROFILE_DOWNLINE_DASHBOARD ?> | Pure & Cure" />
<title><?php echo _PROFILE_DOWNLINE_DASHBOARD ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_DOWNLINE_DASHBOARD ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
    
    <?php $level = $userLevel->getCurrentLevel();?>

    <div class="width100 same-padding details-min-height padding-top2 overflow overflow-x">
    	<div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
					
                    <th><?php echo _DOWNLINE_GENERATION ?></th>
                    <th><?php echo _INDEX_USERNAME ?></th>
                    <th><?php echo _DOWNLINE_SPONSOR ?></th>
                    <th>Colloid Plus</th>
                    <th>Eye Love Oil</th>
                    <th><?php echo _DOWNLINE_DIRECT_DOWNLINE ?></th>
                    <th><?php echo _DOWNLINE_TEAM_MEMBER ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($getWho)
                {
                    for($cnt = 0;$cnt < count($getWho) ;$cnt++)
                    {
                        $totalDownline = 0;
                        $downline = 0;
                        $directDownline = 0;
                        $downlineCurrentLvl = 0;
                        $totalDirectDownline = 0;
                        $downline = $getWho[$cnt]->getCurrentLevel();
                        $directDownline = $downline + 1;

                        $downlineUid = $getWho[$cnt]->getReferralId();
                        $getWhoII = getWholeDownlineTree($conn,$downlineUid,false);
                        if ($getWhoII)
                        {
                            for ($i=0; $i <count($getWhoII) ; $i++)
                            {
                                $allDownlineUid = $getWhoII[$i]->getReferralId();
                                $downlineCurrentLvl = $getWhoII[$i]->getCurrentLevel();
                                if ($directDownline == $downlineCurrentLvl)
                                {
                                    $totalDirectDownline++;
                                }
                            }
                        }
                        ?>
                        <tr>
                            <td>
                                <?php
                                    $downlineLvl = $getWho[$cnt]->getCurrentLevel();
                                    echo $lvl = $downlineLvl - $level;
                                ?>
                            </td>

                            <td>
                                <?php
                                    $userUid = $getWho[$cnt]->getReferralId();
                                    $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                                    echo $username = $thisUserDetails[0]->getUsername();
                                ?>
                            </td>

                            <td>
                                <?php
                                $uplineUid = $getWho[$cnt]->getReferrerId();
                                $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                                echo $uplineUsername = $uplineDetails[0]->getUsername();
                                ?>
                            </td>

                            <td>
                                <!-- <?php //echo $username = $thisUserDetails[0]->getRankA();?> -->
                                <?php 
                                    $rankA = $thisUserDetails[0]->getRankA();
                                    if($rankA == '1')
                                    {
                                        $renameRankA = 'Member';
                                    }
                                    elseif($rankA == '2')
                                    {
                                        $renameRankA = 'Angel Agent';
                                    }
                                    elseif($rankA == '3')
                                    {
                                        $renameRankA = 'Silver Dealer';
                                    }
                                    elseif($rankA == '4')
                                    {
                                        $renameRankA = 'Ace Dealer';
                                    }
                                    elseif($rankA == '5')
                                    {
                                        $renameRankA = 'CO-Founder';
                                    }
                                    else
                                    {
                                        $renameRankA = $rankA;
                                    }
                                    echo $renameRankA;
                                ?>
                            </td>

                            <td>
                                <?php echo $username = $thisUserDetails[0]->getRankB();?>
                            </td>

                            <td><?php echo $totalDirectDownline;?></td>

                            <td>
                                <?php
                                $getDownlineAmount = getWholeDownlineTree($conn, $userUid,false);
                                if ($getDownlineAmount)
                                {
                                    echo $groupMember = count($getDownlineAmount);
                                }
                                else
                                {
                                    echo $groupMember = 0;
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
        </div>
    </div>
</div>


</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>