<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://agentpnchc.com/img/fb-meta-04.jpg" />
<meta name="author" content="Pure & Cure">
<meta property="og:description" content="Pure & Cure" />
<meta name="description" content="Pure & Cure" />
<meta name="keywords" content="PNCHC, Pure & Cure, Pure and Cure, Health Care, eye care, beauty care, etc">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JKZ4MJZR2R"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-JKZ4MJZR2R');
</script>