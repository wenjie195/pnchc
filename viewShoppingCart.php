<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$products = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>



<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/viewShoppingCart.php" />
<link rel="canonical" href="https://agentpnchc.com/viewShoppingCart.php" />
<meta property="og:title" content="<?php echo _STOCK_CART ?> | Pure & Cure" />
<title><?php echo _STOCK_CART ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _STOCK_CART ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <?php include 'userTitle.php'; ?>
    
    <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _STOCK_PRODUCT ?></th>
                    <th class="white-space"><?php echo _STOCK_QUANTITY ?> &nbsp; &nbsp; &nbsp; &nbsp;</th>
                    <th><?php echo _STOCK_DELETE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($products)
                    {
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>
                                    <!-- <?php //echo $products[$cnt]->getProductName();?> -->
                                    <?php 
                                        $productName = $products[$cnt]->getProductName();
                                        if($productName == 'Product A')
                                        {
                                            $renameProductName = 'Colloid Plus';
                                        }
                                        elseif($productName == 'Product B')
                                        {
                                            $renameProductName = 'Eye Love Oil';
                                        }
                                        else
                                        {
                                            $renameProductName = $productName;
                                        }
                                        echo $renameProductName;
                                    ?>
                                </td>

                                <td class="white-space">
                                    <form method="POST" action="utilities/preOrderSubtractFunction.php" class="oz-form">
                                        <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getOriginalPrice();?>" id="product_price" name="product_price" readonly>
                                        <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getQuantity();?>" id="product_quantity" name="product_quantity" readonly>
                                        <button class="clean left-minus oz-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                            -
                                        </button>
                                    </form>

                                    <p class="btn-p"><?php echo $products[$cnt]->getQuantity();?></p>

                                    <form method="POST" action="utilities/preOrderAddFunction.php"  class="oz-form">
                                        <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getOriginalPrice();?>" id="product_price" name="product_price" readonly>
                                        <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getQuantity();?>" id="product_quantity" name="product_quantity" readonly>
                                        <button class="clean right-add oz-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                            +
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form method="POST" action="utilities/deletePreOrderFunction.php" class="hover1">
                                        <button class="clean transparent-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                            <img src="img/delete.png" class="icon-size opacity-hover" alt="<?php echo _STOCK_DELETE ?>" title="<?php echo _STOCK_DELETE ?>">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
		</div>
        <div class="center-div2">
            <?php
                if($products)
                {
                ?>
                    <form method="POST" action="utilities/orderListFunction.php" >
                        <button class="clean yellow-btn edit-profile-width ow-margin-left0" style="margin-top:50px;" type="submit" name="user_uid" value="<?php echo $products[0]->getUserUid();?>"><?php echo _STOCK_CHECKOUT ?></button>
                    </form>
                <?php
                }
            ?> 
        </div>

    </div>

</div>

</div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>