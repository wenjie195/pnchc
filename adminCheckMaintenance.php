<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getUser($conn, " WHERE user_type = 1 ");
$allProduct = getProduct($conn, " WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminCheckMaintenance.php" />
<link rel="canonical" href="https://agentpnchc.com/adminCheckMaintenance.php" />
<meta property="og:title" content="<?php echo _ADMIN_MAINTENANCE ?> | Pure & Cure" />
<title><?php echo _ADMIN_MAINTENANCE ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ADMIN_MAINTENANCE ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
   
	<div class="same-padding width100 min-height100 padding-top overflow">
       <div class="width103 overflow">

            <a href="productADowngrade.php">
                <div class="four-div">
                    <div class="inner-green-box">
                        <img src="img/package.png" class="width100" alt="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" title="<?php echo _PROFILE_PURCHASE_CREDIT_REQUEST ?>" >
                        <p class="brown-text text-center">Product A</p>
                    </div>
                </div>
            </a>
            
            <a href="productBDowngrade.php">
                <div class="four-div">
                    <div class="inner-green-box">
                        <img src="img/package.png" class="width100" alt="<?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" title="<?php echo _PROFILE_WITHDRAW_CREDIT_REQUEST ?>" >
                        <p class="brown-text text-center">Product B</p>
                    </div>
                </div>
            </a>
        </div>
    </div> 
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>