<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productVariation = getProduct($conn,"WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/productPackage.php" />
<link rel="canonical" href="https://agentpnchc.com/productPackage.php" />
<meta property="og:title" content="<?php echo _PROFILE_ADD_STOCK ?> | Pure & Cure" />
<title><?php echo _PROFILE_ADD_STOCK ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_ADD_STOCK ?></h1><?php include 'header.php'; ?>
	
</div>
<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
        <?php
        if($productVariation)
        {
            for($cnt = 0;$cnt < count($productVariation) ;$cnt++)
            {
            ?>
            
                <a href='productPackageDetails.php?id=<?php echo $productVariation[$cnt]->getUid();?>' class="opacity-hover">
                    <div class="four-div">
                        <div class="inner-green-box">
                        <div class="square">
                        	<div class="product-img-box">
                            <img src="productImage/<?php echo $productVariation[$cnt]->getImageOne();?>" class="width100" alt="<?php echo $productVariation[$cnt]->getName();?>" title="<?php echo $productVariation[$cnt]->getName();?>" >
                            </div>
                         </div>
                            <?php 
                                $productName = $productVariation[$cnt]->getName();
                                if($productName == 'Product A')
                                {
                                    $renameProductName = 'Colloid Plus';
                                }
                                elseif($productName == 'Product B')
                                {
                                    $renameProductName = 'Eye Love Oil';
                                }
                                else
                                {
                                    $renameProductName = $productName;
                                }
                            ?>

                            <p class="brown-text"><?php echo _PROFILE_ADD ?> <?php echo $renameProductName;?></p>
                        </div>
                    </div>
                </a>

            <?php
            }
            ?>
        <?php
        }
        ?>

        <!-- </div> -->
    </div>
</div>
</div>

<div class="clear"></div>

<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>