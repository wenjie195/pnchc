<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
// $orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productA = getUser($conn, "WHERE uid = ? AND value_a != '' ",array("uid"),array($uid),"s");
$productB = getUser($conn, "WHERE uid = ? AND value_b != '' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userProduct.php" />
<link rel="canonical" href="https://agentpnchc.com/userProduct.php" />
<meta property="og:title" content="<?php echo _STOCK_ALL_PRODUCTS ?> | Pure & Cure" />
<title><?php echo _STOCK_ALL_PRODUCTS ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _STOCK_ALL_PRODUCTS ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow ">
    	<div class="center-div2">
    		    <img src="img/package.png" alt="<?php echo _STOCK_ALL_PRODUCTS ?>"   title="<?php echo _STOCK_ALL_PRODUCTS ?>" class="center-icon">
            <div class="clear"></div>
            <h3 class="center-div-h3"><?php echo _STOCK_ALL_PRODUCTS ?></h3>  
        </div>
            <div class="white-card-div">
                <p class="dark-tur-text card-left-p ow-left-card-p">Colloid Plus</p>

                <?php
                    if($productA)
                    {
                        for($cnt = 0;$cnt < count($productA) ;$cnt++)
                        {
                            $productAValue = $productA[$cnt]->getValueA();
                        }
                    }
                    else
                    {
                        $productAValue = 0 ;
                    }
                ?>    

               		<a href="userProductDetailsA.php">
                    <div class="dark-tur-link transparent-button ow-card-right-p margin-top17">
                	<?php echo _STOCK_DETAILS ?></div></a>
                
                
        	</div> 
            <div class="clear"></div>   
            <!--<div class="white-card-div">
                <p class="dark-tur-text card-left-p ow-left-card-p">Eye Love Oil</p>

                <?php
                    if($productB)
                    {
                        for($cnt = 0;$cnt < count($productB) ;$cnt++)
                        {
                            $productBValue = $productB[$cnt]->getValueB();
                        }
                    }
                    else
                    {
                        $productBValue = 0 ;
                    }
                ?>  
			
               		<a href="userProductDetailsB.php">
                    <div class="dark-tur-link transparent-button ow-card-right-p margin-top17">
                	<?php echo _STOCK_DETAILS ?></div></a>
        	</div> 
             <div class="clear"></div> -->
            <div class="ow-green-card-css white-card-div">
                <p class="white-text card-left-p ow-left-card-p"><?php echo _PROFILE_ALL_ORDER ?></p>

                <?php
                    if($productA)
                    {
                        for($cnt = 0;$cnt < count($productA) ;$cnt++)
                        {
                            $productAValue = $productA[$cnt]->getValueA();
                        }
                    }
                    else
                    {
                        $productAValue = 0 ;
                    }
                ?>    

               		
                	<p class="card-left-p ow-card-right-p margin-top17">
                    	<a href="userProductOrderHistory.php" class="dark-tur-link"><img src="img/edit.png" class="icon-size opacity-hover" alt="<?php echo _PROFILE_ALL_ORDER ?>" title="<?php echo _PROFILE_ALL_ORDER ?>"></a>
                    </p>
                
                
        	</div> 
            <div class="clear"></div>                
             
             
             
                
    </div>

</div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Insufficent Credit <br> Please Reload !!"; 
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Insufficent Credit for Transfer !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>