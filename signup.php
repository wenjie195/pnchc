<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/signup.php" />
<link rel="canonical" href="https://agentpnchc.com/signup.php" />
<meta property="og:title" content="<?php echo _INDEX_SIGN_UP ?> | Pure & Cure" />
<title><?php echo _INDEX_SIGN_UP ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding  min-height100 padding-top login-bg padding-top-bottom">
	<p class="text-center"><img src="img/account.png"  class="center-logo"></p>
    <h1 class="h1 brown-text text-center login-h1 ow-margin-bottom10"><?php echo _SIGN_UP_CREATE ?> <b><?php echo _SIGN_UP_ACCOUNT ?></b></h1> <div class="brown-border margin-auto"></div>
    
    <div class="login-div margin-auto">
        <form action="utilities/registerWithoutRCFunction.php" method="POST">
           
            
            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean brown-text" placeholder="<?php echo _SIGN_UP_YOUR_NAME ?>" id="register_fullname" name="register_fullname" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="email" class="input-css input-css2 clean brown-text" placeholder="<?php echo _SIGN_UP_YOUR_EMAIL ?>" id="register_email" name="register_email" required>
            </div>

            <div class="clear"></div>    

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean brown-text" placeholder="<?php echo _SIGN_UP_YOUR_PHONE_NO ?>" id="register_phone" name="register_phone" required>
            </div>

            <div class="clear"></div>   

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean brown-text" placeholder="<?php echo _SIGN_UP_IC_NO ?>" id="register_icno" name="register_icno" required>
            </div>

            <div class="clear"></div>  

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean brown-text" placeholder="<?php echo _SIGN_UP_WECHAT_ID ?>" id="register_wechatid" name="register_wechatid" required>
            </div>

            <div class="clear"></div>      

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean brown-text" placeholder="<?php echo _INDEX_USERNAME ?>" id="register_username" name="register_username" required>
            </div>

            <div class="clear"></div>       

            <div class="fake-input-div">
                <input type="password" class="input-css clean icon-input password-input2 brown-text" id="register_password" name="register_password" placeholder="<?php echo _INDEX_PASSWORD ?>" required>
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionA()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="password" class="input-css clean icon-input password-input2 brown-text" id="register_retype_password" name="register_retype_password" placeholder="<?php echo _SIGN_UP_RETYPE_PASSWORD ?>" required>
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionB()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div>

            <div class="clear"></div>       

            <button class="clean yellow-button ow-red-bg white-text margin-top30" name="submit">
                <?php echo _SIGN_UP_CREATE ?>
            </button>

            <div class="clear"></div>

            <p class="signup-p signup-p2 text-center black-text"><?php echo _SIGN_UP_ALREADY_MEMBER ?> <a href="index.php" class="light-green-link signup-a"><?php echo _SIGN_UP_LOGIN_NOW ?></a></p>

        </form>
    </div>
</div>

<style>
::-webkit-input-placeholder { /* Edge */
  color: white;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: white;
}

::placeholder {
  color: white;
}

</style>

<?php include 'js.php'; ?>
</body>
</html>