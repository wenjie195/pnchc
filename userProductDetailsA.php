<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productVariation = getProduct($conn,"WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>



<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userProductDetailsA.php" />
<link rel="canonical" href="https://agentpnchc.com/userProductDetailsA.php" />
<meta property="og:title" content="<?php echo _STOCK_PRODUCT_DETAILS ?> | Pure & Cure" />
<title><?php echo _STOCK_PRODUCT_DETAILS ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _STOCK_PRODUCT_DETAILS ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow ">
    	<div class="center-div2">
       
       
    		    <img src="img/colloid.png" alt="PRP Colloid Plus"   title="PRP Colloid Plus" class="product-image2">
        
            <div class="clear"></div>
            <h3 class="center-div-h3">PRP Colloid Plus</h3>  
            <p class="product-details-p dark-tur-text"><?php echo _STOCK_CO1 ?></p>
            <ul class="product-ul dark-tur-text">
            	<li><?php echo _STOCK_COLI1 ?></li>
                <li><?php echo _STOCK_COLI2 ?></li>
                <li><?php echo _STOCK_COLI3 ?></li>
                <li><?php echo _STOCK_COLI4 ?></li>
                <li><?php echo _STOCK_COLI5 ?></li>
            </ul>
       
   
            
                
                <!-- <a href="productPackageDetails.php"> -->
                <a href='productPackageDetails.php?id=6cac5d1e947baf95271cf106ecb1dbcb'>
                    <div class="pill-button yellow-hover-bg middle-center-button margin-top30">
						<?php echo _PROFILE_ADD_STOCK ?>
                    </div>
                </a>


            <div class="clear"></div>                
             
             
             
                
    </div>

</div></div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>