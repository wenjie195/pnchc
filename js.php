<div id="upload-modal" class="modal-css">
	<div class="modal-content-css upload-modal-content">
    <form action="utilities/updateProfilePicFunction.php" method="POST" enctype="multipart/form-data">
        <span class="close-css close-upload">&times;</span>
        <div class="clear"></div>
        <h1 class="upload-h1"><?php echo _PROFILE_UPLOAD_PROFILE_PIC ?></h1>
        <div class="clear"></div>
        <input type="file" class="clean file-css" name="image_one" id="image_one" accept="image/*" required>
        <div class="clear"></div>
        <button class="clean yellow-btn small-btn"><?php echo _PROFILE_CONFIRM ?></button>
    </form>
	</div>
</div>
<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/esm/popper.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.js'></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<script src="js/headroom.js"></script>


<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
 <script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script>
$(".sidebar-dropdown > p").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#main-start").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
function reLoad() {
  location.reload();
}
</script>
<script>

var uploadmodal = document.getElementById("upload-modal");
var openupload = document.getElementsByClassName("open-upload")[0];
var closeupload = document.getElementsByClassName("close-upload")[0];

if(openupload){
openupload.onclick = function() {
  uploadmodal.style.display = "block";
}
}

if(closeupload){
  closeupload.onclick = function() {
  uploadmodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == uploadmodal) {
    uploadmodal.style.display = "none";
  }
}
</script>

<!-- JS Notice Modal Start-->
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="notice-h1 dark-tur-text text-center" id="noticeTitle">Title Here</h1>
	<div class="notice-message text-center light-tur-text" id="noticeMessage">Message Here</div>
  </div>

</div>
<!-- JS Notice Modal Start End-->

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>
    <script src="js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>