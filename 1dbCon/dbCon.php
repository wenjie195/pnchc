<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_pnchc");//for localhost
    // $conn = new mysqli("localhost", "agentpnc_user", "vv7mCB8wC4Sf","agentpnc_live"); // server
    // $conn = new mysqli("localhost", "agentpnc_user", "b82EFBSd1vIF","agentpnc_live"); // server
    // $conn = new mysqli("localhost", "agentpnc_tester", "tr6mz9HfwS0P","agentpnc_test"); // test server
    // $conn = new mysqli("localhost", "qlianmen_pnchctt", "rUR3leV4qTgz","qlianmen_pnchc"); // qlianmeng test server
    // $conn = new mysqli("localhost", "vincapsc_pnchctt", "mvNP1HSSnUGI","vincapsc_pnchc"); // vincaps test server
    // $conn = new mysqli("localhost", "hygenieg_pnchctt", "EWyFxq9lXvw6","hygenieg_pnchc"); // hydrogen test server
    // $conn = new mysqli("localhost", "qlianmen_apnchc", "jVx4EKg3paYU","qlianmen_realpnchc"); // qlianmeng server
    // $conn = new mysqli("localhost", "agentpnc_users", "lZTRtxjAOlAa","agentpnc_live"); // pnchc 2nd server

    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
