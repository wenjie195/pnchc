<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" />
<meta property="og:title" content="Pure & Cure" />
<title>Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding  min-height100 padding-top login-bg padding-top-bottom">

	<p class="text-center"><img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="center-logo"></p>
    <h1 class="h1 red-text text-center login-h1"><?php echo _INDEX_WELCOME ?><br><b>Beauty Care</b></h1>

    <div class="login-div margin-auto">
        <form action="utilities/loginFunction.php" method="POST">
            <div class="fake-input-div">
                <img src="img/user.png" class="input-icon">
                <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="<?php echo _INDEX_USERNAME ?>" id="username" name="username" required>
            </div>
            <div class="clear"></div>
            <div class="fake-input-div before-forgot">
                <img src="img/password.png" class="input-icon">
                <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="<?php echo _INDEX_PASSWORD ?>" id="password" name="password" required>
                <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionC()" alt="<?php echo _INDEX_VIEW_PASSWORD ?>" title="<?php echo _INDEX_VIEW_PASSWORD ?>">
            </div>
            <div class="clear"></div>
            <p class="forgot-p"><a href="forgot.php" class="dark-tur-link forgot-a"><?php echo _INDEX_FORGOT ?></a></p>
            <div class="clear"></div>
            <button class="clean white-button ow-red-bg white-text" name="loginButton">
                <?php echo _INDEX_SIGN_IN ?>
            </button>
            <div class="clear"></div>
            <p class="signup-p text-center"><?php echo _INDEX_NOT_MEMBER ?> <a href="signup.php" class="dark-tur-link signup-a"><?php echo _INDEX_SIGN_UP_NOW ?></a></p>
        </form>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>