<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Orders.php';
// require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/profile.php" />
<link rel="canonical" href="https://agentpnchc.com/profile.php" />
<meta property="og:title" content="<?php echo _PROFILE ?> | Pure & Cure" />
<title><?php echo _PROFILE ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">




    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding profile-min-height padding-top2 overflow">
        <div class="white-card-div">
            <p class="dark-tur-text card-left-p"><?php echo _INDEX_USERNAME ?>: <?php echo $userData->getUsername();?></p>
            <a href="editProfile.php"><div class="line-button"><img src="img/edit.png" class="edit-icon" alt="<?php echo _PROFILE_EDIT_PROFILE ?>" title="<?php echo _PROFILE_EDIT_PROFILE ?>"><?php echo _PROFILE_EDIT ?></div></a>
        </div>
        <div class="white-card-div">
            <p class="dark-tur-text card-left-p"><?php echo _PROFILE_NATIONALITY ?>: <?php echo $userData->getCountry();?></p>
            <a href="editProfile.php"><div class="line-button"><img src="img/edit.png" class="edit-icon" alt="<?php echo _PROFILE_EDIT_PROFILE ?>" title="<?php echo _PROFILE_EDIT_PROFILE ?>"><?php echo _PROFILE_EDIT ?></div></a>
        </div>            
        <div class="white-card-div">
            <p class="dark-tur-text card-left-p"><?php echo _SIGN_UP_WECHAT_ID ?>: <?php echo $userData->getWeChatId();?></p>
            <a href="editProfile.php"><div class="line-button"><img src="img/edit.png" class="edit-icon" alt="<?php echo _PROFILE_EDIT_PROFILE ?>" title="<?php echo _PROFILE_EDIT_PROFILE ?>"><?php echo _PROFILE_EDIT ?></div></a>
        </div>               
        <div class="white-card-div">
            <p class="dark-tur-text card-left-p"><?php echo _SIGN_UP_PHONE_NO ?>: <?php echo $userData->getPhoneNo();?></p>
            <a href="editProfile.php"><div class="line-button"><img src="img/edit.png" class="edit-icon" alt="<?php echo _PROFILE_EDIT_PROFILE ?>" title="<?php echo _PROFILE_EDIT_PROFILE ?>"><?php echo _PROFILE_EDIT ?></div></a>
        </div>       
    </div>

</div>

<div class="clear"></div>


</div>

<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>



</body>
</html>