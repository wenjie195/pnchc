<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
// $orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productA = getUser($conn, "WHERE uid = ? AND value_a != '' ",array("uid"),array($uid),"s");
$productB = getUser($conn, "WHERE uid = ? AND value_b != '' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userStockDeliver.php" />
<link rel="canonical" href="https://agentpnchc.com/userStockDeliver.php" />
<meta property="og:title" content="<?php echo _PROFILE_DELIVER_STOCK ?> | Pure & Cure" />
<title><?php echo _PROFILE_DELIVER_STOCK ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_DELIVER_STOCK ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
    
    <?php
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();
        $productName = rewrite($_POST['product_name']);
        // echo "<br>";
        if($productName == 'A')
        {
            $renameProductName = 'Colloid Plus';

            $checkProductValue = $userDetails[0]->getValueA();
            if($checkProductValue != '')
            {
                $productValue = $checkProductValue;
            }
            else
            {
                $productValue = 0;
            }
        }
        elseif($productName == 'B')
        {
            $renameProductName = 'Eye Love Oil';

            $checkProductValue = $userDetails[0]->getValueB();
            if($checkProductValue != '')
            {
                $productValue = $checkProductValue;
            }
            else
            {
                $productValue = 0;
            }
        }
        else
        {}

    }
    ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow ">
        <div class="center-div2">
        <img src="img/stock2.png" alt="<?php echo _STOCK_CURRENT ?>"   title="<?php echo _STOCK_CURRENT ?>" class="center-icon">
        <div class="clear"></div>
        <h3 class="center-div-h3"><?php echo _PROFILE_DELIVER_STOCK ?></h3> 
        
        <!-- <p class="dark-tur-text note-p3 ow-font-weight400 stock-text-top"><?php echo _STOCK_CURRENT_FOR ?> Product A</p>  -->
        <!-- <h3 class="center-div-h3 stock-text-bottom">300  <?php echo _STOCK_PCS ?></h3> -->

        <p class="dark-tur-text note-p3 ow-font-weight400 stock-text-top"><?php echo _STOCK_CURRENT_FOR ?> <?php echo $renameProductName;?></p> 
        <h3 class="center-div-h3 stock-text-bottom"><?php echo $productValue;?>  <?php echo _STOCK_PCS ?></h3>

        <!-- <form> -->
        <form action="utilities/userStockDeliverFunction.php" method="POST">
            <input type="text" class="rec-input clean ow-margin-left0 margin-top30" placeholder="<?php echo _STOCK_AMOUNT ?>"  id="amount" name="amount" required>
            <div class="clear"></div>
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _STOCK_RECEIVER_NAME ?>"  id="received_name" name="received_name" required>
            <div class="clear"></div>        
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _PROFILE_ADDRESS ?>"  id="address" name="address" required>
            <div class="clear"></div>        
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _STOCK_CONTACT ?>"  id="contact" name="contact" required>
            <div class="clear"></div>         
            <input type="hidden" value="<?php echo $productName;?>"  id="product_type" name="product_type" readonly>
            <div class="clear"></div>    
            <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _PROFILE_CONFIRM ?></button>
        </form>

        </div>
    </div>

</div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>