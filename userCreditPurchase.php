<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCreditPurchase.php" />
<link rel="canonical" href="https://agentpnchc.com/userCreditPurchase.php" />
<meta property="og:title" content="<?php echo _PROFILE_PURCHASE_CREDIT ?> | Pure & Cure" />
<title><?php echo _PROFILE_PURCHASE_CREDIT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>


<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_PURCHASE_CREDIT ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <?php include 'userTitle.php'; ?>

    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
    <p class="explain dark-tur-text"><?php echo _TOPUP_DETAILS ?></p>
    <p class="explain dark-tur-text"><?php echo _TOPUP_BANK ?>: UOB Bank</p>
    <p class="explain dark-tur-text"><?php echo _TOPUP_BANK_ACC_HOLDER ?>: Fortune Capital Worldwide</p>
    <p class="explain dark-tur-text"><?php echo _TOPUP_BANK_ACC_NO ?>: 220-302-363-6</p>

        <form action="utilities/userCreditPurchaseFunction.php" method="POST" enctype="multipart/form-data">
            <!-- <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _TOPUP_AMOUNT ?>"  id="amount" name="amount" required>
            <div class="clear"></div> -->
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _TOPUP_CREDIT_AMOUNT ?>"  id="credit" name="credit" required><div class="clear"></div>
            <p class="explain dark-tur-text"><?php echo _TOPUP_UPLOAD_RECEIPT ?></p>
            <input id="file-upload" type="file" name="file_one" id="file_one" class="ow-margin-bottom10 pointer" required/>
            <div class="clear"></div>
            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _TOPUP_REFERENCE ?>"  id="reference" name="reference" required><div class="clear"></div>
            <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _INDEX_SUBMIT ?></button>
        </form>      
    
    </div>
</div>
</div>

<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>



</body>
</html>