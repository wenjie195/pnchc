<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Variation.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$newProductUid = $_SESSION['product_uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminProductVariationTwoAdd.php" />
<link rel="canonical" href="https://agentpnchc.com/adminProductVariationTwoAdd.php" />
<meta property="og:title" content="Add Product | Pure & Cure" />
<title>Add Product | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding tur-bg min-height100 padding-top">

    <p class="text-center"><img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="center-logo"></p>
    <h1 class="h1 white-text text-center login-h1"><b>Add Variation</b></h1>

    <!-- <form action="utilities/adminProductRegisterFunction.php" method="POST" enctype="multipart/form-data"> -->
    <form action="utilities/adminRegisterVariationTwoFunction.php" method="POST">

        <div class="login-div margin-auto">

            <!-- <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Product Name" id="product_name" name="product_name" required>
            </div>

            <div class="clear"></div> -->

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Level" id="level" name="level" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Minimum Order Quantity" id="moq" name="moq" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Unit Price" id="unit_price" name="unit_price" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Profit" id="profit" name="profit" required>
            </div>

            <div class="clear"></div>
            
            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Commission Level 1" id="com_lvl_one" name="com_lvl_one">
            </div>

            <div class="clear"></div>
            
            <div class="fake-input-div">
                <input type="text" class="input-css input-css2 clean white-text" placeholder="Commission Level 2" id="com_lvl_two" name="com_lvl_two">
            </div>

            <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $newProductUid ;?>" name="main_product_uid" id="main_product_uid" readonly>

            <div class="clear"></div>       

            <button class="clean yellow-button white-text margin-top30" name="submit">Submit</button>

        </div>

    </form>

    <div class="clear"></div>
   
    <div class="width100 text-center" style="margin-top:20px;">
        <a href="adminProductAll.php" class="green-a text-center" style="font-size:18px;">Skip</a>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>