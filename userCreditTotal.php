<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCreditTotal.php" />
<link rel="canonical" href="https://agentpnchc.com/userCreditTotal.php" />
<meta property="og:title" content="<?php echo _PROFILE_TOTAL_CREDIT ?> | Pure & Cure" />
<title><?php echo _PROFILE_TOTAL_CREDIT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_TOTAL_CREDIT ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
		    <img src="img/credit.png" alt="<?php echo _PROFILE_TOTAL_CREDIT ?>"   title="<?php echo _PROFILE_TOTAL_CREDIT ?>" class="center-icon">
            <div class="clear"></div>
            <h3 class="center-div-h3"><?php echo _PROFILE_CURRENT_CREDIT ?></h3>  
            <h1 class="brown-text value-h1">
                <!-- <?php //echo $userData->getCredit();?> -->
                <?php $credit = $userData->getCredit();?>
                <?php echo number_format("$credit",2);?>
            </h1> 
            <div class="clear"></div>
           
            <a href="userCreditPurchase.php"><div class="pill-button yellow-hover-bg open-topup three-btn-row"><?php echo _PROFILE_TOPUP ?></div></a>
            
            <!-- <a href="userCreditWithdrawal.php"><div class="pill-button yellow-hover-bg three-btn-row mid-btn-row"><?php echo _PROFILE_WITHDRAW ?></div></a> -->
            <a href="userCreditConvert.php"><div class="pill-button yellow-hover-bg three-btn-row mid-btn-row"><?php echo _COMMISSION_CONVERT ?></div></a>
            
            <a href="userCreditPurchaseHistory.php"><div class="pill-button green-hover-bg three-btn-row"><?php echo _PROFILE_HISTORY ?></div></a>
            <div class="clear"></div>
            <p class="dark-tur-text note-p3">
				<?php echo _PROFILE_NOTE_1 ?>
                <br><?php echo _PROFILE_NOTE_2 ?>
                <br><?php echo _PROFILE_NOTE_3 ?>
            </p> 
    </div>
</div>
</div>
<div class="clear"></div>


<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Convert Commission To Credit !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>