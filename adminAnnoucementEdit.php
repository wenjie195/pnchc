<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminAnnoucementEdit.php" />
<link rel="canonical" href="https://agentpnchc.com/adminAnnoucementEdit.php" />
<meta property="og:title" content="<?php echo _PROFILE_ANNOUNCEMENT_EDIT ?> | Pure & Cure" />
<title><?php echo _PROFILE_ANNOUNCEMENT_EDIT ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>


<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_ANNOUNCEMENT_EDIT ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
	
    
   
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x text-center">

    <form action="utilities/adminAnnouncementEditFunction.php" method="POST">

        
            <?php
            if(isset($_POST['announcement_uid']))
            {
                $conn = connDB();
                $announcementContent = getAnnouncement($conn,"WHERE uid = ? ", array("uid") ,array($_POST['announcement_uid']),"s");
            ?>

                
                    <input type="date" placeholder="<?php echo _TOPUP_DATE ?>" value="<?php echo $announcementContent[0]->getDateInput();?>" class="rec-input clean ow-margin-left0 ow-width100" name='update_date' id="update_date">
               

                <div class="clear"></div>

               
                    <input type="text" class="rec-input clean ow-margin-left0 ow-width100" placeholder="<?php echo _ANNOUNCEMENT_TITLE ?>" value="<?php echo $announcementContent[0]->getTitle();?>" id="update_title" name="update_title" required>
                

                <div class="clear"></div>
<textarea type="text" class="rec-input clean ow-margin-left0 ow-width100 text-area-height" placeholder="<?php echo _ANNOUNCEMENT_CONTENT ?>"  id="update_message" name="update_message" required><?php echo $announcementContent[0]->getContent();?></textarea>
                
               
                    <!--<input type="text" class="rec-input clean ow-margin-left0 margin-top30 ow-width100" placeholder="<?php echo _ANNOUNCEMENT_CONTENT ?>" value="<?php echo $announcementContent[0]->getContent();?>" id="update_message" name="update_message" required>
                -->

                <div class="clear"></div>     

                <input type="hidden" value="<?php echo $announcementContent[0]->getUid();?>" name="announcement_uid" id="announcement_uid" readonly> 
                
                <div class="clear"></div>       

                <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _PROFILE_CONFIRM ?></button>

            <?php
            }
            ?>

        </div>

    </form>

    <div class="clear"></div>
   
</div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

<script>
  $(function()
  {
    $("#date").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

</body>
</html>