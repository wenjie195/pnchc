    <div class="width100 same-padding">
    	<div class="profile-cover-div">
    	    <div class="profile-img-div">

                <?php 
                    $profileIamge = $userData->getImage();
                    if($profileIamge != '')
                    {
                    ?>
                        <img src="profileImage/<?php echo $userData->getImage();?>" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
                    <?php
                    }
                    else
                    {
                    ?>
                        <img src="img/account.png" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
                    <?php
                    }
                ?>

        		<!-- <img src="img/account.png" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>"> -->
            </div>
            <div class="line-button open-upload pointer"><img src="img/edit.png" class="edit-icon" alt="<?php echo _PROFILE_EDIT_PROFILE ?>" title="<?php echo _PROFILE_EDIT_PROFILE ?>"><?php echo _PROFILE_EDIT ?></div>
            
            <div class="clear"></div>
            
            <?php 
                $rankA = $userData->getRankA();
                if($rankA == '1')
                {
                    $renameRankA = 'Member';
                }
                elseif($rankA == '2')
                {
                    $renameRankA = 'Angel Agent';
                }
                elseif($rankA == '3')
                {
                    $renameRankA = 'Silver Dealer';
                }
                elseif($rankA == '4')
                {
                    $renameRankA = 'Ace Dealer';
                }
                elseif($rankA == '5')
                {
                    $renameRankA = 'CO-Founder';
                }
                else
                {
                    $renameRankA = $rankA;
                }
                // echo $renameRankA;
            ?>

            <?php 
                $rankB = $userData->getRankB();
                if($rankB == '1')
                {
                    $renameRankB = 'Member';
                }
                elseif($rankB == '2')
                {
                    $renameRankB = 'Angel Agent';
                }
                elseif($rankB == '3')
                {
                    $renameRankB = 'Silver Dealer';
                }
                elseif($rankB == '4')
                {
                    $renameRankB = 'Ace Dealer';
                }
                else
                {
                    $renameRankB = $rankB;
                }
                // echo $renameRankB;
            ?>

            <!-- <p class="white-text info-p2"><?php //echo _PROFILE_RANKING ?> A : <?php //echo $userData->getRankA();?> | <?php //echo _PROFILE_RANKING ?> A <?php //echo _TOPUP_STATUS ?> : <?php //echo $userData->getMaintenanceA();?></p>
            <p class="white-text info-p2"><?php //echo _PROFILE_RANKING ?> B : <?php //echo $userData->getRankB();?> | <?php //echo _PROFILE_RANKING ?> B <?php //echo _TOPUP_STATUS ?> : <?php //echo $userData->getMaintenanceB();?></p> -->
            
            <p class="black-text info-p2">Colloid Plus : <?php echo $renameRankA;?> | <?php echo $userData->getMaintenanceA();?></p>
            <!-- <p class="black-text info-p2">Eye Love Oil : <?php echo $renameRankB;?> | <?php echo $userData->getMaintenanceB();?></p> -->

            <p class="black-text info-p2"><?php echo _PROFILE_USER_ID ?>: <?php echo $userData->getId();?></p>
            
            <p class="black-text info-p2"><?php echo _SUCCESS_REFER_LINK ?>: 
            
            <?php
                $actual_link = $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $fullPath = dirname($path);
            ?>

            <input type="hidden" id="linkCopy" value="<?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?>">
            <a id="invest-now-referral-link" style="word-break:break-all;" href="<?php echo 'https://'.$fullPath.'/signupWithReferCode.php?referrerUID='.$_SESSION['uid'] ?>" class="dark-tur-link info-p2" target="_blank"><?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?></a></h3>
            <button class="invite-btn  clean ow-red-bg white-text clean-button" id="copy-referral-link">Copy</button>

            </p>
        </div>
    </div>
<!--<div class="width100 same-padding tur-bg padding-top48 overflow extra-padding-tb text-center">
    <div class="right-profile-div ow-float-left2 ow-float-none">
        <div class="profile-img-div">
            <?php 
                $profileIamge = $userData->getImage();
                if($profileIamge != '')
                {
                ?>
                    <img src="profileImage/<?php echo $userData->getImage();?>" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
                <?php
                }
                else
                {
                ?>
                    <img src="img/account.png" class="profile-pic" alt="<?php echo _PROFILE_PICTURE ?>" title="<?php echo _PROFILE_PICTURE ?>">
                <?php
                }
            ?>
        </div>
    </div>
    
    <div class="left-profile-detail left-profile-detail2 overflow-auto ow-float-right2 ow-float-none">
        <h3 class="ranking-h3"><?php echo _PROFILE_RANKING ?> A : <?php echo $userData->getRankA();?></h3>
        <h3 class="ranking-h3"><?php echo _PROFILE_RANKING ?> B : <?php echo $userData->getRankB();?></h3>
        <p class="white-text info-p2"><?php echo _PROFILE_USER_ID ?>: <?php echo $userData->getId();?></p>
        <p class="white-text info-p2"><?php echo _SUCCESS_REFER_LINK ?>: 
        
        <?php
            $actual_link = $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $fullPath = dirname($path);
        ?>

        <input type="hidden" id="linkCopy" value="<?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?>">
        <a id="invest-now-referral-link" style="word-break:break-all;" href="<?php echo 'https://'.$fullPath.'/signupWithReferCode.php?referrerUID='.$_SESSION['uid'] ?>" class="white-text info-p2" target="_blank"><?php echo "https://".$fullPath."/signupWithReferCode.php?referrerUID=".$_SESSION['uid']?></a></h3>
        <button class="invite-btn  clean green-button" id="copy-referral-link">Copy</button>
        
        </p>
    </div>        
</div>-->