<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$withdrawalDetails = getWithdrawal($conn, " WHERE uid = ? ORDER BY date_created DESC ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCreditWithdrawalHistory.php" />
<link rel="canonical" href="https://agentpnchc.com/userCreditWithdrawalHistory.php" />
<meta property="og:title" content="<?php echo _COMMISSION_WITHDRAWAL_HISTORY ?> | Pure & Cure" />
<title><?php echo _COMMISSION_WITHDRAWAL_HISTORY ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _COMMISSION_WITHDRAWAL_HISTORY ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <?php include 'userTitle.php'; ?>
    
    <div class="width100 same-padding details-min-height padding-top2 overflow overflow-x">
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _WITHDRAW_AMOUNT ?></th>
                    <th><?php echo _TOPUP_STATUS ?></th>
                    <th><?php echo _TOPUP_DATE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($withdrawalDetails)
                    {
                        for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $withdrawalDetails[$cnt]->getAmount();?></td>
                                <td><?php echo $withdrawalDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $withdrawalDetails[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
        </div>
    </div>
</div>


</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Submit Withdrawal Request Successfully !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>