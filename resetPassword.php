<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = null;

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/resetPassword.php" />
<link rel="canonical" href="https://agentpnchc.com/resetPassword.php" />
<meta property="og:title" content="<?php echo _RP_TITLE ?> | Pure & Cure" />
<title><?php echo _RP_TITLE ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>
<div class="width100 same-padding min-height100 padding-top login-bg padding-top-bottom">
	<p class="text-center"><img src="img/logo.png" alt="Pure & Cure" title="Pure & Cure" class="center-logo"></p>
    <h1 class="h1 red-text text-center login-h1"><?php echo _RP_TITLE ?></b></h1>
    <div class="login-div margin-auto">

        <form action="utilities/resetPasswordFunction.php" method="POST">

            <input class="input-css input-css2 clean dark-tur-text2" type="hidden" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>" name="checkThat" readonly>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input class="input-css input-css2 clean dark-tur-text2" type="text" placeholder="<?php echo _RP_CODE ?>" id="verify_code" name="verify_code" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input class="input-css input-css2 clean dark-tur-text2" type="text" placeholder="<?php echo _RP_PASSWORD ?>" id="new_password" name="new_password" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input class="input-css input-css2 clean dark-tur-text2" type="text" placeholder="<?php echo _RP_RETYPE_PASSWORD ?>" id="retype_new_password" name="retype_new_password" required>
                
            </div>

            <div class="clear"></div>

            <div class="clear"></div>
            
            <button class="clean white-button ow-red-bg white-text">
                <?php echo _INDEX_SUBMIT ?>
            </button>
        </form>

        <div class="clear"></div>

    </div>
</div>

<?php include 'js.php'; ?>
</body>
</html>