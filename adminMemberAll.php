<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getUser($conn, " WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminMemberAll.php" />
<link rel="canonical" href="https://agentpnchc.com/adminMemberAll.php" />
<meta property="og:title" content="<?php echo _ADMIN_TOTAL_MEMBER ?> | Pure & Cure" />
<title><?php echo _ADMIN_TOTAL_MEMBER ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _ADMIN_TOTAL_MEMBER ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
        <div class="width100 overflow-x">
            <table class="width100 tur-table">
                <thead>
                    <tr>
                        <th><?php echo _TOPUP_NO ?></th>
                        <th><?php echo _SIGN_UP_NAME ?></th>
                        <th>Colloid Plus</th>
                        <th>Colloid Plus <?php echo _ADMIN_VALUE ?></th>
                        <th>Eye Love Oil</th>
                        <th>Eye Love Oil <?php echo _ADMIN_VALUE ?></th>
                        <th><?php echo _SIGN_UP_JOIN_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($allUser)
                        {
                            for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $allUser[$cnt]->getUsername();?></td>
                                    <td>
                                        <?php 
                                            $rankA = $allUser[$cnt]->getRankA();
                                            if($rankA == '1')
                                            {
                                                $renameRankA = 'Member';
                                            }
                                            elseif($rankA == '2')
                                            {
                                                $renameRankA = 'Angel Agent';
                                            }
                                            elseif($rankA == '3')
                                            {
                                                $renameRankA = 'Silver Dealer';
                                            }
                                            elseif($rankA == '4')
                                            {
                                                $renameRankA = 'Ace Dealer';
                                            }
                                            elseif($rankA == '5')
                                            {
                                                $renameRankA = 'CO-Founder';
                                            }
                                            else
                                            {
                                                $renameRankA = $rankA;
                                            }
                                            echo $renameRankA;
                                        ?>
                                    </td>
                                    <td><?php echo $allUser[$cnt]->getValueA();?></td>
                                    <td>
                                        <?php 
                                            $rankB = $allUser[$cnt]->getRankB();
                                            if($rankB == '1')
                                            {
                                                $renameRankB = 'Member';
                                            }
                                            elseif($rankB == '2')
                                            {
                                                $renameRankB = 'Angel Agent';
                                            }
                                            elseif($rankB == '3')
                                            {
                                                $renameRankB = 'Silver Dealer';
                                            }
                                            elseif($rankB == '4')
                                            {
                                                $renameRankB = 'Ace Dealer';
                                            }
                                            else
                                            {
                                                $renameRankB = $rankB;
                                            }
                                            echo $renameRankB;
                                        ?>
                                    </td>
                                    <td><?php echo $allUser[$cnt]->getValueB();?></td>
                                    <td><?php echo $allUser[$cnt]->getDateCreated();?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>