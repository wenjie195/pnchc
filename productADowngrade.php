<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

echo $todayDate = date('Y-m-d');
echo "<br>";
echo $currentMonth = date('m');
echo "<br>";

$M1 = '01';
$M2 = '02';
$M3 = '03';
$M4 = '04';
$M5 = '05';
$M6 = '06';
$M7 = '07';
$M8 = '08';
$M9 = '09';
$M10 = '10';
$M11 = '11';
$M12 = '12';

$status = "Inactive";
$maintenanceStatusProductA = 'Active';

// $allUsers = getUser($conn);
$allUsers = getUser($conn," WHERE user_type = '1' AND rank_a >= 2 ");
if($allUsers) 
{
    for($cnt = 0;$cnt < count($allUsers) ;$cnt++)
    {
        // $userRank = $allUsers[$cnt]->getUserRank();
        echo $userUid = $allUsers[$cnt]->getUid();
        echo "<br>";
        echo $username = $allUsers[$cnt]->getUsername();
        echo "<br>";
        echo $userCurrentRankA = $allUsers[$cnt]->getRankA();
        echo "<br>";

        if($userCurrentRankA == 5) 
        {
            // $ordersDetails = getOrderList($conn, "WHERE user_uid= ? AND status = 'Sold' AND quantity >= 120 ORDER BY date_created DESC LIMIT 1 ",array("user_uid"),array($allUsers[$cnt]->getUid()), "s");
            $ordersDetails = getOrderList($conn, "WHERE user_uid= ? AND status = 'Sold' AND product_name = 'Product A' AND quantity >= 120 ORDER BY date_created DESC LIMIT 1 ",array("user_uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $orderQuantity = $ordersDetails[$cntA]->getQuantity();
                    echo "<br>";    
                    echo $orderMonth = date('m',strtotime($orderDate = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
    
                    if($orderQuantity >= 120) 
                    {
                        if($currentMonth == $M1) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$maintenanceStatusProductA);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M2) 
                        {
                            if($orderMonth == $M2 || $orderMonth == $M3) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M3) 
                        {
                            if($orderMonth == $M3 || $orderMonth == $M4) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M4) 
                        {
                            if($orderMonth == $M4 || $orderMonth == $M5) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M5) 
                        {
                            if($orderMonth == $M5 || $orderMonth == $M6) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M6) 
                        {
                            if($orderMonth == $M6 || $orderMonth == $M7) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M7) 
                        {
                            if($orderMonth == $M7 || $orderMonth == $M8) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M8) 
                        {
                            if($orderMonth == $M8 || $orderMonth == $M9) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M9) 
                        {
                            if($orderMonth == $M9 || $orderMonth == $M10) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M10) 
                        {
                            if($orderMonth == $M10 || $orderMonth == $M11) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M11) 
                        {
                            if($orderMonth == $M11 || $orderMonth == $M12) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M12) 
                        {
                            if($orderMonth == $M12 || $orderMonth == $M1) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                    }
                    else
                    {
                        // echo "No Enough Quantity FOr Maintenance !!";
                        // echo "<br>";    
    
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";     
                        if($status)
                        {
                             array_push($tableName,"maintenance_a");
                             array_push($tableValue,$status);
                             $stringType .=  "s";
                        }            
                        array_push($tableValue,$userUid);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status Updated ! <br>";
                        }
                        else
                        {
                            echo "ERROR 1 <br>";
                        }
                    }
                }
            }
            else
            {
                echo "No Order !! <br> <br>";
    
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";     
                if($status)
                {
                     array_push($tableName,"maintenance_a");
                     array_push($tableValue,$status);
                     $stringType .=  "s";
                }            
                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    echo "Inactive Status Updated ! <br>";
                }
                else
                {
                    echo "ERROR 1 <br>";
                }
            }
        }
        elseif($userCurrentRankA == 4)
        {
            $ordersDetails = getOrderList($conn, "WHERE user_uid= ? AND status = 'Sold' AND product_name = 'Product A' AND quantity >= 40 ORDER BY date_created DESC LIMIT 1 ",array("user_uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $orderQuantity = $ordersDetails[$cntA]->getQuantity();
                    echo "<br>";    
                    echo $orderMonth = date('m',strtotime($orderDate = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
    
                    if($orderQuantity >= 40) 
                    {
                        if($currentMonth == $M1) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M2) 
                        {
                            if($orderMonth == $M2 || $orderMonth == $M3) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M3) 
                        {
                            if($orderMonth == $M3 || $orderMonth == $M4) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M4) 
                        {
                            if($orderMonth == $M4 || $orderMonth == $M5) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M5) 
                        {
                            if($orderMonth == $M5 || $orderMonth == $M6) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M6) 
                        {
                            if($orderMonth == $M6 || $orderMonth == $M7) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M7) 
                        {
                            if($orderMonth == $M7 || $orderMonth == $M8) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M8) 
                        {
                            if($orderMonth == $M8 || $orderMonth == $M9) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M9) 
                        {
                            if($orderMonth == $M9 || $orderMonth == $M10) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M10) 
                        {
                            if($orderMonth == $M10 || $orderMonth == $M11) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M11) 
                        {
                            if($orderMonth == $M11 || $orderMonth == $M12) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M12) 
                        {
                            if($orderMonth == $M12 || $orderMonth == $M1) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                    }
                    else
                    {
                        // echo "No Enough Quantity FOr Maintenance !!";
                        // echo "<br>";    
    
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";     
                        if($status)
                        {
                             array_push($tableName,"maintenance_a");
                             array_push($tableValue,$status);
                             $stringType .=  "s";
                        }            
                        array_push($tableValue,$userUid);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status Updated ! <br>";
                        }
                        else
                        {
                            echo "ERROR 1 <br>";
                        }
                    }
                }
            }
            else
            {
                echo "No Order !! <br> <br>";
    
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";     
                if($status)
                {
                     array_push($tableName,"maintenance_a");
                     array_push($tableValue,$status);
                     $stringType .=  "s";
                }            
                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    echo "Inactive Status Updated ! <br>";
                }
                else
                {
                    echo "ERROR 1 <br>";
                }
            }
        }
        elseif($userCurrentRankA == 3)
        {
            $ordersDetails = getOrderList($conn, "WHERE user_uid= ? AND status = 'Sold' AND product_name = 'Product A' AND quantity >= 20 ORDER BY date_created DESC LIMIT 1 ",array("user_uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $orderQuantity = $ordersDetails[$cntA]->getQuantity();
                    echo "<br>";    
                    echo $orderMonth = date('m',strtotime($orderDate = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
    
                    if($orderQuantity >= 20) 
                    {
                        if($currentMonth == $M1) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M2) 
                        {
                            if($orderMonth == $M2 || $orderMonth == $M3) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M3) 
                        {
                            if($orderMonth == $M3 || $orderMonth == $M4) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M4) 
                        {
                            if($orderMonth == $M4 || $orderMonth == $M5) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M5) 
                        {
                            if($orderMonth == $M5 || $orderMonth == $M6) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M6) 
                        {
                            if($orderMonth == $M6 || $orderMonth == $M7) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M7) 
                        {
                            if($orderMonth == $M7 || $orderMonth == $M8) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M8) 
                        {
                            if($orderMonth == $M8 || $orderMonth == $M9) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M9) 
                        {
                            if($orderMonth == $M9 || $orderMonth == $M10) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M10) 
                        {
                            if($orderMonth == $M10 || $orderMonth == $M11) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M11) 
                        {
                            if($orderMonth == $M11 || $orderMonth == $M12) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M12) 
                        {
                            if($orderMonth == $M12 || $orderMonth == $M1) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                    }
                    else
                    {
                        // echo "No Enough Quantity FOr Maintenance !!";
                        // echo "<br>";    
    
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";     
                        if($status)
                        {
                             array_push($tableName,"maintenance_a");
                             array_push($tableValue,$status);
                             $stringType .=  "s";
                        }            
                        array_push($tableValue,$userUid);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status Updated ! <br>";
                        }
                        else
                        {
                            echo "ERROR 1 <br>";
                        }
                    }
                }
            }
            else
            {
                echo "No Order !! <br> <br>";
    
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";     
                if($status)
                {
                     array_push($tableName,"maintenance_a");
                     array_push($tableValue,$status);
                     $stringType .=  "s";
                }            
                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    echo "Inactive Status Updated ! <br>";
                }
                else
                {
                    echo "ERROR 1 <br>";
                }
            }
        }
        elseif($userCurrentRankA == 2)
        {
            $ordersDetails = getOrderList($conn, "WHERE user_uid= ? AND status = 'Sold' AND product_name = 'Product A' AND quantity >= 4 ORDER BY date_created DESC LIMIT 1 ",array("user_uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $orderQuantity = $ordersDetails[$cntA]->getQuantity();
                    echo "<br>";    
                    echo $orderMonth = date('m',strtotime($orderDate = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
    
                    if($orderQuantity >= 4) 
                    {
                        if($currentMonth == $M1) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M2) 
                        {
                            if($orderMonth == $M2 || $orderMonth == $M3) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M3) 
                        {
                            if($orderMonth == $M3 || $orderMonth == $M4) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M4) 
                        {
                            if($orderMonth == $M4 || $orderMonth == $M5) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M5) 
                        {
                            if($orderMonth == $M5 || $orderMonth == $M6) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M6) 
                        {
                            if($orderMonth == $M6 || $orderMonth == $M7) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M7) 
                        {
                            if($orderMonth == $M7 || $orderMonth == $M8) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M8) 
                        {
                            if($orderMonth == $M8 || $orderMonth == $M9) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M9) 
                        {
                            if($orderMonth == $M9 || $orderMonth == $M10) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M10) 
                        {
                            if($orderMonth == $M10 || $orderMonth == $M11) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M11) 
                        {
                            if($orderMonth == $M11 || $orderMonth == $M12) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                        elseif($currentMonth == $M12) 
                        {
                            if($orderMonth == $M12 || $orderMonth == $M1) 
                            {
                                // echo "Status Active !! <br>";
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($maintenanceStatusProductA)
                                {
                                    array_push($tableName,"maintenance_a");
                                    array_push($tableValue,$maintenanceStatusProductA);
                                    $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Remain Active Status ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                            else
                            {
                                $tableName = array();
                                $tableValue =  array();
                                $stringType =  "";
                                //echo "save to database";     
                                if($status)
                                {
                                     array_push($tableName,"maintenance_a");
                                     array_push($tableValue,$status);
                                     $stringType .=  "s";
                                }            
                                array_push($tableValue,$userUid);
                                $stringType .=  "s";
                                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                if($passwordUpdated)
                                {
                                    echo "Inactive Status Updated ! <br>";
                                }
                                else
                                {
                                    echo "ERROR 1 <br>";
                                }
                            }
                        }
                    }
                    else
                    {
                        // echo "No Enough Quantity FOr Maintenance !!";
                        // echo "<br>";    
    
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";     
                        if($status)
                        {
                             array_push($tableName,"maintenance_a");
                             array_push($tableValue,$status);
                             $stringType .=  "s";
                        }            
                        array_push($tableValue,$userUid);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status Updated ! <br>";
                        }
                        else
                        {
                            echo "ERROR 1 <br>";
                        }
                    }
                }
            }
            else
            {
                echo "No Order !! <br> <br>";
    
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";     
                if($status)
                {
                     array_push($tableName,"maintenance_a");
                     array_push($tableValue,$status);
                     $stringType .=  "s";
                }            
                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    echo "Inactive Status Updated ! <br>";
                }
                else
                {
                    echo "ERROR 1 <br>";
                }
            }
        }
        else
        {}
    }
}
?>