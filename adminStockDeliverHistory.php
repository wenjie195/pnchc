<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/DeliverRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $creditDetails = getWithdrawal($conn, " WHERE status = 'Pending' ORDER BY date_created DESC ");
$creditDetails = getDeliverRecord($conn, " WHERE status != 'Pending' ORDER BY date_created DESC ");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminStockDeliverHistory.php" />
<link rel="canonical" href="https://agentpnchc.com/adminStockDeliverHistory.php" />
<meta property="og:title" content="<?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?> | Pure & Cure" />
<title><?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_DELIVERY_STOCK_HISTORY ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
	
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
        	<thead>
            	<tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _INDEX_USERNAME ?></th>

                    <th><?php echo _STOCK_PRODUCT ?></th>
                    <th><?php echo _STOCK_QUANTITY ?></th>
                    <th><?php echo _STOCK_RECEIVER_NAME ?></th>
                    <th><?php echo _PROFILE_ADDRESS ?></th>
                    <th><?php echo _STOCK_CONTACT ?></th>

                    <th><?php echo _TOPUP_STATUS ?></th>
                    <th><?php echo _TOPUP_DATE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($creditDetails)
                    {
                        for($cnt = 0;$cnt < count($creditDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>
                                    <?php 
                                        $userUid = $creditDetails[$cnt]->getUid();
                                        $conn = connDB();
                                        $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
                                        echo $usename = $userDetails[0]->getUsername();
                                    ?>
                                </td>

                                <td>
                                    <?php 
                                        $productName = $creditDetails[$cnt]->getStockType();
                                        if($productName == 'A')
                                        {
                                            $renameProductName = 'Colloid Plus';
                                        }
                                        elseif($productName == 'B')
                                        {
                                            $renameProductName = 'Eye Love Oil';
                                        }
                                        else
                                        {
                                            $renameProductName = $productName;
                                        }
                                        echo $renameProductName;
                                    ?>
                                </td>
                                <td><?php echo $creditDetails[$cnt]->getAmount();?></td>
                                <td><?php echo $creditDetails[$cnt]->getReceiver();?></td>
                                <td><?php echo $creditDetails[$cnt]->getAddress();?></td>
                                <td><?php echo $creditDetails[$cnt]->getContact();?></td>
                                
                                <td><?php echo $creditDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $creditDetails[$cnt]->getDateCreated();?></td>

                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

   
</div></div>
<div class="clear"></div>
</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Stock Delivery Approved !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Stock Delivery Rejected !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>