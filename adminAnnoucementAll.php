<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$annoucement = getAnnouncement($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/adminAnnoucementAll.php" />
<link rel="canonical" href="https://agentpnchc.com/adminAnnoucementAll.php" />
<meta property="og:title" content="<?php echo _PROFILE_ANNOUNCEMENT_ALL ?> | Pure & Cure" />
<title><?php echo _PROFILE_ANNOUNCEMENT_ALL ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>



<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _PROFILE_ANNOUNCEMENT_ALL ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	
	<div class="width100 inner-bg inner-padding">
	
  
    
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    <div class="width100 overflow-x">
        <table class="width100 tur-table">
            <thead>
                <tr>
                    <th><?php echo _TOPUP_NO ?></th>
                    <th><?php echo _ANNOUNCEMENT_TITLE ?></th>
                    <th><?php echo _TOPUP_DATE ?></th>
                    <th><?php echo _PROFILE_EDIT ?></th>
                    <th><?php echo _STOCK_DELETE ?></th>
                </tr>
            </thead>
 <tbody>
                <?php
                    if($annoucement)
                    {
                        for($cnt = 0;$cnt < count($annoucement) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $annoucement[$cnt]->getTitle();?></td>
                                <!--<td><?php echo $annoucement[$cnt]->getContent();?></td>-->
                                <td><?php echo $annoucement[$cnt]->getDateInput();?></td>

                                <td>
                                    <form action="adminAnnoucementEdit.php" method="POST" class="hover1">
                                        <button class="clean transparent-button" type="submit" name="announcement_uid" value="<?php echo $annoucement[$cnt]->getUid();?>">
                                            <img src="img/edit.png" class="icon-size opacity-hover" title="<?php echo _PROFILE_EDIT ?>" alt="<?php echo _PROFILE_EDIT ?>">
                                        </button>
                                    </form> 
                                </td>

                                <td>
                                    <form method="POST" action="utilities/adminAnnouncementDeleteFunction.php" class="hover1">
                                        <button class="clean transparent-button" type="submit" name="announcement_uid" value="<?php echo $annoucement[$cnt]->getUid();?>">
                                            <img src="img/delete.png" class="icon-size opacity-hover" title="<?php echo _STOCK_DELETE ?>" alt="<?php echo _STOCK_DELETE ?>">
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
        </div>
    </div>
</div>


<div class="clear"></div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php unset($_SESSION['product_uid']);?>

</body>
</html>