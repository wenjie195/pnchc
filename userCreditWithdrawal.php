<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://agentpnchc.com/userCreditWithdrawal.php" />
<link rel="canonical" href="https://agentpnchc.com/userCreditWithdrawal.php" />
<meta property="og:title" content="<?php echo _COMMISSION_WITHDRAWAL ?> | Pure & Cure" />
<title><?php echo _COMMISSION_WITHDRAWAL ?> | Pure & Cure</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'bg.php'; ?>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text"><?php echo _COMMISSION_WITHDRAWAL ?></h1><?php include 'header.php'; ?>
	
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <?php include 'userTitle.php'; ?>
    <div class="width100 same-padding details-min-height padding-top2 overflow center-div2">
        <h3 class="center-div-h3"><?php echo _COMMISSION_WITHDRAWAL ?></h3>  
        <h1 class="brown-text value-h1">
            <!-- <?php //echo $userData->getWallet();?> -->
            <?php $wallet = $userData->getWallet();?>
            <?php echo number_format("$wallet",2);?>
        </h1> 
        <!-- <form action="utilities/userCreditWithdrawalFunction.php" method="POST" enctype="multipart/form-data"> -->
        <form action="utilities/userCreditWithdrawalFunction.php" method="POST">
            

            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _WITHDRAW_AMOUNT ?>"  id="withdrawal_amount" name="withdrawal_amount" required>

            <div class="clear"></div>

            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _TOPUP_BANK ?>" value="<?php echo $userData->getBankName();?>" id="bank_name" name="bank_name" required>        

            <div class="clear"></div>

            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _TOPUP_BANK_ACC_HOLDER ?>" value="<?php echo $userData->getBankAccName();?>" id="bank_acc_holder" name="bank_acc_holder" required>  

            <div class="clear"></div>

            <input type="text" class="rec-input clean ow-margin-left0" placeholder="<?php echo _TOPUP_BANK_ACC_NO ?>" value="<?php echo $userData->getBankAccNumber();?>" id="bank_account_no" name="bank_account_no" required>     

            <div class="clear"></div>

            <p class="dark-tur-text note-p3">
                <?php echo _PROFILE_NOTE_1C ?>
                <br><?php echo _PROFILE_NOTE_2C ?>
                <br><?php echo _PROFILE_NOTE_3 ?>
            </p> 
            
            <button class="clean yellow-btn edit-profile-width ow-margin-left0" name="submit"><?php echo _INDEX_SUBMIT ?></button>
        </form>      

       

    </div>
</div>

</div>
<?php include 'footermenu.php'; ?>
<div class="clear"></div>
<?php include 'js.php'; ?>



</body>
</html>